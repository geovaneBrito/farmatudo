package br.com.farmatudo.bean;

public class FormaPagamentoBean
{
	private int forma_pagamento_id;
	private int venda_id;
	
	
	
	public FormaPagamentoBean(int forma_pagamento_id, int venda_id)
	{
		this.forma_pagamento_id = forma_pagamento_id;
		this.venda_id = venda_id;
	}
	public int getForma_pagamento_id()
	{
		return forma_pagamento_id;
	}
	public void setForma_pagamento_id(int forma_pagamento_id)
	{
		this.forma_pagamento_id = forma_pagamento_id;
	}
	public int getVenda_id()
	{
		return venda_id;
	}
	public void setVenda_id(int venda_id)
	{
		this.venda_id = venda_id;
	}
	
}
