package br.com.farmatudo.bean;

public class LaboratorioBean
{
	private int id;
	private String nome;
	private String telefone;
	
	public LaboratorioBean()
	{
		
	}
	
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getNome()
	{
		return nome;
	}
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	public String getTelefone()
	{
		return telefone;
	}
	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}
	
	
}
