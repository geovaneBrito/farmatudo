package br.com.farmatudo.bean;

import java.util.Date;

public class CaixaBean
{
	private int id;
	private String usuario_nome;
	private Date data;
	private boolean status;
	private float valor_abertura;
	
	public CaixaBean()
	{
		
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getUsuario_nome()
	{
		return usuario_nome;
	}

	public void setUsuario_nome(String usuario_nome)
	{
		this.usuario_nome = usuario_nome;
	}

	public Date getData()
	{
		return data;
	}

	public void setData(Date data)
	{
		this.data = data;
	}

	public boolean getStatus()
	{
		return status;
	}

	public void setStatus(boolean status)
	{
		this.status = status;
	}

	public float getValor_abertura()
	{
		return valor_abertura;
	}

	public void setValor_abertura(float valor_abertura)
	{
		this.valor_abertura = valor_abertura;
	}


	

}
