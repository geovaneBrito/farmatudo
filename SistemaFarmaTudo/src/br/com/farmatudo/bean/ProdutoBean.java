package br.com.farmatudo.bean;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProdutoBean
{
	private int id;
	private int laboratorio;
	private String categoria;
	private String descricao;
	private double valor_custo;
	private int margem;
	private double valor_venda;
	private int quantidade;


	public ProdutoBean(int id, int laboratorio, String categoria,
			String descricao, double valor_custo, int margem,
			double valor_venda, int quantidade)
	{
		this.id = id;
		this.laboratorio = laboratorio;
		this.categoria = categoria;
		this.descricao = descricao;
		this.valor_custo = valor_custo;
		this.margem = margem;
		this.valor_venda = valor_venda;
		this.quantidade = quantidade;
	}

	public ProdutoBean()
	{

	}

	public int getQuantidade()
	{
		return quantidade;
	}

	public void setQuantidade(int quantidade)
	{
		this.quantidade = quantidade;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getLaboratorio()
	{
		return laboratorio;
	}

	public void setLaboratorio(int laboratorio)
	{
		this.laboratorio = laboratorio;
	}

	public String getCategoria()
	{
		return categoria;
	}

	public void setCategoria(String categoria)
	{
		this.categoria = categoria;
	}

	public String getDescricao()
	{
		return descricao;
	}

	public void setDescricao(String descricao)
	{
		this.descricao = descricao;
	}

	public double getValor_custo()
	{
		return valor_custo;
	}

	public void setValor_custo(double valor_custo)
	{
		this.valor_custo = valor_custo;
	}

	public int getMargem()
	{
		return margem;
	}

	public void setMargem(int margem)
	{
		this.margem = margem;
	}

	public double getValor_venda()
	{
		return valor_venda;
	}

	public void setValor_venda(double valor_venda)
	{
		this.valor_venda = valor_venda;
	}

	
}
