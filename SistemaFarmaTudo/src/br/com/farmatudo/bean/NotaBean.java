package br.com.farmatudo.bean;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class NotaBean
{
	private int id;
	private String numero_nota;
	private float total;
	private Date data;
	
	public NotaBean(){}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getNumero_nota()
	{
		return numero_nota;
	}

	public void setNumero_nota(String numero_nota)
	{
		this.numero_nota = numero_nota;
	}

	public float getTotal()
	{
		return total;
	}

	public void setTotal(float total)
	{
		this.total = total;
	}

	public Date getData()
	{
		return data;
	}


	public void setData(Date data)
	{
		this.data = data;
	};
	
}
