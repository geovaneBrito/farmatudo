package br.com.farmatudo.bean;

public class VendaConvenioBean
{
	private String empresa;
	private int venda_id;
	private String nome_funcionario;

	public VendaConvenioBean()
	{

	}

	public String getEmpresa()
	{
		return empresa;
	}

	public void setEmpresa(String empresa)
	{
		this.empresa = empresa;
	}

	public int getVenda_id()
	{
		return venda_id;
	}

	public void setVenda_id(int venda_id)
	{
		this.venda_id = venda_id;
	}

	public String getNome_funcionario()
	{
		return nome_funcionario;
	}

	public void setNome_funcionario(String nome_funcionario)
	{
		this.nome_funcionario = nome_funcionario;
	}

}
