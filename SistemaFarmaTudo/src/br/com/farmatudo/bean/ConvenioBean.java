package br.com.farmatudo.bean;

public class ConvenioBean
{
	private String empresa;

	public ConvenioBean()
	{
		
	}

	public String getEmpresa()
	{
		return empresa;
	}

	public void setEmpresa(String empresa)
	{
		this.empresa = empresa;
	}
	
	
}
