package br.com.farmatudo.bean;

import java.util.Date;

public class ProdutoNotaBean
{
	private int produto_id;
	private int nota_id;
	private Date data_criacao;
	
	
	public ProdutoNotaBean(){}
	
	
	public int getProduto_id()
	{
		return produto_id;
	}
	public void setProduto_id(int produto_id)
	{
		this.produto_id = produto_id;
	}
	public int getNota_id()
	{
		return nota_id;
	}
	public void setNota_id(int nota_id)
	{
		this.nota_id = nota_id;
	}
	public Date getData_criacao()
	{
		return data_criacao;
	}
	public void setData_criacao(Date data_criacao)
	{
		this.data_criacao = data_criacao;
	}
	
	
	
	
}
