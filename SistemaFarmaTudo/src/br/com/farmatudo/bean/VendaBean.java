package br.com.farmatudo.bean;

import java.util.Date;

public class VendaBean
{
	private int id;
	private String usuario;
	private double sub_total;
	private int desconto;
	private double valor_desconto;
	private Date data;

	public VendaBean()
	{
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getUsuario()
	{
		return usuario;
	}

	public void setUsuario(String usuario)
	{
		this.usuario = usuario;
	}

	public double getSub_total()
	{
		return sub_total;
	}

	public void setSub_total(double sub_total)
	{
		this.sub_total = sub_total;
	}

	public int getDesconto()
	{
		return desconto;
	}

	public void setDesconto(int desconto)
	{
		this.desconto = desconto;
	}

	public double getValor_desconto()
	{
		return valor_desconto;
	}

	public void setValor_desconto(double valor_desconto)
	{
		this.valor_desconto = valor_desconto;
	}

	public Date getData()
	{
		return data;
	}

	public void setData(Date data)
	{
		this.data = data;
	}

}
