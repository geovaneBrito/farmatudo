package br.com.farmatudo.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioBean
{
	private String nome;
	private String senha;
	private String tipo;

	public UsuarioBean(String nome, String senha, String tipo)
	{
		setNome(nome);
		setSenha(senha);
		setTipo(tipo);
	}

	public UsuarioBean()
	{
		
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = (nome != "" ? nome : "vazio");
	}

	public String getSenha()
	{
		return senha;
	}

	public void setSenha(String senha)
	{
		this.senha = (senha != "" ? senha : "vazio");
	}

	public String getTipo()
	{
		return tipo;
	}

	public void setTipo(String tipo)
	{
		this.tipo = (tipo != "" ? tipo : "vazio");
	}

	public void carregarUsuario(Connection conn)
	{
		String sqlSelect = "select * from usuario where nome = ? and senha = ?";
		PreparedStatement stm = null;
		ResultSet rs = null;
		try
		{
			stm = conn.prepareStatement(sqlSelect);
			stm.setString(1, getNome());
			stm.setString(2, getSenha());
			rs = stm.executeQuery();
			while (rs.next())
			{
				this.setTipo(rs.getString(3));

			}
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException e1)
			{
				System.out.print(e1.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException e1)
				{
					System.out.print(e1.getStackTrace());
				}
			}
		}

	}

}
