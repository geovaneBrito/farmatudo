package br.com.farmatudo.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe responsável pela conexão com banco de dados sqlServer
 */

public class AcessoBD
{
	// Usuario e senha padr�o
	private static String USER_BANCO = "root";
	private static String SENHA_BANCO = "";

	// -----------------------------------------------------------
	// Carrega driver JDBC - executada automaticamente quando a classe chamada
	// (Mesma para todos os objetos da classe)
	//
	static
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}

	// -----------------------------------------------------------
	// Obtem conex�o com o banco de dados
	public static Connection obtemConexao() throws SQLException
	{

		GravaTXT gravaTXT = new GravaTXT();
		
		String retornaAcesso = gravaTXT.lerTC();
		String[] camposSplits = retornaAcesso.split("[\\W]");
		if (!camposSplits[0].equals(""))
		{
			USER_BANCO = camposSplits[0];
			
		}
		if (!camposSplits[1].equals(""))
		{
			if (camposSplits[1].equals("branco"))
			{
				SENHA_BANCO = "";
				System.out.println(retornaAcesso + " branco");
			}
			else
			{
				SENHA_BANCO = camposSplits[1];
				
			}	
		}
		Connection connection = null;
		// DriveManager -> Serviço básico para a administração de um
		// conjunto de drivers
		// (http://docs.oracle.com/javase/7/docs/api/java/sql/DriverManager.html)
		connection = DriverManager.getConnection("jdbc:mysql://localhost/db_farmatudo_", USER_BANCO, SENHA_BANCO);

		if (connection != null)
		{
			System.out.println("Conectado com sucesso");
		} else
		{
			System.out.println("N�o foi conectado");
		}

		return connection;
	}
}
