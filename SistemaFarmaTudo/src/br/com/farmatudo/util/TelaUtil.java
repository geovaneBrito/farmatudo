package br.com.farmatudo.util;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.text.ParseException;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.text.MaskFormatter;

import br.com.farmatudo.views.TelaPrincipal;
import datechooser.beans.DateChooserCombo;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class TelaUtil
{

	public static ResourceBundle BUNDLE;
	
	public static Integer LANGUAGE = 0;
	
	/**Metodo que reecebe como parametro, uma consutla do banco, e o caminho do arquivo jasper. Depois imprimir relatorio*/
	public static void gerarRelatorio(ResultSet resultset, String p_caminhoJasper)
	{
		try
		{
			JRResultSetDataSource setDataSource = new JRResultSetDataSource(resultset);
			
			JasperPrint print = JasperFillManager.fillReport(p_caminhoJasper, new HashMap(),	setDataSource);
			JasperViewer jasperViewer = new JasperViewer(print, true);
			
			JDialog viewDialog =  new JDialog(new javax.swing.JFrame(),"Visualiza��o do Relat�rio", true); 
			viewDialog.setIconImage(Toolkit.getDefaultToolkit()
					.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
			viewDialog.setSize(930,700);   
			viewDialog.setLocationRelativeTo(null);  
			viewDialog.getContentPane().add(jasperViewer.getContentPane());
			viewDialog.setVisible(true);
	
		} catch (Exception e)
		{
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Problema com a gera��o do relatorio");
		}

	}
	
	public static  void AcaoTeclaButton(JDialog p_Comp, final JButton p_Button, int p_Tecla)
	{
		InputMap inputMap = p_Comp.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		inputMap.put(KeyStroke.getKeyStroke(p_Tecla, 0),"forward");
		p_Comp.getRootPane().setInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW, inputMap);
		p_Comp.getRootPane().getActionMap().put("forward", new AbstractAction(){
		    private static final long serialVersionUID = 1L;

		    public void actionPerformed(ActionEvent event) {
		    	p_Button.doClick();
		    }
		});
	}
	public static void AcaoTeclaButton(JDialog p_Comp, final JToggleButton p_Button, int p_Tecla)
	{
		InputMap inputMap = p_Comp.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		inputMap.put(KeyStroke.getKeyStroke(p_Tecla, 0),"forward");
		p_Comp.getRootPane().setInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW, inputMap);
		p_Comp.getRootPane().getActionMap().put("forward", new AbstractAction(){
		    private static final long serialVersionUID = 1L;

		    public void actionPerformed(ActionEvent event) {
		    	p_Button.doClick();
		    }
		});
		
	}
	
	/**Aumentar o tamanho dos componentes de um DateChooserCombo
	 * @param chooserCombo : componenten de data**/
	public static Dimension dateDimesion(DateChooserCombo chooserCombo)
	{
		
		Dimension size = chooserCombo.getCalendarPreferredSize();
		size.width += 90;
		return size;
	}
	
	public static void setaNimbus (Component comp)
	{
		ColorUIResource backgroundUI = new ColorUIResource(0xAFBDD4);
		ColorUIResource lightBackgroundUI = new ColorUIResource(0x3b5998);
		ColorUIResource textUI = new ColorUIResource(0x141823);
		ColorUIResource controlBackgroundUI = new ColorUIResource(0x6d84b4);
		ColorUIResource infoBackgroundUI = new ColorUIResource(0x3b5998);
		ColorUIResource infoUI = new ColorUIResource(0x3b5998);
		ColorUIResource focusUI = new ColorUIResource(0x3b5998);

		UIManager.put("control", backgroundUI); // tela de fundo
		UIManager.put("nimbusBase", lightBackgroundUI);// componentes selecionados
		UIManager.put("text", textUI); // texto
		UIManager.put("nimbusBlueGrey", controlBackgroundUI);// bot�es, menus, bordas de campos
		UIManager.put("info", infoUI); // campos de informa��o
		UIManager.put("nimbusInfoBlue", infoBackgroundUI);// n�o sei
		UIManager.put("nimbusFocus", focusUI);// borda o campo em foco
		//UIManager.put("nimbusLightBackground", lightBackgroundUI); // fundo de campos
		
		UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
		for (UIManager.LookAndFeelInfo look :looks) {
			if (look.getClassName().matches("(?i).*nimbus.*")) {
				try {
					UIManager.setLookAndFeel(look.getClassName());
					SwingUtilities.updateComponentTreeUI(comp);
					return;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	

	public static void initBundle()
	{
		if (LANGUAGE == 0)
		{
			BUNDLE = ResourceBundle.getBundle("EN");
		}
		else if (LANGUAGE == 1)
		{
			BUNDLE = ResourceBundle.getBundle("BR");
		}

	}
	
	public static String toUTF8 (String isoString)
	{
		String utf8 = null;
		if (null != isoString && !isoString.equals(""))
		{
			try
			{
				byte[] StringBytesIso = isoString.getBytes("ISO-8859-1");
				utf8 = new String ( StringBytesIso,"UTF-8" );
			} catch (UnsupportedEncodingException e)
			{
				System.out.println("Khong the chuyen sang UTF-8 : " + utf8 );
				utf8 = isoString;
			}
		}
		else
		{
			utf8 = isoString;
		}

		return utf8;



	}
	/** Metodo que formata Telefone **/

	public static MaskFormatter formataTelefone()
	{
		MaskFormatter mask = null;
		try
		{
			mask = new MaskFormatter("(##)-####-####");

		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return mask;

	}
	
	/** Metodo que formata Dinheiro **/
	public static MaskFormatter formataDinheiro()
	{
		MaskFormatter mask = null;
		try
		{
			mask = new MaskFormatter("#######");

		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return mask;

	}
	/** Metodo que formata Data **/

	public static MaskFormatter formataData()
	{
		MaskFormatter mask = null;
		try
		{
			mask = new MaskFormatter("##/##/##");

		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return mask;

	}
	

	/**
	 * Metodo que formata Cep
	 * **/

	public static MaskFormatter formataCep()
	{
		MaskFormatter mask = null;
		try
		{
			mask = new MaskFormatter("#####-###");

		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return mask;

	}
	
	/**
	 * Metodo que formata Cpf
	 * **/

	public static MaskFormatter formataCpf()
	{
		MaskFormatter mask = null;
		try
		{
			mask = new MaskFormatter("###.###.###-##");

		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return mask;

	}
	
	/**
	 * Metodo que formata Rg
	 * **/

	public static MaskFormatter formataRg()
	{
		MaskFormatter mask = null;
		try
		{
			mask = new MaskFormatter("##.###.###-#");

		} catch (ParseException e)
		{
			e.printStackTrace();
		}
		return mask;

	}


	


}
