package br.com.farmatudo.util;

import java.util.Date;
import java.util.Locale;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXMonthView;
import org.jdesktop.swingx.plaf.motif.MotifLookAndFeelAddons;

/**
 * Erweitert JOptionPane um eigene Dialoge
 */
@SuppressWarnings("serial")
public class CustomJOptionPane extends JOptionPane{
	private static String btnStringOk = "OK";
	private static String btnStringCancel = "Abbrechen";

	public static Date showDateInputDialog(String title, String msg, Date lowerBound,
			Date upperBound) {
		JXDatePicker datePicker = new JXDatePicker(Locale.GERMAN);
		datePicker.setMonthView(getMonthView(lowerBound, upperBound));
		datePicker.setDate(upperBound);

		Object[] components = {msg, datePicker};
		Object[] buttons = {btnStringOk, btnStringCancel};
		
		int answer = showOptionDialog(null, components, title,
				YES_NO_OPTION, QUESTION_MESSAGE, null,
				buttons, buttons[0]);
		if(YES_OPTION == answer){
			return datePicker.getDate();
		} else {
			return null;
		}
	}

	public static Date[] showDateRangeInputDialog(String title, String msg, Date lowerBound,
			Date upperBound) {
		JXDatePicker datePickerFrom = new JXDatePicker(Locale.GERMAN);
		datePickerFrom.setMonthView(getMonthView(lowerBound, upperBound));
		datePickerFrom.setDate(lowerBound);
		
		JXDatePicker datePickerTo = new JXDatePicker(Locale.GERMAN);
		datePickerTo.setMonthView(getMonthView(lowerBound, upperBound));
		datePickerTo.setDate(upperBound);

		Object[] components = {msg, "Von:", datePickerFrom, "Bis:", datePickerTo};
		Object[] buttons = {btnStringOk, btnStringCancel};
		
		int answer = showOptionDialog(null, components, title,
				YES_NO_OPTION, QUESTION_MESSAGE, null,
				buttons, buttons[0]);
		if(YES_OPTION == answer){
			Date[] dateRange = {datePickerFrom.getDate(), datePickerTo.getDate()};
			return dateRange;
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("deprecation")
	private static JXMonthView getMonthView(Date lowerBound, Date upperBound){
		JXMonthView datePickerRange = new JXMonthView();
		datePickerRange.setLowerBound(lowerBound);
		datePickerRange.setUpperBound(upperBound);
		boolean moreThanOneMonth = lowerBound.getMonth() != upperBound.getMonth()
				|| lowerBound.getYear() != upperBound.getYear();
		datePickerRange.setTraversable(moreThanOneMonth);
		return datePickerRange;
	}
}
