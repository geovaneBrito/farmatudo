package br.com.farmatudo.views;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import br.com.farmatudo.bean.UsuarioBean;
import br.com.farmatudo.util.AcessoBD;
import br.com.farmatudo.util.TelaUtil;

public class Login extends JDialog implements ActionListener
{

	private JTextField txt_Usuario;
	private JPasswordField ps_Senha;
	private JButton btn_Acessar;
	private JMenuBar menuBar;
	private JMenu mn_Sobre;
	private JMenuItem mntm_SobreOSistema;
	private JLabel lbl_Senha;
	private JLabel lbl_Usuario;
	private UsuarioBean usuarioBean;
	private JMenu mn_config;
	private JMenuItem mntm_configBD;

	/** Construtor da class Login **/
	public Login()
	{
		// configurando a��es da Janela
		TelaUtil.setaNimbus(this);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		setTitle("Acessar Sistema FarmaTudo");
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(397, 220);

		// instanciando GridBagLayout e adcionando na tela
		GridBagLayout containerGrid = new GridBagLayout();
		containerGrid.columnWidths = new int[] { 64, 14, 93, 57, 0 };
		containerGrid.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		containerGrid.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		containerGrid.rowWeights = new double[] { 1.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		getContentPane().setLayout(containerGrid);

		lbl_Usuario = new JLabel("Usu�rio: ");
		lbl_Usuario.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_Usuario = new GridBagConstraints();
		gbc_lbl_Usuario.anchor = GridBagConstraints.EAST;
		gbc_lbl_Usuario.gridwidth = 2;
		gbc_lbl_Usuario.fill = GridBagConstraints.VERTICAL;
		gbc_lbl_Usuario.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Usuario.gridx = 0;
		gbc_lbl_Usuario.gridy = 1;
		getContentPane().add(lbl_Usuario, gbc_lbl_Usuario);

		// criando e posicionado o JTextField para Usu�rio
		txt_Usuario = new JTextField();
		txt_Usuario.setText("geovane");
		txt_Usuario.setToolTipText("Digite o nome de usu�rio");
		txt_Usuario.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_txt_Usuario = new GridBagConstraints();
		gbc_txt_Usuario.insets = new Insets(0, 0, 5, 5);
		gbc_txt_Usuario.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_Usuario.gridx = 2;
		gbc_txt_Usuario.gridy = 1;
		getContentPane().add(txt_Usuario, gbc_txt_Usuario);
		txt_Usuario.setColumns(10);

		lbl_Senha = new JLabel("Senha: ");
		lbl_Senha.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_Senha = new GridBagConstraints();
		gbc_lbl_Senha.anchor = GridBagConstraints.EAST;
		gbc_lbl_Senha.gridwidth = 2;
		gbc_lbl_Senha.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Senha.gridx = 0;
		gbc_lbl_Senha.gridy = 2;
		getContentPane().add(lbl_Senha, gbc_lbl_Senha);

		// criando e posicionado o JPasswordField para Senha
		ps_Senha = new JPasswordField();
		ps_Senha.setEchoChar('*');
		ps_Senha.setText("123");
		ps_Senha.setToolTipText("Digite a senha de acesso");
		ps_Senha.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_ps_Senha = new GridBagConstraints();
		gbc_ps_Senha.insets = new Insets(0, 0, 5, 5);
		gbc_ps_Senha.fill = GridBagConstraints.HORIZONTAL;
		gbc_ps_Senha.gridx = 2;
		gbc_ps_Senha.gridy = 2;
		getContentPane().add(ps_Senha, gbc_ps_Senha);

		// criando e posicionado o bot�o e acessar
		btn_Acessar = new JButton("Acessar");
		btn_Acessar.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btn_Acessar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		GridBagConstraints gbc_btn_Acessar = new GridBagConstraints();
		gbc_btn_Acessar.insets = new Insets(0, 0, 0, 5);
		gbc_btn_Acessar.gridx = 2;
		gbc_btn_Acessar.gridy = 4;
		getContentPane().add(btn_Acessar, gbc_btn_Acessar);
		btn_Acessar.addActionListener(this);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mn_Sobre = new JMenu("Sobre ");
		mn_Sobre.setMnemonic('a');
		mn_Sobre.setToolTipText("Alt + a");
		mn_Sobre.setFont(new Font("Segoe UI", Font.PLAIN, 14));

		menuBar.add(mn_Sobre);

		mntm_SobreOSistema = new JMenuItem("Sobre o Sistema");
		mntm_SobreOSistema.addActionListener(this);
		mntm_SobreOSistema.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		mn_Sobre.add(mntm_SobreOSistema);

		mn_config = new JMenu("Configura��o do Sistema");
		mn_config.setMnemonic('c');
		mn_config.setToolTipText("Alt + c");
		mn_config.setFont(new Font("Segoe UI", Font.PLAIN, 14));

		menuBar.add(mn_config);
		mntm_configBD = new JMenuItem("Acesso ao Banco de Dados");
		mntm_configBD.addActionListener(this);
		mntm_configBD.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		mn_config.add(mntm_configBD);

		TelaUtil.AcaoTeclaButton(this, btn_Acessar, KeyEvent.VK_ENTER);

	}

	public void actionPerformed(ActionEvent e)
	{

		if (e.getSource() == mntm_SobreOSistema)
		{

			SobreSistema sobre = new SobreSistema();
			sobre.setVisible(true);
		}
		if (e.getSource() == btn_Acessar)
		{
			validaAcesso();

		}
		if (e.getSource() == mntm_configBD)
		{
			int respo = JOptionPane.showConfirmDialog(null,
					"Deseja realmente modificar o acesso ao Banco de Dados? Caso o acesso n�o seja permitido, poder� ter problema para acessar o sistema",
					"Importante", JOptionPane.WARNING_MESSAGE);
			if (respo >= 0)
			{
				NovoAcessoDB novoAcessoDB = new NovoAcessoDB();
				novoAcessoDB.setVisible(true);
				
			}
		}

	}

	private void validaAcesso()
	{
		if (txt_Usuario.getText().equals("") || ps_Senha.getText().equals(""))
		{
			JOptionPane.showMessageDialog(null, "Us�ario ou senha invalidos");

		} else
		{

			if (!verificaAcessouUsuario().equalsIgnoreCase("vazio"))
			{

				this.dispose();
				Toolkit toolkit = Toolkit.getDefaultToolkit();
				Dimension screenSize = toolkit.getScreenSize();
				TelaPrincipal telaprincipal = new TelaPrincipal(usuarioBean);

				telaprincipal.setBounds(0, 0, screenSize.width, screenSize.height);
				telaprincipal.setLocationRelativeTo(null);
				telaprincipal.setVisible(true);
			} else
			{
				JOptionPane.showMessageDialog(null, "Us�ario ou senha invalidos");
				txt_Usuario.setText("");
				ps_Senha.setText("");
				txt_Usuario.requestFocus();
				txt_Usuario.selectAll();

			}
		}
	}

	/**
	 * Metodo que valida acesso de usuario
	 * 
	 * @return
	 ***/
	private String verificaAcessouUsuario()
	{
		String retorno = "";
		Connection conn = null;
		try
		{
			AcessoBD bd = new AcessoBD();
			conn = bd.obtemConexao();
			usuarioBean = new UsuarioBean(txt_Usuario.getText().trim(), ps_Senha.getText().trim(), "");
			usuarioBean.carregarUsuario(conn);

			retorno = usuarioBean.getTipo();
			return retorno;

		} catch (Exception exc)
		{
			exc.printStackTrace();
			JOptionPane.showMessageDialog(null, "Erro ao conectar com o banco");

		}
		return retorno;
	}

}
