package br.com.farmatudo.views;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


import br.com.farmatudo.bean.ConvenioBean;
import br.com.farmatudo.bean.LaboratorioBean;
import br.com.farmatudo.bean.VendaBean;
import br.com.farmatudo.daos.ConvenioDao;
import br.com.farmatudo.daos.LaboratorioDao;
import br.com.farmatudo.daos.VendaConvenioDao;
import br.com.farmatudo.util.AcessoBD;
import br.com.farmatudo.util.TelaUtil;

import org.jdesktop.swingx.JXList;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import org.jdesktop.swingx.JXPanel;

public class Convenio extends JDialog implements ActionListener
{
	private JTextField txtNomeFuncionario;
	private boolean flagCadastro;
	private JButton btnSalvar;
	private JButton btnSair;
	private JComboBox comboBox_empresa;
	/**Contrutor da classe convenio**/
	public Convenio()
	{
		
		setSize(638,347);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(true);
		TelaUtil.setaNimbus(this);

		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{104, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		btnSalvar = new JButton();
		btnSalvar.setIcon(new ImageIcon(Convenio.class.getResource("/imagemIcon/icon_carrinho_finalizar.png")));
		GridBagConstraints gbc_btnSalvar = new GridBagConstraints();
		gbc_btnSalvar.insets = new Insets(0, 0, 5, 5);
		gbc_btnSalvar.gridx = 0;
		gbc_btnSalvar.gridy = 0;
		getContentPane().add(btnSalvar, gbc_btnSalvar);
		btnSalvar.addActionListener(this);
		
		btnSair = new JButton();
		btnSair.setIcon(new ImageIcon(Convenio.class.getResource("/imagemIcon/Log Out.png")));
		GridBagConstraints gbc_btnSair = new GridBagConstraints();
		gbc_btnSair.insets = new Insets(0, 0, 5, 5);
		gbc_btnSair.gridx = 1;
		gbc_btnSair.gridy = 0;
		getContentPane().add(btnSair, gbc_btnSair);
		btnSair.addActionListener(this);
		
		JXPanel panelDados = new JXPanel();
		panelDados.setBorder(BorderFactory.createTitledBorder("<html><h4>Dados do Funcionário</h4><html>"));
		GridBagConstraints gbc_panelDados = new GridBagConstraints();
		gbc_panelDados.gridheight = 2;
		gbc_panelDados.gridwidth = 4;
		gbc_panelDados.insets = new Insets(0, 0, 5, 0);
		gbc_panelDados.fill = GridBagConstraints.BOTH;
		gbc_panelDados.gridx = 0;
		gbc_panelDados.gridy = 1;
		getContentPane().add(panelDados, gbc_panelDados);
		GridBagLayout gbl_panelDados = new GridBagLayout();
		gbl_panelDados.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panelDados.rowHeights = new int[]{0, 0, 0, 0};
		gbl_panelDados.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_panelDados.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelDados.setLayout(gbl_panelDados);
		
		JLabel lblNomeDoFuncionario = new JLabel("Nome do Funcionário");
		GridBagConstraints gbc_lblNomeDoFuncionario = new GridBagConstraints();
		gbc_lblNomeDoFuncionario.gridwidth = 5;
		gbc_lblNomeDoFuncionario.insets = new Insets(0, 0, 5, 5);
		gbc_lblNomeDoFuncionario.gridx = 1;
		gbc_lblNomeDoFuncionario.gridy = 0;
		panelDados.add(lblNomeDoFuncionario, gbc_lblNomeDoFuncionario);
		lblNomeDoFuncionario.setFont(new Font("Tahoma", Font.PLAIN, 36));
		
		txtNomeFuncionario = new JTextField();
		GridBagConstraints gbc_txtNomeFuncionario = new GridBagConstraints();
		gbc_txtNomeFuncionario.insets = new Insets(0, 0, 5, 5);
		gbc_txtNomeFuncionario.gridwidth = 5;
		gbc_txtNomeFuncionario.fill = GridBagConstraints.BOTH;
		gbc_txtNomeFuncionario.gridx = 1;
		gbc_txtNomeFuncionario.gridy = 1;
		panelDados.add(txtNomeFuncionario, gbc_txtNomeFuncionario);
		txtNomeFuncionario.setFont(new Font("Tahoma", Font.PLAIN, 36));
		txtNomeFuncionario.setColumns(10);
		
		comboBox_empresa = new JComboBox();
		GridBagConstraints gbc_comboBox_empresa = new GridBagConstraints();
		gbc_comboBox_empresa.gridwidth = 5;
		gbc_comboBox_empresa.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_empresa.insets = new Insets(0, 0, 0, 5);
		gbc_comboBox_empresa.gridx = 1;
		gbc_comboBox_empresa.gridy = 2;
		panelDados.add(comboBox_empresa, gbc_comboBox_empresa);
		comboBox_empresa.setFont(new Font("Tahoma", Font.PLAIN, 36));
		
		carregarEmpresas();
   
		
	}
	/** Metodo que carrega todos as empresa e seta em um combobox **/
	private void carregarEmpresas()
	{
		Connection connection = null;
		ArrayList<ConvenioBean> todaLista = new ArrayList<ConvenioBean>();

		try
		{
			AcessoBD bd = new AcessoBD();
			connection = bd.obtemConexao();
			todaLista = ConvenioDao.carregarTodosConvenio(connection);
			for (ConvenioBean convenioBean : todaLista)
			{

				comboBox_empresa.addItem(convenioBean.getEmpresa());
			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	public boolean cadastroConvenio()
	{
		flagCadastro = false;
		setModal(true);
		setVisible(true);
		return flagCadastro;
		
		
	}
	@Override
	public void actionPerformed(ActionEvent event)
	{
		if (event.getSource() == btnSair)
		{
			dispose();
		}
		else if (event.getSource() == btnSalvar)
		{
			System.out.println(comboBox_empresa.getSelectedIndex());
			if (txtNomeFuncionario.getText().equals("") || !(comboBox_empresa.getSelectedIndex() > 0) )
			{
				JOptionPane.showMessageDialog(null, "Informe todos os campos", "Importante", JOptionPane.INFORMATION_MESSAGE);
			}
			try
			{
				Connection conn = null;
				AcessoBD acessoBD = new AcessoBD();
				conn = acessoBD.obtemConexao();
				if (VendaConvenioDao.inseriVendaConvenio(conn,  String.valueOf(comboBox_empresa.getSelectedItem()), txtNomeFuncionario.getText().trim()))
				{
					JOptionPane.showMessageDialog(null, "Venda concluida com sucesso","Sucesso", JOptionPane.INFORMATION_MESSAGE);
					flagCadastro = true;
					dispose();
				}
						
			} catch (Exception e)
			{
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Erro na venda","Importante", JOptionPane.ERROR_MESSAGE);
				flagCadastro = false;
			}
			
			
			flagCadastro = true;
			
		}
	}
}
