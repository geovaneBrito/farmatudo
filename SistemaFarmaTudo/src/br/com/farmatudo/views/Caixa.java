package br.com.farmatudo.views;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;

import br.com.farmatudo.bean.CaixaBean;
import br.com.farmatudo.bean.UsuarioBean;
import br.com.farmatudo.daos.CaixaDao;
import br.com.farmatudo.util.AcessoBD;
import br.com.farmatudo.util.JMoneyField;
import br.com.farmatudo.util.TelaUtil;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import org.jdesktop.swingx.JXDatePicker;
import javax.swing.ImageIcon;

public class Caixa extends JDialog implements ActionListener
{
	private JRadioButton rdbtnAberturaDePdv;
	private JRadioButton rdbtnFechamentoDePdv;
	private JPanel panelDados;
	private JLabel lblUsurio;
	private JTextField txtUsuario;
	private JLabel lblSenha;
	private JPasswordField psSenha;
	private JPanel panel;
	private JLabel lblSaldoInicial;
	private JTextField txtSaldoInicial;
	private JLabel lblData;
	private JXDatePicker datePicker_Data;
	private JButton btnSalvar;
	private JButton btnVoltar;
	private UsuarioBean usuarioBean;
	private boolean statusCaixa;
	private CaixaBean caixaBean;

	public Caixa(CaixaBean caixaBean, UsuarioBean usuarioLogado)
	{

		usuarioBean = usuarioLogado;
		this.caixaBean = caixaBean;
		setModal(true);
		setSize(885, 460);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(true);
		TelaUtil.setaNimbus(this);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 143, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 70, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		btnSalvar = new JButton();
		btnSalvar.setIcon(new ImageIcon(Caixa.class.getResource("/imagemIcon/guardar.png")));
		GridBagConstraints gbc_btnSalvar = new GridBagConstraints();
		gbc_btnSalvar.anchor = GridBagConstraints.WEST;
		gbc_btnSalvar.insets = new Insets(0, 0, 5, 5);
		gbc_btnSalvar.gridx = 0;
		gbc_btnSalvar.gridy = 0;
		getContentPane().add(btnSalvar, gbc_btnSalvar);
		btnSalvar.addActionListener(this);

		btnVoltar = new JButton();
		btnVoltar.setIcon(new ImageIcon(Caixa.class.getResource("/imagemIcon/voltar.png")));
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 1;
		gbc_button.gridy = 0;
		getContentPane().add(btnVoltar, gbc_button);
		btnVoltar.addActionListener(this);

		JPanel panelAcao = new JPanel();
		panelAcao.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		GridBagConstraints gbc_panelDados = new GridBagConstraints();
		gbc_panelDados.gridwidth = 3;
		gbc_panelDados.insets = new Insets(0, 0, 5, 5);
		gbc_panelDados.fill = GridBagConstraints.HORIZONTAL;
		gbc_panelDados.gridx = 0;
		gbc_panelDados.gridy = 2;
		getContentPane().add(panelAcao, gbc_panelDados);
		GridBagLayout gbl_panelDados = new GridBagLayout();
		gbl_panelDados.columnWidths = new int[] { 0, 0 };
		gbl_panelDados.rowHeights = new int[] { 55, 0, 55, 0 };
		gbl_panelDados.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelDados.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelAcao.setLayout(gbl_panelDados);

		ButtonGroup buttonGroup = new ButtonGroup();

		rdbtnAberturaDePdv = new JRadioButton("Abertura de PDV");
		GridBagConstraints gbc_rdbtnAberturaDePdv = new GridBagConstraints();
		gbc_rdbtnAberturaDePdv.fill = GridBagConstraints.HORIZONTAL;
		gbc_rdbtnAberturaDePdv.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnAberturaDePdv.gridx = 0;
		gbc_rdbtnAberturaDePdv.gridy = 0;
		panelAcao.add(rdbtnAberturaDePdv, gbc_rdbtnAberturaDePdv);
		rdbtnAberturaDePdv.setFont(new Font("Tahoma", Font.PLAIN, 30));
		buttonGroup.add(rdbtnAberturaDePdv);

		rdbtnFechamentoDePdv = new JRadioButton("Fechamento de PDV");
		GridBagConstraints gbc_rdbtnFechamentoDePdv = new GridBagConstraints();
		gbc_rdbtnFechamentoDePdv.fill = GridBagConstraints.HORIZONTAL;
		gbc_rdbtnFechamentoDePdv.gridx = 0;
		gbc_rdbtnFechamentoDePdv.gridy = 2;
		panelAcao.add(rdbtnFechamentoDePdv, gbc_rdbtnFechamentoDePdv);
		rdbtnFechamentoDePdv.setFont(new Font("Tahoma", Font.PLAIN, 30));
		buttonGroup.add(rdbtnFechamentoDePdv);

		panelDados = new JPanel();
		panelDados.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		GridBagConstraints gbc_panelDado = new GridBagConstraints();
		gbc_panelDado.gridwidth = 5;
		gbc_panelDado.insets = new Insets(0, 0, 5, 0);
		gbc_panelDado.fill = GridBagConstraints.BOTH;
		gbc_panelDado.gridx = 0;
		gbc_panelDado.gridy = 1;
		getContentPane().add(panelDados, gbc_panelDado);
		GridBagLayout gbl_panelDados1 = new GridBagLayout();
		gbl_panelDados1.columnWidths = new int[] { 0, 166, 110, 106, 50, 0 };
		gbl_panelDados1.rowHeights = new int[] { 84, 0 };
		gbl_panelDados1.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panelDados1.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panelDados.setLayout(gbl_panelDados1);

		lblUsurio = new JLabel("Usu�rio");
		lblUsurio.setFont(new Font("Tahoma", Font.PLAIN, 30));
		GridBagConstraints gbc_lblUsurio = new GridBagConstraints();
		gbc_lblUsurio.insets = new Insets(0, 0, 0, 5);
		gbc_lblUsurio.anchor = GridBagConstraints.WEST;
		gbc_lblUsurio.gridx = 0;
		gbc_lblUsurio.gridy = 0;
		panelDados.add(lblUsurio, gbc_lblUsurio);

		txtUsuario = new JTextField();
		txtUsuario.setEnabled(false);
		txtUsuario.setEditable(false);
		txtUsuario.setFont(new Font("Tahoma", Font.PLAIN, 30));
		GridBagConstraints gbc_txtUsuario = new GridBagConstraints();
		gbc_txtUsuario.insets = new Insets(0, 0, 0, 5);
		gbc_txtUsuario.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtUsuario.gridx = 1;
		gbc_txtUsuario.gridy = 0;
		panelDados.add(txtUsuario, gbc_txtUsuario);
		txtUsuario.setColumns(10);

		lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Tahoma", Font.PLAIN, 30));
		GridBagConstraints gbc_lblSenha = new GridBagConstraints();
		gbc_lblSenha.insets = new Insets(0, 0, 0, 5);
		gbc_lblSenha.anchor = GridBagConstraints.WEST;
		gbc_lblSenha.gridx = 3;
		gbc_lblSenha.gridy = 0;
		panelDados.add(lblSenha, gbc_lblSenha);

		// criando e posicionado o JPasswordField para Senha
		psSenha = new JPasswordField();
		psSenha.setEchoChar('*');
		psSenha.setFont(new Font("Tahoma", Font.PLAIN, 30));
		psSenha.setColumns(10);
		GridBagConstraints gbc_txtSenha = new GridBagConstraints();
		gbc_txtSenha.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSenha.gridx = 4;
		gbc_txtSenha.gridy = 0;
		panelDados.add(psSenha, gbc_txtSenha);

		panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridwidth = 2;
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel.gridx = 3;
		gbc_panel.gridy = 2;
		getContentPane().add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 160, 166, 0 };
		gbl_panel.rowHeights = new int[] { 63, 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		lblSaldoInicial = new JLabel("Saldo Inicial");
		lblSaldoInicial.setFont(new Font("Tahoma", Font.PLAIN, 30));
		GridBagConstraints gbc_lblFundaCaixa = new GridBagConstraints();
		gbc_lblFundaCaixa.anchor = GridBagConstraints.WEST;
		gbc_lblFundaCaixa.insets = new Insets(0, 0, 5, 5);
		gbc_lblFundaCaixa.gridx = 0;
		gbc_lblFundaCaixa.gridy = 0;
		panel.add(lblSaldoInicial, gbc_lblFundaCaixa);

		txtSaldoInicial = new JMoneyField();
		txtSaldoInicial.setText(String.valueOf(caixaBean.getValor_abertura()));
		txtSaldoInicial.setFont(new Font("Tahoma", Font.PLAIN, 30));
		txtSaldoInicial.setColumns(10);
		GridBagConstraints gbc_txtSaldoInicial = new GridBagConstraints();
		gbc_txtSaldoInicial.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtSaldoInicial.insets = new Insets(0, 0, 5, 0);
		gbc_txtSaldoInicial.gridx = 1;
		gbc_txtSaldoInicial.gridy = 0;
		panel.add(txtSaldoInicial, gbc_txtSaldoInicial);

		lblData = new JLabel("Data");
		lblData.setFont(new Font("Tahoma", Font.PLAIN, 30));
		GridBagConstraints gbc_lblData = new GridBagConstraints();
		gbc_lblData.anchor = GridBagConstraints.WEST;
		gbc_lblData.insets = new Insets(0, 0, 0, 5);
		gbc_lblData.gridx = 0;
		gbc_lblData.gridy = 2;
		panel.add(lblData, gbc_lblData);

		datePicker_Data = new JXDatePicker(System.currentTimeMillis());

		datePicker_Data.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		datePicker_Data.setFormats(new String[] { "dd/MM/yyy hh:mm" });
		datePicker_Data.setFont(new Font("Tahoma", Font.PLAIN, 30));
		GridBagConstraints gbc_datePicker_Data = new GridBagConstraints();
		gbc_datePicker_Data.fill = GridBagConstraints.HORIZONTAL;
		gbc_datePicker_Data.gridx = 1;
		gbc_datePicker_Data.gridy = 2;
		panel.add(datePicker_Data, gbc_datePicker_Data);

		if (caixaBean != null)
		{
			txtUsuario.setText(usuarioBean.getNome());
			if (caixaBean.getStatus())
			{
				rdbtnFechamentoDePdv.setSelected(true);

			} else
			{
				rdbtnAberturaDePdv.setSelected(true);
			}
		}

		rdbtnAberturaDePdv.addActionListener(this);
		rdbtnFechamentoDePdv.addActionListener(this);

	}

	public boolean aberturaDeCaixa()
	{
		statusCaixa = false;
		setModal(true);
		setVisible(true);
		return statusCaixa;
	}

	@Override
	public void actionPerformed(ActionEvent event)
	{

		if (event.getSource() == btnVoltar)
		{
			dispose();
			statusCaixa = false;
		}
		if (event.getSource() == btnSalvar)
		{

			if (!usuarioBean.getSenha().equals(psSenha.getText()))
			{
				JOptionPane.showMessageDialog(null, "Senha incorreta", "Importante", JOptionPane.ERROR_MESSAGE);
				return;
			}

			String saldoInicial = txtSaldoInicial.getText().replace(".", "");
			saldoInicial = saldoInicial.replace(",", ".");
			AcessoBD acessoBD = new AcessoBD();
			Connection conn = null;
			try
			{
				if (rdbtnAberturaDePdv.isSelected())
				{

					if (caixaBean.getUsuario_nome() == null)

					{

						caixaBean.setUsuario_nome(usuarioBean.getNome());
						caixaBean.setData(datePicker_Data.getDate());
						caixaBean.setStatus(true);
						caixaBean.setValor_abertura(Float.parseFloat(saldoInicial));
						conn = acessoBD.obtemConexao();
						if (CaixaDao.inserirCaixaAberto(caixaBean, conn))
						{
							JOptionPane.showMessageDialog(null, "Caixa aberto com sucesso", "Sucesso",
									JOptionPane.INFORMATION_MESSAGE);
							statusCaixa  = true;
							dispose();
						}
					}

					if (!caixaBean.getStatus() && caixaBean.getUsuario_nome() != null)
					{
						caixaBean.setUsuario_nome(usuarioBean.getNome());
						caixaBean.setStatus(true);
						caixaBean.setValor_abertura(Float.parseFloat(saldoInicial));

						conn = acessoBD.obtemConexao();
						if (CaixaDao.atualizarCaixaAberto(caixaBean, conn))
						{
							JOptionPane.showMessageDialog(null, "Caixa aberto com sucesso", "Sucesso",
									JOptionPane.INFORMATION_MESSAGE);
							statusCaixa  = true;
							dispose();
						}
					}

				} else if (rdbtnFechamentoDePdv.isSelected())
				{

					if (caixaBean.getUsuario_nome() != null)

					{
						System.out.println("Atualizar caixa " + caixaBean.getId());

						caixaBean.setData(datePicker_Data.getDate());
						caixaBean.setStatus(false);
						caixaBean.setValor_abertura(Float.parseFloat(saldoInicial));
						conn = acessoBD.obtemConexao();
						if (CaixaDao.atualizarCaixaAberto(caixaBean, conn))
						{
							JOptionPane.showMessageDialog(null, "Caixa Fechado com sucesso", "Sucesso",
									JOptionPane.INFORMATION_MESSAGE);
							dispose();
						}
					} else
					{

						JOptionPane.showMessageDialog(null, "Caixa n�o se encontra aberto", "Importante",
								JOptionPane.INFORMATION_MESSAGE);

					}

				}
			} catch (Exception e)
			{
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Erro ao conectar ao banco. Tente novamente mais tarde",
						"Importante", JOptionPane.ERROR_MESSAGE);
			}

		}
	}
}
