/**@author Igor Orlandi Matos****/
package br.com.farmatudo.views;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import br.com.farmatudo.bean.FornecedorBean;
import br.com.farmatudo.bean.LaboratorioBean;
import br.com.farmatudo.bean.ProdutoBean;
import br.com.farmatudo.daos.FornecedorDao;
import br.com.farmatudo.daos.LaboratorioDao;
import br.com.farmatudo.daos.ProdutoDao;
import br.com.farmatudo.util.AcessoBD;
import br.com.farmatudo.util.TelaUtil;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;

/** Classe que mostrar o relatorio de aulas do aluno selecionado **/
public class Fornecedor_Laboratorio extends JDialog implements MouseListener, KeyListener, ActionListener {
	private JPanel panelRelatorioFornecedores;
	private JScrollPane scrollPaneFornecedores;
	private JTable table_Fornecedores;
	private JTable table_Laboratorios;
	private JLabel lbl_Pesquisa;
	private JTextField txt_Pesquisa_Fornecedor;
	private JPanel panelFornecedor;
	private JRadioButton rdbtnNomeFornecedor;
	private JRadioButton rdbtnTelefoneFornecedor;
	private CadastroFornecedor cadastroFornecedor;
	private CadastroLaboratorio cadastroLaboratorio;
	private JButton btn_NovoFornecedor;
	private JPanel panelLaboratorio;
	private JRadioButton rdbtnNomeLaboratorio;
	private JLabel label;
	private JTextField txt_Pesquisa_Laboratorio;
	private JRadioButton rdbtnTelefoneLaboratorio;
	private JPanel panelRelatorioLaboratorio;
	private JScrollPane scrollPane;
	private JButton btn_NovoLaboratorio;
	private JScrollPane scrollPaneLaboratorios;
	private JTable table_Laboratorio;

	public Fornecedor_Laboratorio() {

		setTitle("Produtos");
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		setModal(true);
		setSize(962, 559);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		setResizable(true);
		TelaUtil.setaNimbus(this);
		setLocationRelativeTo(null);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 36, 50, 331, 64, 214, 97, 70, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 45, 140, 182, 38, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		panelFornecedor = new JPanel();
		panelFornecedor.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"<html><h4>Fornecedor</h4><html>", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_panelFornecedor = new GridBagConstraints();
		gbc_panelFornecedor.anchor = GridBagConstraints.WEST;
		gbc_panelFornecedor.gridwidth = 2;
		gbc_panelFornecedor.insets = new Insets(0, 0, 5, 5);
		gbc_panelFornecedor.fill = GridBagConstraints.VERTICAL;
		gbc_panelFornecedor.gridx = 1;
		gbc_panelFornecedor.gridy = 1;
		getContentPane().add(panelFornecedor, gbc_panelFornecedor);
		GridBagLayout gbl_panelFornecedor = new GridBagLayout();
		gbl_panelFornecedor.columnWidths = new int[] { 83, 31, 0, 122, 63, 0 };
		gbl_panelFornecedor.rowHeights = new int[] { 45, 26, 0 };
		gbl_panelFornecedor.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panelFornecedor.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		panelFornecedor.setLayout(gbl_panelFornecedor);

		ButtonGroup buttonGroup = new ButtonGroup();

		rdbtnNomeFornecedor = new JRadioButton("Nome");
		rdbtnNomeFornecedor.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		rdbtnNomeFornecedor.setVerticalAlignment(SwingConstants.BOTTOM);
		GridBagConstraints gbc_rdbtnNomeFornecedor = new GridBagConstraints();
		gbc_rdbtnNomeFornecedor.anchor = GridBagConstraints.EAST;
		gbc_rdbtnNomeFornecedor.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnNomeFornecedor.gridx = 0;
		gbc_rdbtnNomeFornecedor.gridy = 0;
		panelFornecedor.add(rdbtnNomeFornecedor, gbc_rdbtnNomeFornecedor);
		buttonGroup.add(rdbtnNomeFornecedor);

		txt_Pesquisa_Fornecedor = new JTextField();
		txt_Pesquisa_Fornecedor.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		txt_Pesquisa_Fornecedor.setColumns(10);
		txt_Pesquisa_Fornecedor.selectAll();
		txt_Pesquisa_Fornecedor.requestFocus();

		rdbtnTelefoneFornecedor = new JRadioButton("Telefone\r\n");
		rdbtnTelefoneFornecedor.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		rdbtnTelefoneFornecedor.setVerticalAlignment(SwingConstants.BOTTOM);
		GridBagConstraints gbc_rdbtnTelefoneFornecedor = new GridBagConstraints();
		gbc_rdbtnTelefoneFornecedor.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnTelefoneFornecedor.gridx = 2;
		gbc_rdbtnTelefoneFornecedor.gridy = 0;
		panelFornecedor.add(rdbtnTelefoneFornecedor, gbc_rdbtnTelefoneFornecedor);
		buttonGroup.add(rdbtnTelefoneFornecedor);

		btn_NovoFornecedor = new JButton();
		GridBagConstraints gbc_btn_NovoFornecedor = new GridBagConstraints();
		gbc_btn_NovoFornecedor.fill = GridBagConstraints.VERTICAL;
		gbc_btn_NovoFornecedor.insets = new Insets(0, 0, 5, 0);
		gbc_btn_NovoFornecedor.gridx = 4;
		gbc_btn_NovoFornecedor.gridy = 0;
		panelFornecedor.add(btn_NovoFornecedor, gbc_btn_NovoFornecedor);
		btn_NovoFornecedor.setIcon(new ImageIcon(Produto.class.getResource("/imagemIcon/Add.png")));
		btn_NovoFornecedor.addActionListener(this);

		lbl_Pesquisa = new JLabel("Buscar: ");
		GridBagConstraints gbc_lbl_Pesquisa = new GridBagConstraints();
		gbc_lbl_Pesquisa.insets = new Insets(0, 0, 0, 5);
		gbc_lbl_Pesquisa.gridx = 0;
		gbc_lbl_Pesquisa.gridy = 1;
		panelFornecedor.add(lbl_Pesquisa, gbc_lbl_Pesquisa);
		lbl_Pesquisa.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_txt_Pesquisa_Fornecedor = new GridBagConstraints();
		gbc_txt_Pesquisa_Fornecedor.insets = new Insets(0, 0, 0, 5);
		gbc_txt_Pesquisa_Fornecedor.fill = GridBagConstraints.BOTH;
		gbc_txt_Pesquisa_Fornecedor.gridwidth = 3;
		gbc_txt_Pesquisa_Fornecedor.gridx = 1;
		gbc_txt_Pesquisa_Fornecedor.gridy = 1;
		panelFornecedor.add(txt_Pesquisa_Fornecedor, gbc_txt_Pesquisa_Fornecedor);
		txt_Pesquisa_Fornecedor.addKeyListener(this);

		panelLaboratorio = new JPanel();
		panelLaboratorio.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"<html><h4>Laboratorio</h4><html>", TitledBorder.LEFT, TitledBorder.TOP, null, new Color(0, 0, 0)));
		GridBagConstraints gbc_panelLaboratorio = new GridBagConstraints();
		gbc_panelLaboratorio.gridwidth = 3;
		gbc_panelLaboratorio.insets = new Insets(0, 0, 5, 5);
		gbc_panelLaboratorio.fill = GridBagConstraints.BOTH;
		gbc_panelLaboratorio.gridx = 4;
		gbc_panelLaboratorio.gridy = 1;
		getContentPane().add(panelLaboratorio, gbc_panelLaboratorio);
		GridBagLayout gbl_panelLaboratorio = new GridBagLayout();
		gbl_panelLaboratorio.columnWidths = new int[] { 89, 47, 0, 122, 63, 0 };
		gbl_panelLaboratorio.rowHeights = new int[] { 45, 26, 0 };
		gbl_panelLaboratorio.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panelLaboratorio.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		panelLaboratorio.setLayout(gbl_panelLaboratorio);

		rdbtnNomeLaboratorio = new JRadioButton("Nome");
		rdbtnNomeLaboratorio.setVerticalAlignment(SwingConstants.BOTTOM);
		rdbtnNomeLaboratorio.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_rdbtnNomeLaboratorio = new GridBagConstraints();
		gbc_rdbtnNomeLaboratorio.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnNomeLaboratorio.gridx = 0;
		gbc_rdbtnNomeLaboratorio.gridy = 0;
		panelLaboratorio.add(rdbtnNomeLaboratorio, gbc_rdbtnNomeLaboratorio);
		buttonGroup.add(rdbtnNomeLaboratorio);

		btn_NovoLaboratorio = new JButton();
		btn_NovoLaboratorio.addActionListener(this);

		rdbtnTelefoneLaboratorio = new JRadioButton("Telefone\r\n");
		rdbtnTelefoneLaboratorio.setVerticalAlignment(SwingConstants.BOTTOM);
		rdbtnTelefoneLaboratorio.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_rdbtnTelefoneLaboratorio = new GridBagConstraints();
		gbc_rdbtnTelefoneLaboratorio.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnTelefoneLaboratorio.gridx = 1;
		gbc_rdbtnTelefoneLaboratorio.gridy = 0;
		panelLaboratorio.add(rdbtnTelefoneLaboratorio, gbc_rdbtnTelefoneLaboratorio);
		buttonGroup.add(rdbtnTelefoneLaboratorio);
		GridBagConstraints gbc_btn_NovoLaboratorio = new GridBagConstraints();
		gbc_btn_NovoLaboratorio.fill = GridBagConstraints.VERTICAL;
		gbc_btn_NovoLaboratorio.insets = new Insets(0, 0, 5, 0);
		gbc_btn_NovoLaboratorio.gridx = 4;
		gbc_btn_NovoLaboratorio.gridy = 0;
		panelLaboratorio.add(btn_NovoLaboratorio, gbc_btn_NovoLaboratorio);
		btn_NovoLaboratorio.setIcon(new ImageIcon(Fornecedor_Laboratorio.class.getResource("/imagemIcon/Add.png")));

		label = new JLabel("Buscar: ");
		label.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 0, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 1;
		panelLaboratorio.add(label, gbc_label);

		txt_Pesquisa_Laboratorio = new JTextField();
		txt_Pesquisa_Laboratorio.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		txt_Pesquisa_Laboratorio.setColumns(10);
		GridBagConstraints gbc_txt_Pesquisa_Laboratori = new GridBagConstraints();
		gbc_txt_Pesquisa_Laboratori.fill = GridBagConstraints.BOTH;
		gbc_txt_Pesquisa_Laboratori.gridwidth = 3;
		gbc_txt_Pesquisa_Laboratori.insets = new Insets(0, 0, 0, 5);
		gbc_txt_Pesquisa_Laboratori.gridx = 1;
		gbc_txt_Pesquisa_Laboratori.gridy = 1;
		panelLaboratorio.add(txt_Pesquisa_Laboratorio, gbc_txt_Pesquisa_Laboratori);
		txt_Pesquisa_Laboratorio.addKeyListener(this);

		panelRelatorioFornecedores = new JPanel();
		panelRelatorioFornecedores.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"<html><h4>Lista de Fornecedores</h4><html>", TitledBorder.LEFT, TitledBorder.TOP, null,
				new Color(0, 0, 0)));
		GridBagConstraints gbc_panelRelatorioFornecedores = new GridBagConstraints();
		gbc_panelRelatorioFornecedores.insets = new Insets(0, 0, 5, 5);
		gbc_panelRelatorioFornecedores.gridwidth = 2;
		gbc_panelRelatorioFornecedores.fill = GridBagConstraints.BOTH;
		gbc_panelRelatorioFornecedores.gridx = 1;
		gbc_panelRelatorioFornecedores.gridy = 2;
		getContentPane().add(panelRelatorioFornecedores, gbc_panelRelatorioFornecedores);
		GridBagLayout gbl_panelRelatorioFornecedores = new GridBagLayout();
		gbl_panelRelatorioFornecedores.columnWidths = new int[] { 289, 0, 0, 0, 0 };
		gbl_panelRelatorioFornecedores.rowHeights = new int[] { 234, 0 };
		gbl_panelRelatorioFornecedores.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelRelatorioFornecedores.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panelRelatorioFornecedores.setLayout(gbl_panelRelatorioFornecedores);

		scrollPaneFornecedores = new JScrollPane();

		GridBagConstraints gbc_scrollPaneFornecedores = new GridBagConstraints();
		gbc_scrollPaneFornecedores.gridwidth = 4;
		gbc_scrollPaneFornecedores.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPaneFornecedores.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneFornecedores.gridx = 0;
		gbc_scrollPaneFornecedores.gridy = 0;
		panelRelatorioFornecedores.add(scrollPaneFornecedores, gbc_scrollPaneFornecedores);

		table_Fornecedores = new JTable();
		table_Fornecedores.setBorder(new EmptyBorder(0, 0, 0, 0));
		scrollPaneFornecedores.setViewportView(table_Fornecedores);
		table_Fornecedores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_Fornecedores.setAutoCreateRowSorter(true);
		table_Fornecedores.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		table_Fornecedores.setModel(
				new DefaultTableModel(new Object[][] { { null, null }, }, new String[] { "Nome", "Telefone" }) {
					boolean[] columnEditables = new boolean[] { false, false };

					public boolean isCellEditable(int row, int column) {
						return columnEditables[column];
					}
				});

		panelRelatorioLaboratorio = new JPanel();
		panelRelatorioLaboratorio.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"<html><h4>Lista de Laboratorios</h4><html>", TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(0, 0, 0)));
		GridBagConstraints gbc_panelRelatorioLaboratorio = new GridBagConstraints();
		gbc_panelRelatorioLaboratorio.gridwidth = 3;
		gbc_panelRelatorioLaboratorio.insets = new Insets(0, 0, 5, 5);
		gbc_panelRelatorioLaboratorio.fill = GridBagConstraints.BOTH;
		gbc_panelRelatorioLaboratorio.gridx = 4;
		gbc_panelRelatorioLaboratorio.gridy = 2;
		getContentPane().add(panelRelatorioLaboratorio, gbc_panelRelatorioLaboratorio);
		GridBagLayout gbl_panelRelatorioLaboratorio = new GridBagLayout();
		gbl_panelRelatorioLaboratorio.columnWidths = new int[] { 216, 2, 0 };
		gbl_panelRelatorioLaboratorio.rowHeights = new int[] { 42, 2, 0 };
		gbl_panelRelatorioLaboratorio.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_panelRelatorioLaboratorio.rowWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		panelRelatorioLaboratorio.setLayout(gbl_panelRelatorioLaboratorio);

		scrollPaneLaboratorios = new JScrollPane();
		GridBagConstraints gbc_scrollPaneLaboratorios = new GridBagConstraints();
		gbc_scrollPaneLaboratorios.gridheight = 2;
		gbc_scrollPaneLaboratorios.gridwidth = 2;
		gbc_scrollPaneLaboratorios.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPaneLaboratorios.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneLaboratorios.gridx = 0;
		gbc_scrollPaneLaboratorios.gridy = 0;
		panelRelatorioLaboratorio.add(scrollPaneLaboratorios, gbc_scrollPaneLaboratorios);

		table_Laboratorio = new JTable();
		table_Laboratorio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent event) {
				if (event.getClickCount() == 2) {
					int row = table_Laboratorio.getSelectedRow();
					DefaultTableModel tableModel = (DefaultTableModel) table_Laboratorio.getModel();
					Connection conn = null;
					AcessoBD acessoBD = new AcessoBD();
					try {
						conn = acessoBD.obtemConexao();
						LaboratorioBean laboratorioBean = LaboratorioDao.buscaLaboratorioExpecifico(conn,
								String.valueOf(tableModel.getValueAt(row, 1)), String.valueOf(tableModel.getValueAt(row, 0)));
						
						cadastroLaboratorio = new CadastroLaboratorio(laboratorioBean);
						cadastroLaboratorio.setResizable(true);
						cadastroLaboratorio.setLocationRelativeTo(null);
						cadastroLaboratorio.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
						if (cadastroLaboratorio.cadastroLaboratorio()) {
							carregaFornecedoresTable();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		});
		table_Laboratorio.setBorder(new EmptyBorder(0, 0, 0, 0));
		table_Laboratorio.setModel(
				new DefaultTableModel(new Object[][] { { null, null }, }, new String[] { "Nome", "Telefone" }) {
					boolean[] columnEditables = new boolean[] { false, false };

					public boolean isCellEditable(int row, int column) {
						return columnEditables[column];
					}
				});
		table_Laboratorio.getColumnModel().getColumn(0).setPreferredWidth(30);
		table_Laboratorio.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_Laboratorio.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		table_Laboratorio.setAutoCreateRowSorter(true);
		scrollPaneLaboratorios.setViewportView(table_Laboratorio);

		GridBagConstraints gbc_scrollPaneLaboratorio = new GridBagConstraints();
		gbc_scrollPaneLaboratorio.gridwidth = 2;
		gbc_scrollPaneLaboratorio.anchor = GridBagConstraints.NORTHWEST;
		gbc_scrollPaneLaboratorio.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPaneLaboratorio.fill = GridBagConstraints.VERTICAL;
		gbc_scrollPaneLaboratorio.gridx = 0;
		gbc_scrollPaneLaboratorio.gridy = 1;

		table_Fornecedores.getColumnModel().getColumn(0).setPreferredWidth(140);
		table_Fornecedores.getColumnModel().getColumn(1).setPreferredWidth(140);
		table_Fornecedores.addMouseListener(this);

		carregaFornecedoresTable();
		carregaLaboratoriosTable();

	}

	/**
	 * 
	 * Metodo que carrega os Fornecedores no Table
	 * 
	 */
	private void carregaFornecedoresTable() {
		Connection conn = null;
		try {

			AcessoBD bd = new AcessoBD();
			conn = bd.obtemConexao();
			DefaultTableModel dtm = (DefaultTableModel) table_Fornecedores.getModel();

			while (dtm.getRowCount() > 0) {
				dtm.removeRow(0);
			}
			for (FornecedorBean fornecedor : FornecedorDao.carregarFornecedores(conn)) {
				System.out.println("Fornecedor" + fornecedor.getNome());
				adcionaLinhasFornecedor(dtm, fornecedor);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que acionada linhas na tabela Fornecedor, conforme consulta no
	 * banco
	 ****/
	private void adcionaLinhasFornecedor(DefaultTableModel dtm, FornecedorBean fornecedor) {

		dtm.addRow(new Object[] { fornecedor.getNome(), fornecedor.getTelefone() });
	}

	/**
	 * 
	 * Metodo que carrega os Laboratorios no Table
	 * 
	 */
	private void carregaLaboratoriosTable() {
		Connection conn = null;
		try {

			AcessoBD bd = new AcessoBD();
			conn = bd.obtemConexao();
			DefaultTableModel dtm = (DefaultTableModel) table_Laboratorio.getModel();

			while (dtm.getRowCount() > 0) {
				dtm.removeRow(0);
			}
			for (LaboratorioBean laboratorio : LaboratorioDao.carregarLaboratorios(conn)) {

				adicionaLinhasLaboratorio(dtm, laboratorio);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metodo que acionada linhas na tabela Fornecedor, conforme consulta no
	 * banco
	 ****/
	private void adicionaLinhasLaboratorio(DefaultTableModel dtm, LaboratorioBean laboratorio) {

		dtm.addRow(new Object[] { laboratorio.getNome(), laboratorio.getTelefone() });
	}

	public void mouseClicked(MouseEvent event) {

		if (event.getClickCount() == 2) {
			int row = table_Fornecedores.getSelectedRow();
			DefaultTableModel tableModel = (DefaultTableModel) this.table_Fornecedores.getModel();

			Connection conn = null;
			AcessoBD acessoBD = new AcessoBD();
			try {
				conn = acessoBD.obtemConexao();

				FornecedorBean fornecedorBean = FornecedorDao.buscaFornecedorExpecifico(conn,
						String.valueOf(tableModel.getValueAt(row, 1)), String.valueOf(tableModel.getValueAt(row, 0)));
				cadastroFornecedor = new CadastroFornecedor(fornecedorBean);
				cadastroFornecedor.setResizable(true);
				cadastroFornecedor.setLocationRelativeTo(null);
				cadastroFornecedor.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				if (cadastroFornecedor.cadastroProduto()) {

					carregaFornecedoresTable();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void keyPressed(KeyEvent event) {
		if (txt_Pesquisa_Fornecedor.getText().length() > 0) {
			if (event.getKeyCode() == KeyEvent.VK_ENTER) {
				txt_Pesquisa_Laboratorio.setText("");
				carregaLaboratoriosTable();

				String pesquisa = "";
				pesquisa = txt_Pesquisa_Fornecedor.getText().trim();

				if (pesquisa.length() == 0) {
					carregaFornecedoresTable();
				} else {
					try {
						if (rdbtnNomeFornecedor.isSelected()) {
							Connection conn = null;
							AcessoBD bd = new AcessoBD();
							try {
								conn = bd.obtemConexao();
								DefaultTableModel dtm = (DefaultTableModel) table_Fornecedores.getModel();

								while (dtm.getRowCount() > 0) {
									dtm.removeRow(0);
								}
								String nome = "%" + pesquisa + "%";
								for (FornecedorBean fornecedorBean : FornecedorDao.carregarFornecedoresPorNome(conn,
										nome)) {
									
									adcionaLinhasFornecedor(dtm, fornecedorBean);
								}

							} catch (Exception e) {
								e.printStackTrace();
							}

						} else if (rdbtnTelefoneFornecedor.isSelected()) {

							Connection conn = null;
							AcessoBD bd = new AcessoBD();
							try {
								conn = bd.obtemConexao();
								DefaultTableModel dtm = (DefaultTableModel) table_Fornecedores.getModel();

								while (dtm.getRowCount() > 0) {
									dtm.removeRow(0);
								}
								String phone = "%" + pesquisa + "%";
								for (FornecedorBean fornecedorBean : FornecedorDao.carregarFornecedoresPorTelefone(conn,
										phone)) {
									System.out.println("Fornecedor" + fornecedorBean.getNome());
									adcionaLinhasFornecedor(dtm, fornecedorBean);
								}

							} catch (Exception e) {
								e.printStackTrace();
							}

						}

					} catch (Exception e) {
						System.out.println("ERRO");
					}
				}
			}
		}

		if (txt_Pesquisa_Laboratorio.getText().length() > 0) {
			txt_Pesquisa_Fornecedor.setText("");
			if (event.getKeyCode() == KeyEvent.VK_ENTER) {

				carregaFornecedoresTable();

				String pesquisa = "";
				pesquisa = txt_Pesquisa_Laboratorio.getText().trim();

				System.out.println("pesquisa.length()" + pesquisa.length());
				if (pesquisa.length() == 0) {
					carregaLaboratoriosTable();
				} else {
					try {
						if (rdbtnNomeLaboratorio.isSelected()) {
							Connection conn = null;
							AcessoBD bd = new AcessoBD();
							try {
								conn = bd.obtemConexao();
								DefaultTableModel dtm = (DefaultTableModel) table_Laboratorio.getModel();

								while (dtm.getRowCount() > 0) {
									dtm.removeRow(0);
								}
								String nome = "%" + pesquisa + "%";
								for (LaboratorioBean laboratorioBean : LaboratorioDao.carregarLaboratorioPorNome(conn,
										nome)) {
									adicionaLinhasLaboratorio(dtm, laboratorioBean);
								}

							} catch (Exception e) {
								e.printStackTrace();
							}

						} else if (rdbtnTelefoneLaboratorio.isSelected()) {

							Connection conn = null;
							AcessoBD bd = new AcessoBD();
							try {
								conn = bd.obtemConexao();
								DefaultTableModel dtm = (DefaultTableModel) table_Laboratorio.getModel();

								while (dtm.getRowCount() > 0) {
									dtm.removeRow(0);
								}
								String phone = "%" + pesquisa + "%";

								for (LaboratorioBean laboratorioBean : LaboratorioDao
										.carregarLaboratorioPorTelefone(conn, phone)) {
									adicionaLinhasLaboratorio(dtm, laboratorioBean);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}

						}

					} catch (Exception e) {
						System.out.println("ERRO");
					}
				}
			}
		}

	}

	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == btn_NovoFornecedor) {
			cadastroFornecedor = new CadastroFornecedor(null);
			cadastroFornecedor.setResizable(true);
			cadastroFornecedor.setLocationRelativeTo(null);
			cadastroFornecedor.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			if (cadastroFornecedor.cadastroProduto()) {
				carregaFornecedoresTable();
			}
		}
		if (event.getSource() == btn_NovoLaboratorio) {
			cadastroLaboratorio = new CadastroLaboratorio(null);
			cadastroLaboratorio.setResizable(true);
			cadastroLaboratorio.setLocationRelativeTo(null);
			cadastroLaboratorio.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			if (cadastroLaboratorio.cadastroLaboratorio()) {
				carregaLaboratoriosTable();

			}
		}

	}

}
