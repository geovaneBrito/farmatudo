package br.com.farmatudo.views;

import javax.swing.BorderFactory;
import javax.swing.JDialog;

import java.awt.GridBagLayout;

import javax.swing.JPanel;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.Font;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import br.com.farmatudo.bean.CategoriaBean;
import br.com.farmatudo.bean.FornecedorBean;
import br.com.farmatudo.bean.LaboratorioBean;
import br.com.farmatudo.bean.ProdutoBean;
import br.com.farmatudo.daos.CategoriaDao;
import br.com.farmatudo.daos.FornecedorDao;
import br.com.farmatudo.daos.LaboratorioDao;
import br.com.farmatudo.daos.ProdutoDao;
import br.com.farmatudo.util.AcessoBD;
import br.com.farmatudo.util.TelaUtil;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

public class CadastroFornecedor extends JDialog implements ActionListener
{
	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private JTextField txt_Nome;
	private JTextField txt_Telefone;
	private JButton btn_Editar;
	private JButton btn_Salvar;
	private JButton btn_Volta;
	private boolean flagEditar = false;
	private String idProduto = "";
	private boolean flagSalvo;
	private FornecedorBean beanFornecedor;

	public CadastroFornecedor(FornecedorBean p_Object)
	{
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		setTitle("Cadastro de Fornecedores");
		setModal(true);
		setSize(695, 371);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(true);
		TelaUtil.setaNimbus(this);
		beanFornecedor = p_Object;

		GridBagLayout gridPrincipal = new GridBagLayout();
		gridPrincipal.columnWidths = new int[] { 55, 61, 61, 0, 34, 0 };
		gridPrincipal.rowHeights = new int[] { 52, 0, 36, 0 };
		gridPrincipal.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		gridPrincipal.rowWeights = new double[] { 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		getContentPane().setLayout(gridPrincipal);

		btn_Salvar = new JButton();
		btn_Salvar.setIcon(new ImageIcon(CadastroProduto.class
				.getResource("/imagemIcon/guardar.png")));
		GridBagConstraints gbc_btn_Salvar = new GridBagConstraints();
		gbc_btn_Salvar.insets = new Insets(0, 0, 5, 5);
		gbc_btn_Salvar.gridx = 1;
		gbc_btn_Salvar.gridy = 0;
		getContentPane().add(btn_Salvar, gbc_btn_Salvar);
		btn_Salvar.setMnemonic('s');
		btn_Salvar.addActionListener(this);

		btn_Editar = new JButton();
		btn_Editar.setIcon(new ImageIcon(CadastroProduto.class
				.getResource("/imagemIcon/editar.png")));
		btn_Editar.setMnemonic('e');
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.anchor = GridBagConstraints.WEST;
		gbc_button_1.insets = new Insets(0, 0, 5, 5);
		gbc_button_1.gridx = 2;
		gbc_button_1.gridy = 0;
		getContentPane().add(btn_Editar, gbc_button_1);
		btn_Editar.addActionListener(this);

		btn_Volta = new JButton();
		btn_Volta.setIcon(new ImageIcon(CadastroProduto.class
				.getResource("/imagemIcon/voltar.png")));
		GridBagConstraints gbc_btn_Volta = new GridBagConstraints();
		gbc_btn_Volta.anchor = GridBagConstraints.WEST;
		gbc_btn_Volta.insets = new Insets(0, 0, 5, 5);
		gbc_btn_Volta.gridx = 3;
		gbc_btn_Volta.gridy = 0;
		getContentPane().add(btn_Volta, gbc_btn_Volta);
		btn_Volta.addActionListener(this);

		JPanel panelDescricao = new JPanel();
		panelDescricao.setBorder(new TitledBorder(null,
				"<html><h4>Dados de Fornecedores</h4><html>",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panelDescricao = new GridBagConstraints();
		gbc_panelDescricao.gridwidth = 3;
		gbc_panelDescricao.insets = new Insets(0, 0, 5, 5);
		gbc_panelDescricao.fill = GridBagConstraints.BOTH;
		gbc_panelDescricao.gridx = 1;
		gbc_panelDescricao.gridy = 1;
		getContentPane().add(panelDescricao, gbc_panelDescricao);
		GridBagLayout gbl_panelDescricao = new GridBagLayout();
		gbl_panelDescricao.columnWidths = new int[] { 95, 0, 94, 0, 0 };
		gbl_panelDescricao.rowHeights = new int[] { 38, 29, 23, 35, 35, 0 };
		gbl_panelDescricao.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0,
				Double.MIN_VALUE };
		gbl_panelDescricao.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		panelDescricao.setLayout(gbl_panelDescricao);

		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNome = new GridBagConstraints();
		gbc_lblNome.anchor = GridBagConstraints.EAST;
		gbc_lblNome.insets = new Insets(0, 0, 5, 5);
		gbc_lblNome.gridx = 0;
		gbc_lblNome.gridy = 1;
		panelDescricao.add(lblNome, gbc_lblNome);

		txt_Nome = new JTextField();
		txt_Nome.setEditable(false);
		txt_Nome.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txt_Nome.setToolTipText("Quantidade de produto");
		txt_Nome.setColumns(10);
		GridBagConstraints gbc_txt_Nome = new GridBagConstraints();
		gbc_txt_Nome.insets = new Insets(0, 0, 5, 5);
		gbc_txt_Nome.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_Nome.gridx = 1;
		gbc_txt_Nome.gridy = 1;
		panelDescricao.add(txt_Nome, gbc_txt_Nome);
		ImageIcon icon = new ImageIcon("/imagemIcon/Add.png");

		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblTelefone = new GridBagConstraints();
		gbc_lblTelefone.anchor = GridBagConstraints.EAST;
		gbc_lblTelefone.insets = new Insets(0, 0, 5, 5);
		gbc_lblTelefone.gridx = 0;
		gbc_lblTelefone.gridy = 2;
		panelDescricao.add(lblTelefone, gbc_lblTelefone);

		txt_Telefone = new JTextField();
		txt_Telefone.setEditable(false);
		txt_Telefone.setToolTipText("Valor de Venda");
		txt_Telefone.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txt_Telefone.setColumns(10);
		GridBagConstraints gbc_txt_Telefone = new GridBagConstraints();
		gbc_txt_Telefone.insets = new Insets(0, 0, 5, 5);
		gbc_txt_Telefone.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_Telefone.gridx = 1;
		gbc_txt_Telefone.gridy = 2;
		panelDescricao.add(txt_Telefone, gbc_txt_Telefone);

		if (beanFornecedor != null)
		{
			flagEditar = true;

			txt_Telefone.setText(beanFornecedor.getTelefone());
			txt_Telefone.setEditable(false);

			txt_Nome.setText(beanFornecedor.getNome());
			txt_Nome.setEditable(false);

		}

	}// fim do construtor

	public void actionPerformed(ActionEvent event)
	{
		if (event.getSource() == btn_Editar)
		{
			editaFornecedor();
		}
		if (event.getSource() == btn_Salvar)
		{
			if (txt_Nome.getText().equals("")
					|| txt_Telefone.getText().equals(""))
			{
				JOptionPane.showMessageDialog(null, "Preencha todos os campos");
			} else
			{
				salvarFornecedor();
			}
		}
		if (event.getSource() == btn_Volta)
		{
			dispose();
		}

	}

	/** Metodo que salva produtos **/
	private void salvarFornecedor()
	{
		FornecedorBean fornecedor = new FornecedorBean();
		fornecedor.setNome(txt_Nome.getText());
		fornecedor.setTelefone(txt_Telefone.getText());

		if (!flagEditar)
		{

			Connection conn = null;
			try
			{
				AcessoBD bd = new AcessoBD();
				conn = bd.obtemConexao();
				flagSalvo = FornecedorDao.salvar(conn, fornecedor);
				if (flagSalvo)
				{
					JOptionPane.showMessageDialog(null,
							" Cria��o fornecedor com sucess.", "Sucesso",
							JOptionPane.PLAIN_MESSAGE);
					dispose();
				} else
				{

					JOptionPane.showMessageDialog(null, "ERRO NA CRIA��O",
							"Erro", JOptionPane.ERROR_MESSAGE);
				}

			} catch (Exception e)
			{
				e.printStackTrace();
				System.out.println("Erro");
			}
		} else
		{
			Connection conn = null;
			try
			{
				AcessoBD bd = new AcessoBD();
				conn = bd.obtemConexao();
				flagSalvo = FornecedorDao.updateFornecedor(conn, fornecedor);
				if (flagSalvo)
				{
					JOptionPane.showMessageDialog(null,
							" Alterado fornecedor com Sucesso.",
							"Cria��o com sucesso", JOptionPane.PLAIN_MESSAGE);
					System.out.println("Aqui");
					dispose();
				} else
				{

					JOptionPane.showMessageDialog(null, "ERRO NA ALTERA��O",
							"Erro", JOptionPane.ERROR_MESSAGE);
				}

			} catch (Exception e)
			{
				e.printStackTrace();
				System.out.println("Erro");
			}
		}

	}

	/** Metodo que habilita todos os campos para edi��o **/
	private void editaFornecedor()
	{
		if (txt_Nome.getText().equals(""))
		{
			txt_Nome.setEditable(true);
			txt_Telefone.setEditable(true);
			btn_Editar.setEnabled(false);
		} else
		{
			txt_Nome.setEnabled(false);
			txt_Telefone.setEditable(true);
			btn_Editar.setEnabled(false);
		}
	}

	public boolean cadastroProduto()
	{
		flagSalvo = false; // Marcamos que o ok n�o foi selecionado
		setModal(true); // A dialog tem que ser modal. S� pode retornar do
						// setVisible ap�s ficar invis�vel.
		setVisible(true); // Mostramos a dialog e esperamos o usu�rio escolher
							// alguma coisa.
		return flagSalvo; // Retornamos true, se ele pressionou ok.
	}

}
