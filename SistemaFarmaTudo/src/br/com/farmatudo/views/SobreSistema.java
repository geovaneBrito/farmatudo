package br.com.farmatudo.views;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URI;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import br.com.farmatudo.util.TelaUtil;

public class SobreSistema extends JDialog implements MouseListener
{
	private JLabel lbl_SiteDesv;

	public SobreSistema()
	{
		TelaUtil.setaNimbus(this);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		setTitle("Sobre");
		setModal(true);
		setSize(340, 299);
		setResizable(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		getContentPane().add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 294, 0 };
		gbl_panel.rowHeights = new int[] { 0, 73, 84, 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lbl_Sobre = new JLabel(
				"<html><h3>Sistema FarmaTudo</h3> Um software constru�do para fornecer velocidade, simplicidade e seguran�a</html>");
		lbl_Sobre.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_Sobre = new GridBagConstraints();
		gbc_lbl_Sobre.insets = new Insets(0, 0, 5, 0);
		gbc_lbl_Sobre.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_Sobre.gridheight = 2;
		gbc_lbl_Sobre.gridx = 1;
		gbc_lbl_Sobre.gridy = 0;
		panel.add(lbl_Sobre, gbc_lbl_Sobre);

		JLabel lbl_Desen = new JLabel(
				"<html><h4>Desenvolvido por :</h4>Geovane de Brito Alves <br/><h3>� Desv�ti | Copyright�2014</h3></html>");
		lbl_Desen.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_Desen = new GridBagConstraints();
		gbc_lbl_Desen.insets = new Insets(0, 0, 5, 0);
		gbc_lbl_Desen.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_Desen.gridx = 1;
		gbc_lbl_Desen.gridy = 2;
		panel.add(lbl_Desen, gbc_lbl_Desen);

		lbl_SiteDesv = new JLabel("<html><a href=" + ""
				+ ">Contato com Desv-TI</a></html>");
		lbl_SiteDesv.setCursor(new Cursor(Cursor.HAND_CURSOR));
		lbl_SiteDesv.addMouseListener(this);
		GridBagConstraints gbc_lbl_SiteDesv = new GridBagConstraints();
		gbc_lbl_SiteDesv.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_SiteDesv.gridx = 1;
		gbc_lbl_SiteDesv.gridy = 3;
		panel.add(lbl_SiteDesv, gbc_lbl_SiteDesv);
	}

	@Override
	public void mouseClicked(MouseEvent event)
	{
		if (event.getSource() == lbl_SiteDesv)
		{

			Desktop desktop = Desktop.getDesktop();
			try
			{
				desktop.browse(new URI("www.desvti.com.br/contato.php"));
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}

	}

	@Override
	public void mouseEntered(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}
}
