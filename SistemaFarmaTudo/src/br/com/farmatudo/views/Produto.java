/**@author Geovane de Brito Alves****/
package br.com.farmatudo.views;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import br.com.farmatudo.bean.ProdutoBean;
import br.com.farmatudo.daos.ProdutoDao;
import br.com.farmatudo.util.AcessoBD;
import br.com.farmatudo.util.TelaUtil;

/** Classe que mostrar o relatorio de aulas do aluno selecionado **/
public class Produto extends JDialog implements MouseListener, KeyListener, ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel painelRelatorio;
	private JScrollPane scrollPane;
	private JTable table_Produto;
	private JLabel lbl_Pesquisa;
	private JTextField txt_Pesquisa;
	private JPanel panelPesquisa;
	private JRadioButton rdbtnCategoria;
	private JRadioButton rdbtnDescrio;
	private JRadioButton rdbtnCusto;
	private JRadioButton rdbtnMargem;
	private JRadioButton rdbtnValorDeVenda;
	private CadastroProduto cadastroProduto;
	private JButton btn_Novo;
	private JButton btn_Add_Produto;

	/**
	 * @wbp.nonvisual location=59,59
	 */

	public Produto()
	{

		setTitle("Produtos");
		setModal(true);
		setSize(962, 559);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(true);
		TelaUtil.setaNimbus(this);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 63, 50, 116, 107, 332, 78, 88, 92, 0 };
		gridBagLayout.rowHeights = new int[] { 57, 145, 182, 38, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		panelPesquisa = new JPanel();
		panelPesquisa.setBorder(BorderFactory.createTitledBorder("<html><h4>Pesquisa</h4><html>"));
		GridBagConstraints gbc_panelPesquisa = new GridBagConstraints();
		gbc_panelPesquisa.anchor = GridBagConstraints.WEST;
		gbc_panelPesquisa.gridwidth = 4;
		gbc_panelPesquisa.insets = new Insets(0, 0, 5, 5);
		gbc_panelPesquisa.fill = GridBagConstraints.VERTICAL;
		gbc_panelPesquisa.gridx = 1;
		gbc_panelPesquisa.gridy = 1;
		getContentPane().add(panelPesquisa, gbc_panelPesquisa);
		GridBagLayout gbl_panelPesquisa = new GridBagLayout();
		gbl_panelPesquisa.columnWidths = new int[] { 62, 47, 0, 0, 71, 0, 0 };
		gbl_panelPesquisa.rowHeights = new int[] { 45, 26, 0 };
		gbl_panelPesquisa.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelPesquisa.rowWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		panelPesquisa.setLayout(gbl_panelPesquisa);

		ButtonGroup buttonGroup = new ButtonGroup();

		rdbtnCategoria = new JRadioButton("Categoria");
		rdbtnCategoria.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnCategoria.setVerticalAlignment(SwingConstants.BOTTOM);
		GridBagConstraints gbc_rdbtnCategoria = new GridBagConstraints();
		gbc_rdbtnCategoria.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnCategoria.gridx = 1;
		gbc_rdbtnCategoria.gridy = 0;
		panelPesquisa.add(rdbtnCategoria, gbc_rdbtnCategoria);
		buttonGroup.add(rdbtnCategoria);

		rdbtnDescrio = new JRadioButton("Descri��o");
		rdbtnDescrio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnDescrio.setVerticalAlignment(SwingConstants.BOTTOM);
		GridBagConstraints gbc_rdbtnDescrio = new GridBagConstraints();
		gbc_rdbtnDescrio.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnDescrio.gridx = 2;
		gbc_rdbtnDescrio.gridy = 0;
		panelPesquisa.add(rdbtnDescrio, gbc_rdbtnDescrio);
		buttonGroup.add(rdbtnDescrio);

		rdbtnCusto = new JRadioButton("Valor de Custo");
		rdbtnCusto.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnCusto.setVerticalAlignment(SwingConstants.BOTTOM);
		GridBagConstraints gbc_rdbtnCurso = new GridBagConstraints();
		gbc_rdbtnCurso.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnCurso.gridx = 3;
		gbc_rdbtnCurso.gridy = 0;
		panelPesquisa.add(rdbtnCusto, gbc_rdbtnCurso);
		buttonGroup.add(rdbtnCusto);

		rdbtnMargem = new JRadioButton("Margem");
		rdbtnMargem.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnMargem.setVerticalAlignment(SwingConstants.BOTTOM);
		GridBagConstraints gbc_rdbtnMargem = new GridBagConstraints();
		gbc_rdbtnMargem.insets = new Insets(0, 0, 5, 5);
		gbc_rdbtnMargem.gridx = 4;
		gbc_rdbtnMargem.gridy = 0;
		panelPesquisa.add(rdbtnMargem, gbc_rdbtnMargem);
		buttonGroup.add(rdbtnMargem);

		rdbtnValorDeVenda = new JRadioButton("Valor de Venda");
		rdbtnValorDeVenda.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnValorDeVenda.setVerticalAlignment(SwingConstants.BOTTOM);
		GridBagConstraints gbc_rdbtnValorDeVenda = new GridBagConstraints();
		gbc_rdbtnValorDeVenda.insets = new Insets(0, 0, 5, 0);
		gbc_rdbtnValorDeVenda.gridx = 5;
		gbc_rdbtnValorDeVenda.gridy = 0;
		panelPesquisa.add(rdbtnValorDeVenda, gbc_rdbtnValorDeVenda);
		buttonGroup.add(rdbtnValorDeVenda);

		lbl_Pesquisa = new JLabel("Buscar: ");
		GridBagConstraints gbc_lbl_Pesquisa = new GridBagConstraints();
		gbc_lbl_Pesquisa.insets = new Insets(0, 0, 0, 5);
		gbc_lbl_Pesquisa.gridx = 0;
		gbc_lbl_Pesquisa.gridy = 1;
		panelPesquisa.add(lbl_Pesquisa, gbc_lbl_Pesquisa);
		lbl_Pesquisa.setFont(new Font("Tahoma", Font.PLAIN, 14));

		txt_Pesquisa = new JTextField();
		txt_Pesquisa.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		txt_Pesquisa.setColumns(10);
		txt_Pesquisa.selectAll();
		txt_Pesquisa.requestFocus();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 0, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridwidth = 2;
		gbc_textField.anchor = GridBagConstraints.NORTH;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 1;
		panelPesquisa.add(txt_Pesquisa, gbc_textField);
		txt_Pesquisa.addKeyListener(this);

		btn_Add_Produto = new JButton();
		btn_Add_Produto.setIcon(new ImageIcon(Produto.class.getResource("/imagemIcon/addProduto.png")));
		GridBagConstraints gbc_btn_Add_Produto = new GridBagConstraints();
		gbc_btn_Add_Produto.anchor = GridBagConstraints.SOUTH;
		gbc_btn_Add_Produto.insets = new Insets(0, 0, 5, 5);
		gbc_btn_Add_Produto.gridx = 5;
		gbc_btn_Add_Produto.gridy = 1;
		getContentPane().add(btn_Add_Produto, gbc_btn_Add_Produto);
		btn_Add_Produto.addActionListener(this);

		btn_Novo = new JButton();
		btn_Novo.setIcon(new ImageIcon(Produto.class.getResource("/imagemIcon/Add.png")));
		GridBagConstraints gbc_btn_Novo = new GridBagConstraints();
		gbc_btn_Novo.anchor = GridBagConstraints.SOUTH;
		gbc_btn_Novo.insets = new Insets(0, 0, 5, 5);
		gbc_btn_Novo.gridx = 6;
		gbc_btn_Novo.gridy = 1;
		getContentPane().add(btn_Novo, gbc_btn_Novo);
		btn_Novo.addActionListener(this);

		painelRelatorio = new JPanel();
		painelRelatorio.setBorder(BorderFactory.createTitledBorder("<html><h4>Lista de Produtos</h4><html>"));
		GridBagConstraints gbc_painelRelatorio = new GridBagConstraints();
		gbc_painelRelatorio.insets = new Insets(0, 0, 5, 5);
		gbc_painelRelatorio.gridwidth = 6;
		gbc_painelRelatorio.fill = GridBagConstraints.BOTH;
		gbc_painelRelatorio.gridx = 1;
		gbc_painelRelatorio.gridy = 2;
		getContentPane().add(painelRelatorio, gbc_painelRelatorio);
		GridBagLayout gbl_painelRelatorio = new GridBagLayout();
		gbl_painelRelatorio.columnWidths = new int[] { 665, 0, 0, 0, 0 };
		gbl_painelRelatorio.rowHeights = new int[] { 234, 0 };
		gbl_painelRelatorio.columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_painelRelatorio.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		painelRelatorio.setLayout(gbl_painelRelatorio);

		scrollPane = new JScrollPane();

		table_Produto = new JTable();
		table_Produto.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_Produto.setAutoCreateRowSorter(true);
		table_Produto.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		scrollPane.setViewportView(table_Produto);
		table_Produto.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null, null, null, null }, },
				new String[] { "C�digo de Produto", "Descri��o", "Valor de Custo", "Margem", "Valor de Venda",
						"Quantidades", "Categoria" })
		{
			boolean[] columnEditables = new boolean[] { false, false, false, false, false, false, false };

			public boolean isCellEditable(int row, int column)
			{
				return columnEditables[column];
			}
		});
		table_Produto.getColumnModel().getColumn(0).setPreferredWidth(140);
		table_Produto.getColumnModel().getColumn(1).setPreferredWidth(140);
		table_Produto.getColumnModel().getColumn(2).setPreferredWidth(50);
		table_Produto.getColumnModel().getColumn(3).setPreferredWidth(50);
		table_Produto.getColumnModel().getColumn(4).setPreferredWidth(50);
		table_Produto.getColumnModel().getColumn(5).setPreferredWidth(50);
		table_Produto.getColumnModel().getColumn(6).setPreferredWidth(100);
		table_Produto.addMouseListener(this);
		// new TableFilter(table);
		table_Produto.changeSelection(table_Produto.getSelectedRow(), 0, false, false);
		/**
		 * private int id; private int laboratorio; private String categoria;
		 * private String descricao; private double valor_custo; private int
		 * margem; private double valor_venda; private int quantidade;
		 **/

		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 4;
		gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		painelRelatorio.add(scrollPane, gbc_scrollPane);
		painelRelatorio.setVisible(false);

		carregaProdutosTable();

	}

	private void carregaProdutosTable()
	{
		Connection conn = null;
		try
		{

			AcessoBD bd = new AcessoBD();
			conn = bd.obtemConexao();
			DefaultTableModel dtm = (DefaultTableModel) table_Produto.getModel();

			while (dtm.getRowCount() > 0)
			{
				dtm.removeRow(0);

			}
			for (ProdutoBean produtos : ProdutoDao.carregarTodosProduto(conn))
			{

				adcionaLinhas(dtm, produtos);

			}

			painelRelatorio.setVisible(true);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/** Metodo que acionada linhas na tabela, conforme consulta no banco ****/
	private void adcionaLinhas(DefaultTableModel dtm, ProdutoBean produto)
	{
		// "C�digo de Produto, Descri��o,Valor de Custo, Margem, Valor de Venda,
		// Quantidades, Categoria"

		dtm.addRow(new Object[] { (produto.getId() + "" + produto.getLaboratorio()), produto.getDescricao(),
				produto.getValor_custo(), produto.getMargem(), produto.getValor_venda(), produto.getQuantidade(),
				produto.getCategoria() });
	}

	@Override
	public void mouseClicked(MouseEvent event)
	{

		int row = table_Produto.getSelectedRow();
		DefaultTableModel tableModel = (DefaultTableModel) this.table_Produto.getModel();

		if (event.getClickCount() == 2)
		{

			cadastroProduto = new CadastroProduto(tableModel, row);
			cadastroProduto.setResizable(true);
			cadastroProduto.setLocationRelativeTo(null);
			cadastroProduto.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			if (cadastroProduto.cadastroProduto())
			{

				carregaProdutosTable();
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent event)
	{
		if (event.getKeyCode() == KeyEvent.VK_ENTER )
		{
			if (txt_Pesquisa.getText().length()  == 0)
			{
				carregaProdutosTable();
			}
		}
		
		if (txt_Pesquisa.getText().length() > 0)
		{

			if (event.getKeyCode() == KeyEvent.VK_ENTER || event.getKeyCode() == KeyEvent.VK_TAB)
			{
				String pesquisa = "";
				
				pesquisa = txt_Pesquisa.getText().trim();

				if (pesquisa.length() == 0)
				{
					carregaProdutosTable();
				} else
				{
					try
					{
						DefaultTableModel dtm = (DefaultTableModel) table_Produto.getModel();
						
						 if (rdbtnDescrio.isSelected())
						{
							 // descrica��o
							AcessoBD acessoBD = new AcessoBD();
							Connection conn = null;

							while (dtm.getRowCount() > 0)
							{
								dtm.removeRow(0);
							}

							conn = acessoBD.obtemConexao();
							for (ProdutoBean produtoBean : ProdutoDao.buscarPorStringExpecifico(conn,
									"descricao like ?", pesquisa))
							{
								System.out.println("descricao " + produtoBean.getDescricao());
								adcionaLinhas(dtm, produtoBean);
							}
							

						} else if (rdbtnCusto.isSelected())
						{
							// custo
							AcessoBD acessoBD = new AcessoBD();
							Connection conn = null;
							try
							{
								conn = acessoBD.obtemConexao();

								while (dtm.getRowCount() > 0)
								{
									dtm.removeRow(0);
								}

								for (ProdutoBean produtoBean : ProdutoDao.buscarPorDoubleExpecifico(conn,
										" valor_custo like ?", pesquisa))
								{
									
									adcionaLinhas(dtm, produtoBean);
								}
							} catch (Exception e)
							{
								e.printStackTrace();
							}
							
						} else if (rdbtnMargem.isSelected())
						{
							// margem
							AcessoBD acessoBD = new AcessoBD();
							Connection conn = null;
							try
							{
								conn = acessoBD.obtemConexao();

								while (dtm.getRowCount() > 0)
								{
									dtm.removeRow(0);
								}

								for (ProdutoBean produtoBean : ProdutoDao.buscarPorDoubleExpecifico(conn,
										" margem like ?", pesquisa))
								{
									
									adcionaLinhas(dtm, produtoBean);
								}
							} catch (Exception e)
							{
								e.printStackTrace();
							}
							
						} else if (rdbtnValorDeVenda.isSelected())
						{
							// valor de venda
							AcessoBD acessoBD = new AcessoBD();
							Connection conn = null;
							try
							{
								conn = acessoBD.obtemConexao();

								while (dtm.getRowCount() > 0)
								{
									dtm.removeRow(0);
								}

								for (ProdutoBean produtoBean : ProdutoDao.buscarPorDoubleExpecifico(conn,
										" valor_venda like ?", pesquisa))
								{
									
									adcionaLinhas(dtm, produtoBean);
								}
							} catch (Exception e)
							{
								e.printStackTrace();
							}
						} else if (rdbtnCategoria.isSelected())
						{
							// categoria
							AcessoBD acessoBD = new AcessoBD();
							Connection conn = null;
							try
							{
								conn = acessoBD.obtemConexao();

								while (dtm.getRowCount() > 0)
								{
									dtm.removeRow(0);
								}

								for (ProdutoBean produtoBean : ProdutoDao.buscarPorStringExpecifico(conn,
										" categoria like ?", pesquisa))
								{
									
									adcionaLinhas(dtm, produtoBean);
								}
							} catch (Exception e)
							{
								e.printStackTrace();
							}

						}
						 
						
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			}

		}

	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		if (event.getSource() == btn_Novo)
		{
			Nota nota = new Nota();
			nota.setResizable(true);
			nota.setSize(962, 559);
			nota.setLocationRelativeTo(null);
			nota.setModal(true);
			nota.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			nota.setVisible(true);

		}
		if (event.getSource() == btn_Add_Produto)
		{
			CadastroProduto cadastroProduto = new CadastroProduto(null, 0);
			if (cadastroProduto.cadastroProduto())
			{
				System.out.println("Salvo");
				carregaProdutosTable();
			}
		}

	}

}
