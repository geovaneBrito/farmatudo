package br.com.farmatudo.views;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;

import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;

import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import br.com.farmatudo.bean.CaixaBean;
import br.com.farmatudo.bean.ProdutoBean;
import br.com.farmatudo.bean.UsuarioBean;
import br.com.farmatudo.bean.VendaBean;
import br.com.farmatudo.daos.CaixaDao;
import br.com.farmatudo.daos.FormaPagamentoDao;
import br.com.farmatudo.daos.ProdutoDao;
import br.com.farmatudo.daos.VendaDao;
import br.com.farmatudo.util.AcessoBD;
import br.com.farmatudo.util.JMoneyField;
import br.com.farmatudo.util.JtextFieldSomenteNumeros;
import br.com.farmatudo.util.TelaUtil;

public class Venda extends JDialog implements MouseListener, KeyListener,
		FocusListener, ActionListener
{
	private JTextComponent txtCdigoDoProduto;
	private JComboBox comboboxProduto;
	private JTextField txtQuantidade;
	private JXTable table_Produto;
	private JXDatePicker datePicker_Venda;
	private ArrayList<ProdutoBean> listProdutoComprados;
	private ArrayList<ProdutoBean> todosProdutos;
	private ProdutoBean beanProduto;
	private JMoneyField txtTotal;
	private JButton btnCancelar;
	private JButton btnConvenio;
	private JToggleButton btnCheque;
	private JToggleButton btnDinheiro;
	private JToggleButton btnCartao;
	private JButton btnSair;
	private JButton btnFinalizar;
	UsuarioBean usuarioLogado;
	private JLabel lblTotal;
	private JMoneyField txtValorDesconto;
	private JButton btnDesconto;
	private String idProduto = "";
	private double valordesconto = 0;
	private JButton btnAberturaFechamento;
	private JLabel lblUsuario;
	private CaixaBean caixaBean;
	private JLabel lblDesconto;

	public Venda(UsuarioBean usuario)
	{
		// instanciando um arraylist de Produto
		listProdutoComprados = new ArrayList<ProdutoBean>();
		setTitle("Tela de Vendas");
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		setSize(1266, 732);
		setModal(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(true);
		TelaUtil.setaNimbus(this);
		// setUndecorated(true);

		usuarioLogado = usuario;

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 214, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 51, 0, 0, 38, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, 1.0,
				Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, 0.0, 0.0,
				Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		JXPanel panelVenda = new JXPanel();
		panelVenda.setBorder(BorderFactory
				.createTitledBorder("<html><h2>Itens Venda</h2><html>"));
		GridBagConstraints gbc_panelVenda = new GridBagConstraints();
		gbc_panelVenda.gridwidth = 3;
		gbc_panelVenda.insets = new Insets(0, 0, 5, 0);
		gbc_panelVenda.fill = GridBagConstraints.BOTH;
		gbc_panelVenda.gridx = 0;
		gbc_panelVenda.gridy = 1;
		getContentPane().add(panelVenda, gbc_panelVenda);

		GridBagLayout gbl_panelVenda = new GridBagLayout();
		gbl_panelVenda.columnWidths = new int[] { 183, 126, 137, 160, 48, 135, 78,
				187, 83, 0 };
		gbl_panelVenda.rowHeights = new int[] { 0, 0, 0, 0, 47, 0, 0 };
		gbl_panelVenda.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0, 1.0,
				0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelVenda.rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0,
				0.0, Double.MIN_VALUE };
		panelVenda.setLayout(gbl_panelVenda);

		txtCdigoDoProduto = new JTextField();
		txtCdigoDoProduto.setFont(new Font("Tahoma", Font.PLAIN, 36));
		GridBagConstraints gbc_txtCdigoDoProduto = new GridBagConstraints();
		gbc_txtCdigoDoProduto.gridwidth = 2;
		gbc_txtCdigoDoProduto.insets = new Insets(0, 0, 5, 5);
		gbc_txtCdigoDoProduto.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtCdigoDoProduto.gridx = 1;
		gbc_txtCdigoDoProduto.gridy = 0;
		panelVenda.add(txtCdigoDoProduto, gbc_txtCdigoDoProduto);
		txtCdigoDoProduto.addKeyListener(this);
		txtCdigoDoProduto.addFocusListener(this);

		JPanel panelAcao = new JPanel();
		panelAcao.setBorder(BorderFactory
				.createTitledBorder("<html><h4>Venda</h4><html>"));
		GridBagConstraints gbc_panelAcao = new GridBagConstraints();
		gbc_panelAcao.gridwidth = 3;
		gbc_panelAcao.insets = new Insets(0, 0, 5, 0);
		gbc_panelAcao.fill = GridBagConstraints.BOTH;
		gbc_panelAcao.gridx = 0;
		gbc_panelAcao.gridy = 0;
		getContentPane().add(panelAcao, gbc_panelAcao);
		GridBagLayout gbl_panelAcao = new GridBagLayout();
		gbl_panelAcao.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 235, 0, 0, 0, 0, 0 };
		gbl_panelAcao.rowHeights = new int[] { 46, 0 };
		gbl_panelAcao.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_panelAcao.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		panelAcao.setLayout(gbl_panelAcao);

		btnFinalizar = new JButton();
		btnFinalizar.setFocusable(false);
		btnFinalizar.setIcon(new ImageIcon(Venda.class
				.getResource("/imagemIcon/icon_carrinho_finalizar.png")));
		btnFinalizar.setToolTipText("F1 - Finalizar Venda");
		GridBagConstraints gbc_btnFinalizar = new GridBagConstraints();
		gbc_btnFinalizar.insets = new Insets(0, 0, 0, 5);
		gbc_btnFinalizar.gridx = 0;
		gbc_btnFinalizar.gridy = 0;
		panelAcao.add(btnFinalizar, gbc_btnFinalizar);
		btnFinalizar.addActionListener(this);

		btnCancelar = new JButton();
		btnCancelar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnCancelar.setIcon(new ImageIcon(Venda.class
				.getResource("/imagemIcon/cancelar_venda.png")));
		btnCancelar.setToolTipText("F7 - Cancelar ");
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.insets = new Insets(0, 0, 0, 5);
		gbc_btnCancelar.gridx = 1;
		gbc_btnCancelar.gridy = 0;
		panelAcao.add(btnCancelar, gbc_btnCancelar);
		btnCancelar.addActionListener(this);

		btnSair = new JButton();
		btnSair.setIcon(new ImageIcon(Venda.class
				.getResource("/imagemIcon/Log Out.png")));
		btnSair.setToolTipText("F10 - Sair ");
		GridBagConstraints gbc_btnSair = new GridBagConstraints();
		gbc_btnSair.insets = new Insets(0, 0, 0, 5);
		gbc_btnSair.gridx = 2;
		gbc_btnSair.gridy = 0;
		panelAcao.add(btnSair, gbc_btnSair);
		btnSair.addActionListener(this);

		btnAberturaFechamento = new JButton();
		btnAberturaFechamento.setFocusable(false);
		btnAberturaFechamento.setIcon(new ImageIcon(Venda.class
				.getResource("/imagemIcon/caixa.png")));
		btnAberturaFechamento
				.setToolTipText("F11 - Abertura/Fechamento de Caixa ");
		GridBagConstraints gbc_btnAberturaFechamento = new GridBagConstraints();
		gbc_btnAberturaFechamento.insets = new Insets(0, 0, 0, 5);
		gbc_btnAberturaFechamento.gridx = 3;
		gbc_btnAberturaFechamento.gridy = 0;
		panelAcao.add(btnAberturaFechamento, gbc_btnAberturaFechamento);
		btnAberturaFechamento.addActionListener(this);

		lblUsuario = new JLabel("Usu�rio Logado : "
				+ usuarioLogado.getNome().toUpperCase());
		GridBagConstraints gbc_lblUsuario = new GridBagConstraints();
		gbc_lblUsuario.anchor = GridBagConstraints.WEST;
		gbc_lblUsuario.gridwidth = 9;
		gbc_lblUsuario.insets = new Insets(0, 0, 0, 5);
		gbc_lblUsuario.gridx = 4;
		gbc_lblUsuario.gridy = 0;
		panelAcao.add(lblUsuario, gbc_lblUsuario);
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 30));
		
		//registrarAcoesDoTeclado(panelAcao);

		datePicker_Venda = new JXDatePicker(System.currentTimeMillis());
		GridBagConstraints gbc_datePicker_Venda = new GridBagConstraints();
		gbc_datePicker_Venda.gridwidth = 3;
		gbc_datePicker_Venda.gridx = 14;
		gbc_datePicker_Venda.gridy = 0;
		panelAcao.add(datePicker_Venda, gbc_datePicker_Venda);
		datePicker_Venda.setEditable(false);
		datePicker_Venda.setEnabled(false);
		datePicker_Venda.getEditor().setEnabled(false);
		datePicker_Venda.getEditor().setEditable(false);
		datePicker_Venda.setFormats(new String[] { "dd/MM/yyyy" });

		datePicker_Venda.setFont(new Font("Tahoma", Font.PLAIN, 30));

		comboboxProduto = new JComboBox();
		comboboxProduto.setFont(new Font("Tahoma", Font.PLAIN, 36));
		//comboboxProduto.addActionListener(this);

		GridBagConstraints gbc_txtProduto = new GridBagConstraints();
		gbc_txtProduto.gridwidth = 3;
		gbc_txtProduto.insets = new Insets(0, 0, 5, 5);
		gbc_txtProduto.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtProduto.gridx = 4;
		gbc_txtProduto.gridy = 0;
		panelVenda.add(comboboxProduto, gbc_txtProduto);


		JLabel lblCdigoDoProduto = new JLabel("C�digo:");
		lblCdigoDoProduto.setHorizontalAlignment(SwingConstants.LEFT);
		lblCdigoDoProduto.setFont(new Font("Tahoma", Font.PLAIN, 36));
		GridBagConstraints gbc_lblCdigoDoProduto = new GridBagConstraints();
		gbc_lblCdigoDoProduto.anchor = GridBagConstraints.EAST;
		gbc_lblCdigoDoProduto.insets = new Insets(0, 0, 5, 5);
		gbc_lblCdigoDoProduto.gridx = 0;
		gbc_lblCdigoDoProduto.gridy = 0;
		panelVenda.add(lblCdigoDoProduto, gbc_lblCdigoDoProduto);

		// carregando lista dinamica
		Connection connection = null;
		AcessoBD acessoBD = new AcessoBD();

		try
		{
			connection = acessoBD.obtemConexao();
			todosProdutos = ProdutoDao.carregarTodosProduto(connection);
			DefaultListModel modelCodig = new DefaultListModel();
			DefaultListModel modelProduto = new DefaultListModel();
			for (ProdutoBean produtoBean : todosProdutos)
			{

				modelCodig.addElement(produtoBean.getId() + ""
						+ produtoBean.getLaboratorio());
				comboboxProduto.addItem(produtoBean.getDescricao());

			}
			JList listCod = new JList();
			listCod.setModel(modelCodig);


			AutoCompleteDecorator.decorate(listCod, txtCdigoDoProduto);
			AutoCompleteDecorator.decorate(this.comboboxProduto);

		} catch (Exception e)
		{
			e.printStackTrace();
			JOptionPane
					.showMessageDialog(
							null,
							"Problema ao carregar Produtos. Por favor, feche a tela e tente novamente, ou entre em contato com Administrador do sistema",
							"Erro", JOptionPane.ERROR_MESSAGE);
		}

		JLabel lblProduto = new JLabel("Produto:");
		lblProduto.setFont(new Font("Tahoma", Font.PLAIN, 36));
		GridBagConstraints gbc_lblProduto = new GridBagConstraints();
		gbc_lblProduto.insets = new Insets(0, 0, 5, 5);
		gbc_lblProduto.anchor = GridBagConstraints.EAST;
		gbc_lblProduto.gridx = 3;
		gbc_lblProduto.gridy = 0;
		panelVenda.add(lblProduto, gbc_lblProduto);

		JLabel lblQuantidade = new JLabel("Quantidade:");
		lblQuantidade.setFont(new Font("Tahoma", Font.PLAIN, 36));
		GridBagConstraints gbc_lblQuantidade = new GridBagConstraints();
		gbc_lblQuantidade.insets = new Insets(0, 0, 5, 5);
		gbc_lblQuantidade.anchor = GridBagConstraints.EAST;
		gbc_lblQuantidade.gridx = 7;
		gbc_lblQuantidade.gridy = 0;
		panelVenda.add(lblQuantidade, gbc_lblQuantidade);

		txtQuantidade = new JtextFieldSomenteNumeros();
		txtQuantidade.setText("1");
		txtQuantidade.setFont(new Font("Tahoma", Font.PLAIN, 36));
		GridBagConstraints gbc_txtQuantidade = new GridBagConstraints();
		gbc_txtQuantidade.insets = new Insets(0, 0, 5, 0);
		gbc_txtQuantidade.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtQuantidade.gridx = 8;
		gbc_txtQuantidade.gridy = 0;
		panelVenda.add(txtQuantidade, gbc_txtQuantidade);
		txtQuantidade.addFocusListener(this);
		txtQuantidade.addKeyListener(this);
		txtQuantidade.setColumns(10);

		JScrollPane scrollPaneProduto = new JScrollPane();
		GridBagConstraints gbc_scrollPaneProduto = new GridBagConstraints();
		gbc_scrollPaneProduto.gridheight = 3;
		gbc_scrollPaneProduto.gridwidth = 9;
		gbc_scrollPaneProduto.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPaneProduto.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneProduto.gridx = 0;
		gbc_scrollPaneProduto.gridy = 1;
		panelVenda.add(scrollPaneProduto, gbc_scrollPaneProduto);

		table_Produto = new JXTable();
		table_Produto.setSortable(false);
		table_Produto.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_Produto.setAutoCreateRowSorter(true);
		table_Produto.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		scrollPaneProduto.setViewportView(table_Produto);
		table_Produto.setModel(new DefaultTableModel(new Object[][] { { null,
				null, null, null, null, null, null }, }, new String[] {
				"<html><h4>C�digo de Produto", "<html><h4>Descri��o",
				"<html><h4>Valor de Venda", "<html><h4>Quantidades",
				"<html><h4>Valor unit�rio x quantidade" })
		{
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			boolean[] columnEditables = new boolean[] { false, false, false,
					false, false };

			public boolean isCellEditable(int row, int column)
			{
				return columnEditables[column];
			}
		});
		table_Produto.addKeyListener(this);

		JPanel panelCompra = new JPanel();
		panelCompra
				.setBorder(BorderFactory
						.createTitledBorder("<html><h4>Formas de Pagamento</h4><html>"));
		GridBagConstraints gbc_panelCompra = new GridBagConstraints();
		gbc_panelCompra.gridwidth = 5;
		gbc_panelCompra.insets = new Insets(0, 0, 5, 5);
		gbc_panelCompra.fill = GridBagConstraints.BOTH;
		gbc_panelCompra.gridx = 0;
		gbc_panelCompra.gridy = 4;
		panelVenda.add(panelCompra, gbc_panelCompra);
		GridBagLayout gbl_panelCompra = new GridBagLayout();
		gbl_panelCompra.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panelCompra.rowHeights = new int[] { 0, 0 };
		gbl_panelCompra.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0,
				1.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_panelCompra.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		panelCompra.setLayout(gbl_panelCompra);

		btnDinheiro = new JToggleButton();
		btnDinheiro.setIcon(new ImageIcon(Venda.class
				.getResource("/imagemIcon/nota.png")));
		btnDinheiro.setToolTipText("F2 - Vendas em dinheiro");
		GridBagConstraints gbc_btnDinheiro = new GridBagConstraints();
		gbc_btnDinheiro.insets = new Insets(0, 0, 0, 5);
		gbc_btnDinheiro.gridx = 0;
		gbc_btnDinheiro.gridy = 0;
		panelCompra.add(btnDinheiro, gbc_btnDinheiro);
		btnDinheiro.addActionListener(this);

		btnCheque = new JToggleButton();
		GridBagConstraints gbc_btnCheque = new GridBagConstraints();
		gbc_btnCheque.insets = new Insets(0, 0, 0, 5);
		gbc_btnCheque.gridx = 1;
		gbc_btnCheque.gridy = 0;
		panelCompra.add(btnCheque, gbc_btnCheque);
		btnCheque.setToolTipText("F3 � Cheques");
		btnCheque.setIcon(new ImageIcon(Venda.class
				.getResource("/imagemIcon/Cheque.png")));
		btnCheque.addActionListener(this);

		btnCartao = new JToggleButton();
		btnCartao.setIcon(new ImageIcon(Venda.class
				.getResource("/imagemIcon/cartao.png")));
		btnCartao.setToolTipText("F4 � Cart�o");
		GridBagConstraints gbc_btnCartao = new GridBagConstraints();
		gbc_btnCartao.insets = new Insets(0, 0, 0, 5);
		gbc_btnCartao.gridx = 2;
		gbc_btnCartao.gridy = 0;
		panelCompra.add(btnCartao, gbc_btnCartao);
		btnCartao.addActionListener(this);

		btnConvenio = new JButton();
		btnConvenio.setIcon(new ImageIcon(Venda.class
				.getResource("/imagemIcon/convenios.png")));
		btnConvenio.setToolTipText("F5 � Conv�nio");
		btnConvenio.setEnabled(false);
		GridBagConstraints gbc_btnConvenio = new GridBagConstraints();
		gbc_btnConvenio.insets = new Insets(0, 0, 0, 5);
		gbc_btnConvenio.gridx = 3;
		gbc_btnConvenio.gridy = 0;
		panelCompra.add(btnConvenio, gbc_btnConvenio);
		btnConvenio.addActionListener(this);
		table_Produto.getColumnModel().getColumn(0).setPreferredWidth(140);
		table_Produto.getColumnModel().getColumn(1).setPreferredWidth(260);
		table_Produto.getColumnModel().getColumn(2).setPreferredWidth(50);
		table_Produto.getColumnModel().getColumn(3).setPreferredWidth(50);

		table_Produto.addMouseListener(this);

		btnDesconto = new JButton();
		GridBagConstraints gbc_btnDesconto = new GridBagConstraints();
		gbc_btnDesconto.gridx = 7;
		gbc_btnDesconto.gridy = 0;
		panelCompra.add(btnDesconto, gbc_btnDesconto);
		btnDesconto.setIcon(new ImageIcon(Venda.class
				.getResource("/imagemIcon/desconto.png")));
		btnDesconto.setToolTipText("F12 - Desconto");
		btnDesconto.addActionListener(this);
		btnDesconto.setEnabled(false);

		JLabel lblSubTotal = new JLabel("Sub Total");
		lblSubTotal.setHorizontalAlignment(SwingConstants.LEFT);
		lblSubTotal.setFont(new Font("Tahoma", Font.BOLD, 40));
		GridBagConstraints gbc_lblSubTotal = new GridBagConstraints();
		gbc_lblSubTotal.anchor = GridBagConstraints.EAST;
		gbc_lblSubTotal.insets = new Insets(0, 0, 0, 5);
		gbc_lblSubTotal.gridx = 0;
		gbc_lblSubTotal.gridy = 5;
		panelVenda.add(lblSubTotal, gbc_lblSubTotal);

		txtTotal = new JMoneyField();
		txtTotal.setEditable(false);
		txtTotal.setFont(new Font("Tahoma", Font.PLAIN, 53));
		GridBagConstraints gbc_txtTotal = new GridBagConstraints();
		gbc_txtTotal.gridwidth = 2;
		gbc_txtTotal.insets = new Insets(0, 0, 0, 5);
		gbc_txtTotal.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtTotal.gridx = 2;
		gbc_txtTotal.gridy = 5;
		panelVenda.add(txtTotal, gbc_txtTotal);

		lblDesconto = new JLabel("Desconto");
		lblDesconto.setHorizontalAlignment(SwingConstants.LEFT);
		lblDesconto.setFont(new Font("Tahoma", Font.BOLD, 40));
		GridBagConstraints gbc_lblDesconto_1 = new GridBagConstraints();
		gbc_lblDesconto_1.gridwidth = 2;
		gbc_lblDesconto_1.insets = new Insets(0, 0, 0, 5);
		gbc_lblDesconto_1.gridx = 4;
		gbc_lblDesconto_1.gridy = 5;
		panelVenda.add(lblDesconto, gbc_lblDesconto_1);

		lblTotal = new JLabel("Total");
		GridBagConstraints gbc_lblTotal = new GridBagConstraints();
		gbc_lblTotal.anchor = GridBagConstraints.EAST;
		gbc_lblTotal.insets = new Insets(0, 0, 0, 5);
		gbc_lblTotal.gridx = 6;
		gbc_lblTotal.gridy = 5;
		panelVenda.add(lblTotal, gbc_lblTotal);
		lblTotal.setHorizontalAlignment(SwingConstants.LEFT);
		lblTotal.setFont(new Font("Tahoma", Font.BOLD, 40));

		txtValorDesconto = new JMoneyField();
		GridBagConstraints gbc_txtValorDesconto = new GridBagConstraints();
		gbc_txtValorDesconto.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtValorDesconto.gridwidth = 2;
		gbc_txtValorDesconto.gridx = 7;
		gbc_txtValorDesconto.gridy = 5;
		panelVenda.add(txtValorDesconto, gbc_txtValorDesconto);
		txtValorDesconto.setFont(new Font("Tahoma", Font.PLAIN, 53));
		txtValorDesconto.setEditable(false);

		verificaCaixa();
		txtCdigoDoProduto.setFocusable(true);

	}// fim do construtor

	private void verificaCaixa()
	{
		Connection conn = null;
		AcessoBD acessoBD = new AcessoBD();

		try
		{
			conn = acessoBD.obtemConexao();
			caixaBean = CaixaDao.buscaPorID(conn, usuarioLogado.getNome(),
					datePicker_Venda);

			if (caixaBean == null)
			{

				JOptionPane
						.showMessageDialog(null, "Caixa se encontra Fechado");
				desabilitaCampos();
				lblUsuario.setText("Usu�rio Logado : "
						+ usuarioLogado.getNome().toUpperCase()
						+ " | Caixa Fechado ");

			} else
			{
				if (caixaBean.getStatus())
				{

					lblUsuario.setText("Usu�rio Logado : "
							+ usuarioLogado.getNome().toUpperCase()
							+ " | Caixa Aberto");
					habilitaCampos();
					txtCdigoDoProduto.grabFocus();

				} else
				{
					JOptionPane.showMessageDialog(null,
							"Caixa se encontra Fechado com a data de Hoje");
					desabilitaCampos();
					lblUsuario.setText("Usu�rio Logado : "
							+ usuarioLogado.getNome().toUpperCase()
							+ " | Caixa Fechado ");
				}
			}
		} catch (Exception e2)
		{
			e2.printStackTrace();
		}

	}

	/** Metodo que desabilita todos os campos da janela **/
	private void desabilitaCampos()
	{
		txtCdigoDoProduto.setEnabled(false);
		comboboxProduto.setEnabled(false);
		btnFinalizar.setEnabled(false);
		btnDesconto.setEnabled(false);

	}

	/** Metodo que habilita todos os campos da janela **/
	private void habilitaCampos()
	{
		comboboxProduto.setEnabled(true);
		btnFinalizar.setEnabled(true);
		btnDesconto.setEnabled(true);
		txtCdigoDoProduto.setEnabled(true);
		txtCdigoDoProduto.requestFocus();

	}

	@Override
	public void mouseClicked(MouseEvent arg0)
	{

	}

	@Override
	public void mouseEntered(MouseEvent arg0)
	{

	}

	@Override
	public void mouseExited(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	/** Metodo que pega a��o do teclado **/
	public void keyPressed(KeyEvent event)
	{

		if (event.getKeyChar() == KeyEvent.VK_DELETE)
		{
			if (event.getSource() == table_Produto)
			{
				int respo = JOptionPane.showConfirmDialog(null,
						"Deseja realmente remover o produto selecionado?",
						"Importante", JOptionPane.YES_NO_OPTION);
				if (respo == JOptionPane.YES_OPTION)
				{
					DefaultTableModel dtm = (DefaultTableModel) table_Produto
							.getModel();

					for (ProdutoBean produtoBean : listProdutoComprados)
					{

						if (produtoBean.getDescricao().equals(
								dtm.getValueAt(table_Produto.getSelectedRow(),
										1)))
						{
							beanProduto.setQuantidade(Integer
									.parseInt(txtQuantidade.getText())
									+ produtoBean.getQuantidade());
							listProdutoComprados.remove(produtoBean);
							break;
						}

					}
					if (listProdutoComprados.size() == 0)
					{
						btnConvenio.setEnabled(false);
						btnDesconto.setEnabled(false);
					}

					atualizarGrid(dtm);

				}

			}
		} else if (event.getKeyChar() == KeyEvent.VK_ENTER)
		{

			if (event.getSource() == txtCdigoDoProduto
					&& !txtCdigoDoProduto.getText().equals(""))
			{

				pesquisaProdutosPorCodigo();

			}

			/**if (!comboboxProduto.getSelectedItem().equals(""))
			{

				pesquisaProdutosPorNome();

			}**/

			if (event.getSource() == txtQuantidade)
			{

				incluirProdutoTabela();
			}
		}

	}

	@Override
	public void keyReleased(KeyEvent event)
	{

	}

	@Override
	public void keyTyped(KeyEvent event)
	{

	}

	@Override
	public void focusGained(FocusEvent event)
	{
/**
		for (ProdutoBean produtoBean : todosProdutos)
		{
			if (comboboxProduto.getSelectedItem().equals(produtoBean.getDescricao()))
			{
				txtCdigoDoProduto.setText(produtoBean.getId() + ""
						+ produtoBean.getLaboratorio());
				break;
			}
		}**/

	}

	@Override
	public void focusLost(FocusEvent event)
	{
		if (event.getSource() == txtCdigoDoProduto
				&& !txtCdigoDoProduto.getText().equals(""))
		{

			pesquisaProdutosPorCodigo();

		}

		/**if (event.getSource() == comboboxProduto && !comboboxProduto.getSelectedItem().equals(""))
		{

			pesquisaProdutosPorNome();

		}**/

		if (event.getSource() == txtQuantidade)
		{

			incluirProdutoTabela();
		}
	}

	private void pesquisaProdutosPorNome()
	{
		for (ProdutoBean produtoBean : todosProdutos)
		{
			if (comboboxProduto.getSelectedItem().equals(produtoBean.getDescricao()))
			{

				txtCdigoDoProduto.setText(produtoBean.getId() + ""
						+ produtoBean.getLaboratorio());
				txtQuantidade.setText("");
				txtQuantidade.requestFocus();
				break;
			}
		}

	}

	/** Metodo que incluir o produto selecionado na Tabela **/
	private void incluirProdutoTabela()
	{
		int idProdutoSelecionado = 0;
		if (!idProduto.equals("") && !txtQuantidade.getText().equals("")
				|| !comboboxProduto.getSelectedItem().equals(""))
		{
			if (txtQuantidade.getText().equals("")
					|| Integer.parseInt(txtQuantidade.getText()) == 0)
			{
				limpaCampos();
				return;
			}

			if (Integer.parseInt(txtQuantidade.getText()) > 0
					&& !txtQuantidade.getText().equals(""))
			{

				DefaultTableModel dtm = (DefaultTableModel) table_Produto
						.getModel();

				Connection conn = null;
				AcessoBD acessoBD = new AcessoBD();
				boolean verificaStatus = false;
				int quantidadeEstoque = 0;
				try
				{
					conn = acessoBD.obtemConexao();

					for (ProdutoBean produtoBean : todosProdutos)
					{
						if (produtoBean.getDescricao().equals(
								comboboxProduto.getSelectedItem()))
						{
							idProdutoSelecionado = produtoBean.getId();
							break;
						}
					}
					// verificando se a quantidade de produto em estoque � menor
					// doque a quantidade digitada
					quantidadeEstoque = ProdutoDao.consultaProdutoEstoque(conn,
							idProdutoSelecionado);
					if (quantidadeEstoque < Integer.parseInt(txtQuantidade
							.getText()))
					{
						// se for, vai tentar atualizar a quantidade em estoque
						verificaStatus = verificaQuantidadeEstoque(
								quantidadeEstoque, idProdutoSelecionado, conn);
						if (!verificaStatus)
						{
							limpaCampos();
							return;
						}

					}
				} catch (Exception e)
				{
					e.printStackTrace();
				}

				try
				{
					conn = acessoBD.obtemConexao();
					beanProduto = ProdutoDao.buscaPorNome(conn,
							comboboxProduto.getSelectedItem().toString());
					if (beanProduto != null)
					{
						beanProduto.setQuantidade(Integer
								.parseInt(txtQuantidade.getText()));

					}
				} catch (Exception e)
				{
					e.printStackTrace();
					JOptionPane
							.showMessageDialog(
									null,
									"Problema ao carregar Produtos. Por favor, feche a tela e tente novamente, ou entre em contato com Administrador do sistema",
									"Erro", JOptionPane.ERROR_MESSAGE);
				}

				for (ProdutoBean produtoBean : listProdutoComprados)
				{

					if (produtoBean.getDescricao().equals(comboboxProduto.getSelectedItem()))
					{
						int somaQuantidade = Integer.parseInt(txtQuantidade
								.getText()) + produtoBean.getQuantidade();
						if (quantidadeEstoque < somaQuantidade)
						{
							// se for, vai tentar atualizar a quantidade em
							// estoque
							verificaStatus = verificaQuantidadeEstoque(
									somaQuantidade, idProdutoSelecionado, conn);
							if (!verificaStatus)
							{
								limpaCampos();

								return;
							}

						}

						beanProduto.setQuantidade(somaQuantidade);
						listProdutoComprados.remove(produtoBean);
						break;
					}
				}
				while (dtm.getRowCount() > 0)
				{
					dtm.removeRow(0);

				}
				listProdutoComprados.add(beanProduto);

				atualizarGrid(dtm);
				btnConvenio.setEnabled(true);
				btnDesconto.setEnabled(true);
			}
		} else
		{
			JOptionPane.showMessageDialog(null, "Quantidade em branco",
					"Importante", JOptionPane.DEFAULT_OPTION);
			txtQuantidade.requestFocus();
		}
	}

	public boolean verificaQuantidadeEstoque(int quantidadeEstoque,
			int p_idProdutoSelecionado, Connection conn)
	{

		int novaQuantidadeEstoque = 0;
		boolean flagTroca = false;

		int respo = JOptionPane
				.showConfirmDialog(
						null,
						"Quantidade maior do que estoque. Deseja alterar a quantidade de produto em estoque para mais? ",
						"Importante", JOptionPane.YES_NO_OPTION);
		if (respo == JOptionPane.YES_OPTION)
		{
			try
			{
				novaQuantidadeEstoque = Integer.parseInt(JOptionPane
						.showInputDialog("Informe a quantidade maior que :"
								+ quantidadeEstoque, (quantidadeEstoque + 1)));
				if (novaQuantidadeEstoque > quantidadeEstoque)
				{

					if (ProdutoDao.updateEstoque(conn, novaQuantidadeEstoque,
							p_idProdutoSelecionado))
					{
						JOptionPane.showMessageDialog(null,
								"Produto atualizado com sucesso", "Sucesso",
								JOptionPane.INFORMATION_MESSAGE);
						flagTroca = true;
						return flagTroca;
					} else
					{
						JOptionPane.showMessageDialog(null,
								"Erro ao inserir quantidade", "Importante",
								JOptionPane.ERROR_MESSAGE);
						flagTroca = false;
					}
				} else
				{
					JOptionPane.showMessageDialog(null,
							"Informe quantidade maior", "Importante",
							JOptionPane.ERROR_MESSAGE);
					flagTroca = false;
				}

			} catch (Exception e)
			{
				e.printStackTrace();
				JOptionPane.showMessageDialog(null,
						"Erro ao inserir quantidade", "Importante",
						JOptionPane.ERROR_MESSAGE);
				flagTroca = false;
			}

		}

		return flagTroca;
	}

	/** Metodo que atualiza table de produto **/
	private void atualizarGrid(DefaultTableModel dtm)
	{
		while (dtm.getRowCount() > 0)
		{
			dtm.removeRow(0);

		}
		atualizaTotal(dtm, listProdutoComprados);

		for (ProdutoBean produtoBean : listProdutoComprados)
		{

			adcionaLinhas(dtm, produtoBean);
		}
		limpaCampos();
	}

	/**
	 * Metodo que limpa os campos dos produtos e seta o foco no campo de codigo
	 **/
	private void limpaCampos()
	{
		txtCdigoDoProduto.setText("");
		txtCdigoDoProduto.requestFocus();
		txtQuantidade.setText("0");
	}

	/**
	 * Metodo que atualiza o campo de total
	 * 
	 * @param dtm
	 **/
	private void atualizaTotal(DefaultTableModel dtm,
			ArrayList<ProdutoBean> listaProduto)
	{
		txtTotal.setText("");
		double valor = 0;
		for (ProdutoBean produtoBean : listaProduto)
		{

			valor += produtoBean.getValor_venda() * produtoBean.getQuantidade();

		}

		txtTotal.setText(String.format("%.2f", valor));
		txtValorDesconto.setText(String.format("%.2f", valor));

	}

	/** Metodo que acionada linhas na tabela ****/
	private void adcionaLinhas(DefaultTableModel dtm, ProdutoBean produto)
	{
		// C�digo de Produto,Descri��o,Valor de Venda,Quantidades

		dtm.addRow(new Object[] {
				(produto.getId() + "" + produto.getLaboratorio()),
				produto.getDescricao(), produto.getValor_venda(),
				produto.getQuantidade() });
	}

	/**
	 * Metodo que pesquisa o item do campo de codigo, e seta as informa��es no
	 * nome do produto
	 **/
	private void pesquisaProdutosPorCodigo()
	{

		// vamos comparar o codigo digitado com os dados de todos os produtos
		for (ProdutoBean produtoBean : todosProdutos)
		{
			if (txtCdigoDoProduto.getText().equals(
					produtoBean.getId() + "" + produtoBean.getLaboratorio())
					&& !txtCdigoDoProduto.getText().equals(""))
			{
				comboboxProduto.setSelectedItem(produtoBean.getDescricao());
				txtQuantidade.setEnabled(true);
				txtQuantidade.setText("");
				txtQuantidade.requestFocus();

			}
		}

	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		if (comboboxProduto.getSelectedIndex() > 0) {
			//JOptionPane.showMessageDialog(null, "Selecionado " + comboboxProduto.getSelectedItem());
			pesquisaProdutosPorNome();
		}

		if (event.getSource() == btnSair)
		{

			acaoSair();

		} else if (event.getSource() == btnAberturaFechamento)
		{

			Caixa caixa = new Caixa(caixaBean, usuarioLogado);
			if (caixa.aberturaDeCaixa())
			{
				habilitaCampos();
				lblUsuario.setText("Usu�rio Logado : "
						+ usuarioLogado.getNome().toUpperCase()
						+ " | Caixa Aberto");
			}

		}

		else if (event.getSource() == btnCancelar)
		{

			acaoCancelar();

		} else if (event.getSource() == btnConvenio)
		{
			acaoConvenio();

		} else if (event.getSource() == btnFinalizar)
		{
			acaoFinalizar();

		} else if (event.getSource() == btnDesconto)
		{
			acaoDesconto();
		}

	}

	private void acaoDesconto()
	{
		try
		{
			valordesconto = Double.parseDouble(JOptionPane
					.showInputDialog("Informe o valor do desconto"));
			NumberFormat nf = new DecimalFormat("#,##0.00",
					new DecimalFormatSymbols(new Locale("pt", "BR")));
			double novoValor = (nf.parse(txtTotal.getText()).doubleValue() * (valordesconto / 100))
					- nf.parse(txtTotal.getText()).doubleValue();
			lblDesconto.setText("Desconto " + valordesconto + " %");
			txtValorDesconto.setText(String.format("%.2f", novoValor));
		} catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, "Informe um numero",
					"Importante", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void acaoFinalizar()
	{
		Connection conn = null;
		if (!btnCartao.isSelected() && !btnCheque.isSelected()
				&& !btnDinheiro.isSelected())
		{
			JOptionPane.showMessageDialog(null,
					"Nenhuma forma de pagamento selecionada");
		}

		try
		{
			AcessoBD acessoBD = new AcessoBD();
			conn = acessoBD.obtemConexao();
			VendaBean venda = criarVenda(conn);

			if (venda != null)
			{

				verificaOpcaoSelecionada();

				if (FormaPagamentoDao.inseriFormaPagamento(conn,
						verificaOpcaoSelecionada()))
				{
					for (ProdutoBean produtoBean : listProdutoComprados)
					{
						int quantidadeEstoque = ProdutoDao
								.consultaProdutoEstoque(conn,
										produtoBean.getId());
						int novaQuantidade = ((quantidadeEstoque - produtoBean
								.getQuantidade()) >= 0 ? (quantidadeEstoque - produtoBean
								.getQuantidade()) : 0);
						ProdutoDao.updateEstoque(conn, novaQuantidade,
								produtoBean.getId());
					}
					JOptionPane.showMessageDialog(null,
							"Venda realizada com sucesso", "Sucesso",
							JOptionPane.INFORMATION_MESSAGE);
					atualizarFrame();
					habilitaCampos();
				}
			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void acaoConvenio()
	{
		int respo = JOptionPane.showConfirmDialog(null,
				"Deseja realmente ir para convenio?", "Importante",
				JOptionPane.YES_NO_OPTION);
		if (respo == JOptionPane.YES_OPTION)
		{
			Connection conn = null;
			try
			{
				AcessoBD acessoBD = new AcessoBD();
				conn = acessoBD.obtemConexao();
				VendaBean venda = criarVenda(conn);

				if (venda != null)
				{

					Convenio convenio = new Convenio();
					if (convenio.cadastroConvenio())
					{
						atualizarFrame();
						habilitaCampos();

					}

				}

			} catch (Exception e)
			{
				e.printStackTrace();
			}

		}
	}

	private void acaoCancelar()
	{
		int respo = JOptionPane.showConfirmDialog(null,
				"Deseja realmente cancelar essa venda? ", "Importante",
				JOptionPane.YES_NO_OPTION);
		if (respo == JOptionPane.YES_OPTION)
		{
			atualizarFrame();
			habilitaCampos();

		}
	}

	private void acaoSair()
	{
		int respo = JOptionPane.showConfirmDialog(null,
				"Deseja realmente sair da venda? ", "Importante",
				JOptionPane.YES_NO_OPTION);
		if (respo == JOptionPane.YES_OPTION)
		{
			dispose();

		}
	}

	public void atualizarFrame()
	{
		Venda venda = new Venda(usuarioLogado);

		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();

		venda.setBounds(0, 0, screenSize.width, screenSize.height);

		this.dispose();
		venda.setVisible(true);
	}

	private void registrarAcoesDoTeclado(JPanel painel)
	{
		// Damos um nome para cada a��o. Esse nome � �til pois mais de
		// uma tecla pode ser associada a cada a��o, como veremos abaixo
		ActionMap actionMap = painel.getActionMap();
		actionMap.put("botao1", (Action) btnSair);
		painel.setActionMap(actionMap);

		// Pegamos o input map que ocorre sempre que a janela atual est� em foco
		InputMap imap = painel.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW);
		imap.put(KeyStroke.getKeyStroke("F10"), "botao1");
	}

	public int[] verificaOpcaoSelecionada()
	{
		int[] formaPagamento = null;
		if (btnCartao.isSelected() && btnCheque.isSelected()
				&& btnDinheiro.isSelected())
		{
			formaPagamento = new int[3];
			formaPagamento[0] = 1;
			formaPagamento[1] = 2;
			formaPagamento[2] = 3;
			return formaPagamento;
		} else if (btnCartao.isSelected() && btnCheque.isSelected())
		{
			formaPagamento = new int[2];
			formaPagamento[0] = 1;
			formaPagamento[1] = 3;
			return formaPagamento;

		} else if (btnCartao.isSelected() && btnDinheiro.isSelected())
		{
			formaPagamento = new int[2];
			formaPagamento[0] = 1;
			formaPagamento[1] = 2;
			return formaPagamento;

		}

		else if (btnCheque.isSelected() && btnDinheiro.isSelected())
		{
			formaPagamento = new int[2];
			formaPagamento[0] = 3;
			formaPagamento[1] = 2;
			return formaPagamento;

		} else if (btnCheque.isSelected())
		{
			formaPagamento = new int[1];
			formaPagamento[0] = 3;
			return formaPagamento;

		}

		else if (btnDinheiro.isSelected())
		{
			formaPagamento = new int[1];
			formaPagamento[0] = 1;
			return formaPagamento;

		}

		else if (btnCartao.isSelected())
		{
			formaPagamento = new int[1];
			formaPagamento[0] = 2;
			return formaPagamento;

		}
		return formaPagamento;
	}

	/** Metod que criar a venda e retorna a venda criada ***/
	private VendaBean criarVenda(Connection conn)
	{

		NumberFormat nf = new DecimalFormat("#,##0.00",
				new DecimalFormatSymbols(new Locale("pt", "BR")));

		VendaBean vendaBean = new VendaBean();

		vendaBean.setUsuario(usuarioLogado.getNome());
		// vendaBean.setUsuario(usuarioLogado.getNome());
		try
		{
			vendaBean.setSub_total(nf.parse(txtTotal.getText()).doubleValue());
		} catch (ParseException e)
		{
			
			e.printStackTrace();
		}
		vendaBean.setDesconto((int) (valordesconto));
		vendaBean.setValor_desconto(vendaBean.getSub_total()
				* (valordesconto / 100));
		vendaBean.setData(datePicker_Venda.getDate());

		if (VendaDao.inseriVenda(conn, vendaBean))
		{
			return vendaBean;
		}

		return null;
	}
}
