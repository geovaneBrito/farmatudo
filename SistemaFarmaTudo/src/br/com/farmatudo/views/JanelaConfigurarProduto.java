package br.com.farmatudo.views;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;

/*
 * /*
 */
import br.com.satcfe.ac.controles.ControleArquivos;
import br.com.satcfe.ac.interfaces.JanelaAC;
import br.com.satcfe.ac.interfaces.JanelaCOFINSAliq;
import br.com.satcfe.ac.interfaces.JanelaCOFINSNT;
import br.com.satcfe.ac.interfaces.JanelaCOFINSQtde;
import br.com.satcfe.ac.interfaces.JanelaICMS00;
import br.com.satcfe.ac.interfaces.JanelaICMS40;
import br.com.satcfe.ac.interfaces.JanelaICMSSN102;
import br.com.satcfe.ac.interfaces.JanelaICMSSN900;
import br.com.satcfe.ac.interfaces.JanelaISSQN;
import br.com.satcfe.ac.interfaces.JanelaPISAliq;
import br.com.satcfe.ac.interfaces.JanelaPISNT;
import br.com.satcfe.ac.interfaces.JanelaPISQtde;
import br.com.satcfe.ac.interfaces.SplashScreen;
import br.com.satcfe.ac.modelos.cfeac.COFINSAliq;
import br.com.satcfe.ac.modelos.cfeac.COFINSNT;
import br.com.satcfe.ac.modelos.cfeac.COFINSQtde;
import br.com.satcfe.ac.modelos.cfeac.DetalhamentoProdutosCFe;
import br.com.satcfe.ac.modelos.cfeac.ICMS00;
import br.com.satcfe.ac.modelos.cfeac.ICMS40;
import br.com.satcfe.ac.modelos.cfeac.ICMSSN102;
import br.com.satcfe.ac.modelos.cfeac.ICMSSN900;
import br.com.satcfe.ac.modelos.cfeac.InformacoesCFe;
import br.com.satcfe.ac.modelos.cfeac.InformacoesCOFINS;
import br.com.satcfe.ac.modelos.cfeac.InformacoesICMS;
import br.com.satcfe.ac.modelos.cfeac.InformacoesPIS;
import br.com.satcfe.ac.modelos.cfeac.PISAliq;
import br.com.satcfe.ac.modelos.cfeac.PISNT;
import br.com.satcfe.ac.modelos.cfeac.PISQtde;

public class JanelaConfigurarProduto extends javax.swing.JFrame
{
	private static final long serialVersionUID = 1L;
	/* 40 */ private JPanel jContentPane = null;

	/* 42 */ private JLabel labelCodigo = null;

	/* 44 */ private JLabel labelCodigoBarras = null;

	/* 46 */ private JLabel labelDescricao = null;

	/* 48 */ private JLabel labelNCM = null;

	/* 50 */ private JLabel labelCFOP = null;

	/* 52 */ private JLabel labelUCom = null;

	/* 54 */ private JLabel labelVUCom = null;

	/* 56 */ private JLabel labelIndRegra = null;

	/* 58 */ private JTextField textoCodigo = null;

	/* 60 */ private JTextField textoCodigoBarra = null;

	/* 62 */ private JTextField textoDescricao = null;

	/* 64 */ private JTextField textoNCM = null;

	/* 66 */ private JTextField textoCFOP = null;

	/* 68 */ private JTextField textoUCom = null;

	/* 70 */ private JTextField textoVUn = null;

	/* 72 */ private JTextField textoindRegra = null;

	/* 74 */ private JButton botaoOK = null;

	/* 76 */ private JButton botaoCancelar = null;

	private InformacoesCFe baseProd;

	/* 80 */ private JComboBox comboProdutos = null;

	/* 82 */ private JLabel labelProdutos = null;

	/* 84 */ private String referenciaAntiga = "";

	/* 86 */ private JLabel labelTributacaoICMS = null;

	/* 88 */ private JComboBox comboICMS = null;

	/* 90 */ private JLabel labelTributacaoPIS = null;

	/* 92 */ private JLabel labelTributacaoCOFINS = null;

	/* 94 */ private JComboBox comboPIS = null;

	/* 96 */ private JComboBox comboCOFINS = null;

	/* 98 */ protected JanelaICMS00 jICMS00 = null;

	/* 100 */ protected JanelaICMS40 jICMS40 = null;

	/* 102 */ protected JanelaICMSSN102 jICMSSN102 = null;

	/* 104 */ protected JanelaICMSSN900 jICMSSN900 = null;

	/* 106 */ protected JanelaCOFINSAliq jCOFINSAliq = null;

	/* 108 */ protected JanelaCOFINSQtde jCOFINSQtde = null;

	/* 110 */ protected JanelaCOFINSNT jCOFINSNT = null;

	/* 112 */ protected JanelaPISAliq jPISAliq = null;

	/* 114 */ protected JanelaPISQtde jPISQtde = null;

	/* 116 */ protected JanelaPISNT jPISNT = null;

	public JanelaConfigurarProduto(InformacoesCFe baseProd)
	{
		/* 120 */ this.baseProd = baseProd;
		/* 121 */ initialize();
	}

	private void initialize()
	{
		try
		{
			/* 133 */ UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e)
		{
			/* 136 */ e.printStackTrace();
		}
		/* 138 */ setSize(287, 730);
		/* 139 */ setResizable(false);
		/* 140 */ // setLocation(getDefaultToolkitgetScreenSizewidth / 2 -
					// getWidth() / 2, getDefaultToolkitgetScreenSizeheight / 2
					// - getHeight() / 2);
		/* 141 */ setTitle("Configurar produto");
		/* 142 */ setContentPane(getJContentPane());
	}

	private JPanel getJContentPane()
	{
		/* 152 */ if (jContentPane == null)
		{
			/* 154 */ labelTributacaoCOFINS = new JLabel();
			/* 155 */ labelTributacaoCOFINS.setBounds(new Rectangle(13, 599, 120, 16));
			/* 156 */ labelTributacaoCOFINS.setText("Tributa��o COFINS:");
			/* 157 */ labelTributacaoPIS = new JLabel();
			/* 158 */ labelTributacaoPIS.setBounds(new Rectangle(13, 547, 96, 16));
			/* 159 */ labelTributacaoPIS.setText("Tributa��o PIS:");
			/* 160 */ labelTributacaoICMS = new JLabel();
			/* 161 */ labelTributacaoICMS.setBounds(new Rectangle(13, 495, 101, 16));
			/* 162 */ labelTributacaoICMS.setText("Tributa��o ICMS:");
			/* 163 */ labelProdutos = new JLabel();
			/* 164 */ labelProdutos.setBounds(new Rectangle(13, 13, 79, 16));
			/* 165 */ labelProdutos.setText("Produtos");
			/* 166 */ labelIndRegra = new JLabel();
			/* 167 */ labelIndRegra.setBounds(new Rectangle(13, 441, 150, 16));
			/* 168 */ labelIndRegra.setText("Regra de calculo (A ou T)");
			/* 169 */ labelVUCom = new JLabel();
			/* 170 */ labelVUCom.setBounds(new Rectangle(13, 389, 89, 16));
			/* 171 */ labelVUCom.setText("Valor unit�rio");
			/* 172 */ labelUCom = new JLabel();
			/* 173 */ labelUCom.setBounds(new Rectangle(13, 337, 113, 16));
			/* 174 */ labelUCom.setText("Unidade comercial");
			/* 175 */ labelCFOP = new JLabel();
			/* 176 */ labelCFOP.setBounds(new Rectangle(13, 285, 240, 16));
			/* 177 */ labelCFOP.setText("C�digo Fiscal de Opera��es e Presta��es");
			/* 178 */ labelNCM = new JLabel();
			/* 179 */ labelNCM.setBounds(new Rectangle(13, 233, 73, 16));
			/* 180 */ labelNCM.setText("C�digo NCM");
			/* 181 */ labelDescricao = new JLabel();
			/* 182 */ labelDescricao.setBounds(new Rectangle(13, 181, 132, 16));
			/* 183 */ labelDescricao.setText("Descri��o do produto");
			/* 184 */ labelCodigoBarras = new JLabel();
			/* 185 */ labelCodigoBarras.setBounds(new Rectangle(13, 129, 106, 16));
			/* 186 */ labelCodigoBarras.setText("C�digo de barras");
			/* 187 */ labelCodigo = new JLabel();
			/* 188 */ labelCodigo.setBounds(new Rectangle(13, 77, 132, 16));
			/* 189 */ labelCodigo.setText("Refer�ncia do produto");
			/* 190 */ jContentPane = new JPanel();
			/* 191 */ jContentPane.setLayout(null);
			/* 192 */ jContentPane.add(labelCodigo, null);
			/* 193 */ jContentPane.add(labelCodigoBarras, null);
			/* 194 */ jContentPane.add(labelDescricao, null);
			/* 195 */ jContentPane.add(labelNCM, null);
			/* 196 */ jContentPane.add(labelCFOP, null);
			/* 197 */ jContentPane.add(labelUCom, null);
			/* 198 */ jContentPane.add(labelVUCom, null);
			/* 199 */ jContentPane.add(labelIndRegra, null);
			/* 200 */ jContentPane.add(getTextoCodigo(), null);
			/* 201 */ jContentPane.add(getTextoCodigoBarra(), null);
			/* 202 */ jContentPane.add(getTextoDescricao(), null);
			/* 203 */ jContentPane.add(getTextoNCM(), null);
			/* 204 */ jContentPane.add(getTextoCFOP(), null);
			/* 205 */ jContentPane.add(getTextoUCom(), null);
			/* 206 */ jContentPane.add(getTextoVUn(), null);
			/* 207 */ jContentPane.add(getTextoindRegra(), null);
			/* 208 */ jContentPane.add(getBotaoOK(), null);
			/* 209 */ jContentPane.add(getBotaoCancelar(), null);
			/* 210 */ jContentPane.add(labelProdutos, null);
			/* 211 */ jContentPane.add(labelTributacaoICMS, null);
			/* 212 */ jContentPane.add(getComboImposto(), null);
			/* 213 */ jContentPane.add(labelTributacaoPIS, null);
			/* 214 */ jContentPane.add(labelTributacaoCOFINS, null);
			/* 215 */ jContentPane.add(getComboPIS(), null);
			/* 216 */ jContentPane.add(getComboCOFINS(), null);
			/* 217 */ jContentPane.add(getComboProdutos(), null);
		}
		/* 219 */ return jContentPane;
	}

	private JTextField getTextoCodigo()
	{
		/* 229 */ if (textoCodigo == null)
		{
			/* 231 */ textoCodigo = new JTextField();
			/* 232 */ textoCodigo.setBounds(new Rectangle(13, 99, 258, 25));
		}
		/* 234 */ return textoCodigo;
	}

	private JTextField getTextoCodigoBarra()
	{
		/* 244 */ if (textoCodigoBarra == null)
		{
			/* 246 */ textoCodigoBarra = new JTextField();
			/* 247 */ textoCodigoBarra.setBounds(new Rectangle(13, 151, 258, 25));
		}
		/* 249 */ return textoCodigoBarra;
	}

	private JTextField getTextoDescricao()
	{
		/* 259 */ if (textoDescricao == null)
		{
			/* 261 */ textoDescricao = new JTextField();
			/* 262 */ textoDescricao.setBounds(new Rectangle(13, 203, 258, 25));
		}
		/* 264 */ return textoDescricao;
	}

	private JTextField getTextoNCM()
	{
		/* 274 */ if (textoNCM == null)
		{
			/* 276 */ textoNCM = new JTextField();
			/* 277 */ textoNCM.setBounds(new Rectangle(13, 255, 258, 25));
		}
		/* 279 */ return textoNCM;
	}

	private JTextField getTextoCFOP()
	{
		/* 284 */ if (textoCFOP == null)
		{
			/* 286 */ textoCFOP = new JTextField();
			/* 287 */ textoCFOP.setBounds(new Rectangle(13, 307, 258, 25));
			/* 288 */ textoCFOP.addKeyListener(new KeyListener()
			{
				public void keyTyped(KeyEvent e)
				{
					/* 292 */ Long.parseLong(textoCFOP.getText() + e.getKeyChar());
				}

				public void keyReleased(KeyEvent e)
				{
				}

				public void keyPressed(KeyEvent e)
				{
				}
			});
		}

		/* 304 */ return textoCFOP;
	}

	private JTextField getTextoUCom()
	{
		/* 314 */ if (textoUCom == null)
		{
			/* 316 */ textoUCom = new JTextField();
			/* 317 */ textoUCom.setBounds(new Rectangle(13, 359, 258, 25));
		}
		/* 319 */ return textoUCom;
	}

	private JTextField getTextoVUn()
	{
		/* 329 */ if (textoVUn == null)
		{
			/* 331 */ textoVUn = new JTextField();
			/* 332 */ textoVUn.setBounds(new Rectangle(13, 411, 258, 25));
			/* 333 */ textoVUn.addKeyListener(new KeyListener()
			{
				public void keyTyped(KeyEvent e)
				{
					/* 337 */ Double.parseDouble(textoVUn.getText() + e.getKeyChar());
				}

				public void keyReleased(KeyEvent e)
				{
				}

				public void keyPressed(KeyEvent e)
				{
				}
			});
		}

		/* 349 */ return textoVUn;
	}

	private JTextField getTextoindRegra()
	{
		/* 359 */ if (textoindRegra == null)
		{
			/* 361 */ textoindRegra = new JTextField();
			/* 362 */ textoindRegra.setBounds(new Rectangle(13, 463, 258, 25));
		}
		/* 364 */ return textoindRegra;
	}

	private JButton getBotaoOK()
	{
		/* 374 */ if (botaoOK == null)
		{
			/* 376 */ botaoOK = new JButton();
			/* 377 */ botaoOK.setBounds(new Rectangle(13, 663, 100, 26));
			/* 378 */ botaoOK.setText("Salvar");
			/* 379 */ botaoOK.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					/* 383 */ String cProd = textoCodigo.getText();
					/* 384 */ String cEAN = textoCodigoBarra.getText();
					/* 385 */ String xProd = textoDescricao.getText();
					/* 386 */ String NCM = textoNCM.getText();
					/* 387 */ String CFOP = textoCFOP.getText();
					/* 388 */ String uCom = textoUCom.getText();
					/* 389 */ String vUnCom = textoVUn.getText();
					/* 390 */ String indRegra = textoindRegra.getText();
					/* 391 */ if (cProd.length() <= 0)
					{
						/* 393 */ JOptionPane.showMessageDialog(null,
								"O campo \"Refer�ncia do produto\" � de preenchimento obrigat�rio!");
						/* 394 */ return;
					}
					/* 395 */ if (xProd.length() <= 0)
					{
						/* 397 */ JOptionPane.showMessageDialog(null,
								"O campo \"Descri��o do produto\" � de preenchimento obrigat�rio!");
						/* 398 */ return;
					}
					/* 399 */ if (CFOP.length() <= 0)
					{
						/* 401 */ JOptionPane.showMessageDialog(null,
								"O campo \"C�digo Fiscal de Opera��es e Presta��es\" � de preenchimento obrigat�rio!");
						/* 402 */ return;
					}
					/* 403 */ if (uCom.length() <= 0)
					{
						/* 405 */ JOptionPane.showMessageDialog(null,
								"O campo \"Unidade Comercial\" � de preenchimento obrigat�rio!");
						/* 406 */ return;
					}
					/* 407 */ if (vUnCom.length() <= 0)
					{
						/* 409 */ JOptionPane.showMessageDialog(null,
								"O campo \"Valor unit�rio\" � de preenchimento obrigat�rio!");
						/* 410 */ return;
					}
					/* 411 */ if (indRegra.length() <= 0)
					{
						/* 413 */ JOptionPane.showMessageDialog(null,
								"O campo \"Regra de calculo (A ou T)\" � de preenchimento obrigat�rio!");
						/* 414 */ return;
					}
					/* 415 */ if ((!indRegra.equals("A")) && (!indRegra.equals("T")))
					{
						/* 417 */ JOptionPane.showMessageDialog(null,
								"O campo \"Regra de calculo (A ou T)\" deve ser preenchido com \"A\" para arredondamento e \"T\" para truncamento!");
						/* 418 */ return;
					}
					/* 420 */ ArrayList<DetalhamentoProdutosCFe> produtos = baseProd.getDet();
					/* 421 */ for (DetalhamentoProdutosCFe d : produtos)
					{
						/* 423 */ if ((d.getProd().getcProd().equals(cProd)) && (!cProd.equals(referenciaAntiga)))
						{
							/* 425 */ JOptionPane.showMessageDialog(null, "Refer�ncia do produto j� existente!");
							/* 426 */ return;
						}
					}
					/* 429 */ if (cEAN.length() <= 0)/* 430 */ cEAN = null;
					/* 431 */ if (NCM.length() <= 0)/* 432 */ NCM = null;
					/* 433 */ DetalhamentoProdutosCFe produto = (DetalhamentoProdutosCFe) comboProdutos
							.getSelectedItem();
					/* 434 */ produto.getProd().setcProd(cProd);
					/* 435 */ produto.getProd().setcEAN(cEAN);
					/* 436 */ produto.getProd().setxProd(xProd);
					/* 437 */ produto.getProd().setNCM(NCM);
					/* 438 */ produto.getProd().setCFOP(CFOP);
					/* 439 */ produto.getProd().setuCom(uCom);
					/* 440 */ produto.getProd().setqCom("1.000");
					/* 441 */ produto.getProd().setvUnCom(vUnCom);
					/* 442 */ produto.getProd().setIndRegra(indRegra);
					/* 443 */ InformacoesICMS infICMS = null;
					/* 444 */ InformacoesPIS infPIS = null;
					/* 445 */ InformacoesCOFINS infCOFINS = null;
					/* 446 */ if (jICMS00 != null)
					{
						/* 448 */ ICMS00 i00 = new ICMS00(jICMS00.orig, jICMS00.CST, jICMS00.aliq);
						/* 449 */ infICMS = new InformacoesICMS(i00);
						/* 450 */ produto.getImposto().setICMS(infICMS);
						/* 451 */ } else if (jICMS40 != null)
					{
						/* 453 */ ICMS40 i40 = new ICMS40(jICMS40.orig, jICMS40.CST);
						/* 454 */ infICMS = new InformacoesICMS(i40);
						/* 455 */ produto.getImposto().setICMS(infICMS);
						/* 456 */ } else if (jICMSSN102 != null)
					{
						/* 458 */ ICMSSN102 i102 = new ICMSSN102(jICMSSN102.orig, jICMSSN102.CSOSN);
						/* 459 */ infICMS = new InformacoesICMS(i102);
						/* 460 */ produto.getImposto().setICMS(infICMS);
						/* 461 */ } else if (jICMSSN900 != null)
					{
						/* 463 */ ICMSSN900 i900 = new ICMSSN900(jICMSSN900.orig, jICMSSN900.CSOSN, jICMSSN900.aliq);
						/* 464 */ infICMS = new InformacoesICMS(i900);
						/* 465 */ produto.getImposto().setICMS(infICMS);
					}
					/* 467 */ if (jPISAliq != null)
					{
						/* 469 */ PISAliq pAliq = new PISAliq(jPISAliq.CST, jPISAliq.vBC, jPISAliq.aliq);
						/* 470 */ infPIS = new InformacoesPIS(pAliq);
						/* 471 */ produto.getImposto().setPIS(infPIS);
						/* 472 */ } else if (jPISQtde != null)
					{
						/* 474 */ PISQtde pQtde = new PISQtde(jPISQtde.CST, "", jPISQtde.aliq);
						/* 475 */ infPIS = new InformacoesPIS(pQtde);
						/* 476 */ produto.getImposto().setPIS(infPIS);
						/* 477 */ } else if (jPISNT != null)
					{
						/* 479 */ PISNT pNT = new PISNT(jPISNT.CST);
						/* 480 */ infPIS = new InformacoesPIS(pNT);
						/* 481 */ produto.getImposto().setPIS(infPIS);
					}
					/* 483 */ if (jCOFINSAliq != null)
					{
						/* 485 */ COFINSAliq cAliq = new COFINSAliq(jCOFINSAliq.CST, jCOFINSAliq.vBC, jCOFINSAliq.aliq);
						/* 486 */ infCOFINS = new InformacoesCOFINS(cAliq);
						/* 487 */ produto.getImposto().setCOFINS(infCOFINS);
						/* 488 */ } else if (jCOFINSQtde != null)
					{
						/* 490 */ COFINSQtde cQtde = new COFINSQtde(jCOFINSQtde.CST, "", jCOFINSQtde.aliq);
						/* 491 */ infCOFINS = new InformacoesCOFINS(cQtde);
						/* 492 */ produto.getImposto().setCOFINS(infCOFINS);
						/* 493 */ } else if (jCOFINSNT != null)
					{
						/* 495 */ COFINSNT cNT = new COFINSNT(jCOFINSNT.CST);
						/* 496 */ infCOFINS = new InformacoesCOFINS(cNT);
						/* 497 */ produto.getImposto().setCOFINS(infCOFINS);
					}
					/* 499 */ ControleArquivos.escreverCaracteresArquivo("C:/AC/DB/DB_PROD.xml",
							baseProd.toString().toCharArray());
					/* 500 */ jICMS00 = null;
					/* 501 */ jICMS40 = null;
					/* 502 */ jICMSSN102 = null;
					/* 503 */ jICMSSN900 = null;
					/* 504 */ jCOFINSAliq = null;
					/* 505 */ jCOFINSQtde = null;
					/* 506 */ jCOFINSNT = null;
					/* 507 */ jPISAliq = null;
					/* 508 */ jPISQtde = null;
					/* 509 */ jPISNT = null;
					/* 510 */ JOptionPane.showMessageDialog(JanelaConfigurarProduto.this,
							"Altera��es salvas com sucesso!");
				}
			});
		}
		/* 514 */ return botaoOK;
	}

	private JButton getBotaoCancelar()
	{
		/* 524 */ if (botaoCancelar == null)
		{
			/* 526 */ botaoCancelar = new JButton();
			/* 527 */ botaoCancelar.setBounds(new Rectangle(171, 663, 100, 26));
			/* 528 */ botaoCancelar.setText("Sair");
			/* 529 */ botaoCancelar.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					/* 533 */ dispose();
				}
			});
		}
		/* 537 */ return botaoCancelar;
	}

	private JComboBox getComboProdutos()
	{
		/* 547 */ if (comboProdutos == null)
		{
			/* 549 */ comboProdutos = new JComboBox();
			/* 550 */ comboProdutos.setBounds(new Rectangle(13, 35, 258, 20));
			/* 551 */ ArrayList<DetalhamentoProdutosCFe> array = baseProd.getDet();
			/* 552 */ for (DetalhamentoProdutosCFe o : array)
				/* 553 */ comboProdutos.addItem(o);
			/* 554 */ selecionar();
			/* 555 */ comboProdutos.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					/* 559 */ JanelaConfigurarProduto.this.selecionar();
				}
			});
		}
		/* 563 */ return comboProdutos;
	}

	private void selecionar()
	{
		ActionListener[] arrayOfActionListener;
		/* 568 */ int j = (arrayOfActionListener = comboICMS.getActionListeners()).length;
		for (int i = 0; i < j; i++)
		{
			ActionListener al = arrayOfActionListener[i];

			/* 570 */ comboICMS.removeActionListener(al);
		}
		/* 572 */
		j = (arrayOfActionListener = comboPIS.getActionListeners()).length;
		for (int i = 0; i < j; i++)
		{
			ActionListener al = arrayOfActionListener[i];

			/* 574 */ comboPIS.removeActionListener(al);

		}
		/* 576 */ j = (arrayOfActionListener = comboCOFINS.getActionListeners()).length;
		for (int i = 0; i < j; i++)
		{
			ActionListener al = arrayOfActionListener[i];

			/* 578 */ comboCOFINS.removeActionListener(al);
		}

		/* 581 */ DetalhamentoProdutosCFe selecionado = (DetalhamentoProdutosCFe) comboProdutos.getSelectedItem();
		/* 582 */ textoCodigo.setText(referenciaAntiga = selecionado.getProd().getcProd());
		/* 583 */ textoCodigoBarra.setText(selecionado.getProd().getcEAN());
		/* 584 */ textoDescricao.setText(selecionado.getProd().getxProd());
		/* 585 */ textoNCM.setText(selecionado.getProd().getNCM());
		/* 586 */ textoCFOP.setText(selecionado.getProd().getCFOP());
		/* 587 */ textoUCom.setText(selecionado.getProd().getuCom());
		/* 588 */ textoVUn.setText(selecionado.getProd().getvUnCom());
		/* 589 */ textoindRegra.setText(selecionado.getProd().getIndRegra());
		/* 590 */ InformacoesICMS infICMS = selecionado.getImposto().getICMS();
		/* 591 */ ICMS00 i00 = infICMS.getICMS00();
		/* 592 */ ICMS40 i40 = infICMS.getICMS40();
		/* 593 */ ICMSSN102 i102 = infICMS.getICMSSN102();
		/* 594 */ ICMSSN900 i900 = infICMS.getICMSSN900();
		/* 595 */ if (i00 != null)
		{
			/* 597 */ String CST = i00.getCST();
			/* 598 */ if (CST.equals("00"))
			{
				/* 600 */ comboICMS.setSelectedIndex(0);
				/* 601 */ } else if (CST.equals("20"))
			{
				/* 603 */ comboICMS.setSelectedIndex(1);
				/* 604 */ } else if (CST.equals("90"))
			{
				/* 606 */ comboICMS.setSelectedIndex(6);
			}
			/* 608 */ } else if (i40 != null)
		{
			/* 610 */ String CST = i40.getCST();
			/* 611 */ if (CST.equals("40"))
			{
				/* 613 */ comboICMS.setSelectedIndex(2);
				/* 614 */ } else if (CST.equals("41"))
			{
				/* 616 */ comboICMS.setSelectedIndex(3);
				/* 617 */ } else if (CST.equals("50"))
			{
				/* 619 */ comboICMS.setSelectedIndex(4);
				/* 620 */ } else if (CST.equals("60"))
			{
				/* 622 */ comboICMS.setSelectedIndex(5);
			}
			/* 624 */ } else if (i102 != null)
		{
			/* 626 */ String CST = i102.getCSOSN();
			/* 627 */ if (CST.equals("102"))
			{
				/* 629 */ comboICMS.setSelectedIndex(7);
				/* 630 */ } else if (CST.equals("300"))
			{
				/* 632 */ comboICMS.setSelectedIndex(8);
				/* 633 */ } else if (CST.equals("500"))
			{
				/* 635 */ comboICMS.setSelectedIndex(9);
			}
			/* 637 */ } else if (i900 != null)
		{
			/* 639 */ comboICMS.setSelectedIndex(10);
		}
		/* 641 */ InformacoesPIS infPIS = selecionado.getImposto().getPIS();
		/* 642 */ PISAliq pAliq = infPIS.getPISAliq();
		/* 643 */ PISQtde pQtde = infPIS.getPISQTde();
		/* 644 */ PISNT pNT = infPIS.getPISNT();
		/* 645 */ if (pAliq != null)
		{
			/* 647 */ comboPIS.setSelectedIndex(0);
			/* 648 */ } else if (pQtde != null)
		{
			/* 650 */ comboPIS.setSelectedIndex(1);
			/* 651 */ } else if (pNT != null)
		{
			/* 653 */ comboPIS.setSelectedIndex(2);
		}
		/* 655 */ InformacoesCOFINS infCOFINS = selecionado.getImposto().getCOFINS();
		/* 656 */ COFINSAliq cAliq = infCOFINS.getCOFINSAliq();
		/* 657 */ COFINSQtde cQtde = infCOFINS.getCOFINSQtde();
		/* 658 */ COFINSNT cNT = infCOFINS.getCOFINSNT();
		/* 659 */ if (cAliq != null)
		{
			/* 661 */ comboCOFINS.setSelectedIndex(0);
			/* 662 */ } else if (cQtde != null)
		{
			/* 664 */ comboCOFINS.setSelectedIndex(1);
			/* 665 */ } else if (cNT != null)
		{
			/* 667 */ comboCOFINS.setSelectedIndex(2);
		}
		/* 669 */ comboPIS.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				/* 673 */ int index = ((JComboBox) e.getSource()).getSelectedIndex();
				/* 674 */ if (index == 0)
				{
					/* 676 */ jPISAliq = new JanelaPISAliq();
					/* 677 */ jPISAliq.setVisible(true);
					/* 678 */ jPISQtde = null;
					/* 679 */ jPISNT = null;
					/* 680 */ } else if (index == 1)
				{
					/* 682 */ jPISQtde = new JanelaPISQtde();
					/* 683 */ jPISQtde.setVisible(true);
					/* 684 */ jPISAliq = null;
					/* 685 */ jPISNT = null;
					/* 686 */ } else if (index == 2)
				{
					/* 688 */ jPISNT = new JanelaPISNT();
					/* 689 */ jPISNT.setVisible(true);
					/* 690 */ jPISAliq = null;
					/* 691 */ jPISQtde = null;
				}
			}
			/* 694 */ });
		/* 695 */ comboICMS.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				/* 699 */ int index = ((JComboBox) e.getSource()).getSelectedIndex();
				/* 700 */ if ((index == 0) || (index == 1) || (index == 6))
				{
					/* 702 */ String CST = "";
					/* 703 */ if (index == 0)
					{
						/* 704 */ CST = "00";
						/* 705 */ } else if (index == 1)
					{
						/* 706 */ CST = "20";
					} else/* 708 */ CST = "90";
					/* 709 */ jICMS00 = new JanelaICMS00(CST);
					/* 710 */ jICMS00.setVisible(true);
					/* 711 */ jICMS40 = null;
					/* 712 */ jICMSSN102 = null;
					/* 713 */ jICMSSN900 = null;
					/* 714 */ } else if ((index == 2) || (index == 3) || (index == 4) || (index == 5))
				{
					/* 716 */ String CST = "";
					/* 717 */ if (index == 2)
					{
						/* 718 */ CST = "40";
						/* 719 */ } else if (index == 3)
					{
						/* 720 */ CST = "41";
						/* 721 */ } else if (index == 4)
					{
						/* 722 */ CST = "50";
					} else/* 724 */ CST = "60";
					/* 725 */ jICMS40 = new JanelaICMS40(CST);
					/* 726 */ jICMS40.setVisible(true);
					/* 727 */ jICMS00 = null;
					/* 728 */ jICMSSN102 = null;
					/* 729 */ jICMSSN900 = null;
					/* 730 */ } else if ((index == 7) || (index == 8) || (index == 9))
				{
					/* 732 */ String CSOSN = "";
					/* 733 */ if (index == 0)
					{
						/* 734 */ CSOSN = "102";
						/* 735 */ } else if (index == 1)
					{
						/* 736 */ CSOSN = "300";
					} else/* 738 */ CSOSN = "500";
					/* 739 */ jICMSSN102 = new JanelaICMSSN102(CSOSN);
					/* 740 */ jICMSSN102.setVisible(true);
					/* 741 */ jICMS00 = null;
					/* 742 */ jICMS40 = null;
					/* 743 */ jICMSSN900 = null;
				} else
				{
					/* 746 */ jICMSSN900 = new JanelaICMSSN900("900");
					/* 747 */ jICMSSN900.setVisible(true);
					/* 748 */ jICMS00 = null;
					/* 749 */ jICMS40 = null;
					/* 750 */ jICMSSN102 = null;
				}
			}
		});
	}

	private JComboBox getComboImposto()
	{
		/* 763 */ if (comboICMS == null)
		{
			/* 765 */ comboICMS = new JComboBox();
			/* 766 */ comboICMS.setBounds(new Rectangle(13, 517, 258, 25));
			/* 767 */ comboICMS.addItem("Tributada integralmente");
			/* 768 */ comboICMS.addItem("Tributado com redu��o de base de c�lculo");
			/* 769 */ comboICMS.addItem("Isenta");
			/* 770 */ comboICMS.addItem("N�o tributada");
			/* 771 */ comboICMS.addItem("Suspens�o");
			/* 772 */ comboICMS.addItem("Cobrado anteriormente por substitui��o tribut�ria");
			/* 773 */ comboICMS.addItem("Outros");
			/* 774 */ comboICMS.addItem("Simples Nacional - Sem permiss�o de cr�dito");
			/* 775 */ comboICMS.addItem("Simples Nacional - Imune");
			/* 776 */ comboICMS
					.addItem("Simples Nacional - Cobrado anteriormente por substitui��o tribut�ria ou por antecipa��o");
			/* 777 */ comboICMS.addItem("Simples Nacional - Outros");
			/* 778 */ comboICMS.setSelectedIndex(-1);
		}

		/* 781 */ return comboICMS;
	}

	private JComboBox getComboPIS()
	{
		/* 791 */ if (comboPIS == null)
		{
			/* 793 */ comboPIS = new JComboBox();
			/* 794 */ comboPIS.setBounds(new Rectangle(13, 569, 258, 25));
			/* 795 */ comboPIS.addItem("Al�quota");
			/* 796 */ comboPIS.addItem("Quantidade");
			/* 797 */ comboPIS.addItem("N�o tributado");
			/* 798 */ comboPIS.addItem("Simples nacional");
			/* 799 */ comboPIS.setSelectedIndex(-1);
		}
		/* 801 */ return comboPIS;
	}

	private JComboBox getComboCOFINS()
	{
		/* 811 */ if (comboCOFINS == null)
		{
			/* 813 */ comboCOFINS = new JComboBox();
			/* 814 */ comboCOFINS.setBounds(new Rectangle(13, 621, 258, 25));
			/* 815 */ comboCOFINS.addItem("Al�quota");
			/* 816 */ comboCOFINS.addItem("Quantidade");
			/* 817 */ comboCOFINS.addItem("N�o tributado");
			/* 818 */ comboCOFINS.addItem("Simples nacional");
			/* 819 */ comboCOFINS.setSelectedIndex(-1);
			/* 820 */ comboCOFINS.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					/* 824 */ int index = ((JComboBox) e.getSource()).getSelectedIndex();
					/* 825 */ if (index == 0)
					{
						/* 827 */ jCOFINSAliq = new JanelaCOFINSAliq();
						/* 828 */ jCOFINSAliq.setVisible(true);
						/* 829 */ jCOFINSQtde = null;
						/* 830 */ jCOFINSNT = null;
						/* 831 */ } else if (index == 1)
					{
						/* 833 */ jCOFINSQtde = new JanelaCOFINSQtde();
						/* 834 */ jCOFINSQtde.setVisible(true);
						/* 835 */ jCOFINSAliq = null;
						/* 836 */ jCOFINSNT = null;
						/* 837 */ } else if (index == 2)
					{
						/* 839 */ jCOFINSNT = new JanelaCOFINSNT();
						/* 840 */ jCOFINSNT.setVisible(true);
						/* 841 */ jCOFINSAliq = null;
						/* 842 */ jCOFINSQtde = null;
					}
				}
			});
		}
		/* 847 */ return comboCOFINS;

	}

	public static void main(String[] args)
	{
		/*
		 * JanelaConfigurarProduto configurarProduto = new
		 * JanelaConfigurarProduto(null); configurarProduto.setVisible(true);
		 */
			JanelaISSQN issqn = new JanelaISSQN(null);
			issqn.setVisible(true);
			}

}
