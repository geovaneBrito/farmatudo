package br.com.farmatudo.views;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import javax.swing.JCheckBox;
import javax.swing.JPasswordField;

import br.com.farmatudo.bean.UsuarioBean;
import br.com.farmatudo.daos.UsuarioDao;
import br.com.farmatudo.util.AcessoBD;
import br.com.farmatudo.util.TelaUtil;

public class NovoUsuario extends JDialog implements ActionListener {
	private JTextField txt_Usuario;
	private JPasswordField txt_passWdField_Senha;
	private JButton btn_Salvar;
	private JLabel lbl_TipoDeUsuario;
	private JComboBox comboBox_Tipo;
	private JCheckBox chckbx_Senha;
	private JPasswordField txt_passField_Senha_Adm;
	private JLabel lbl_Senha_Adm;
	private JLabel lblTipo;
	private JComboBox combo_Tipo;

	public NovoUsuario() {

		TelaUtil.setaNimbus(this);
		setTitle("Novo Usu�rio");
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		setModal(true);
		setSize(500, 285);
		setResizable(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 46, 89, 103, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 24, 35, 35, 35, 35, 0, 35, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0,
				Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		JLabel lbl_Usuario = new JLabel("Novo Usu�rio");
		lbl_Usuario.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_Usuario = new GridBagConstraints();
		gbc_lbl_Usuario.anchor = GridBagConstraints.EAST;
		gbc_lbl_Usuario.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Usuario.gridx = 1;
		gbc_lbl_Usuario.gridy = 1;
		getContentPane().add(lbl_Usuario, gbc_lbl_Usuario);

		txt_Usuario = new JTextField();
		txt_Usuario.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_txt_Usuario = new GridBagConstraints();
		gbc_txt_Usuario.insets = new Insets(0, 0, 5, 5);
		gbc_txt_Usuario.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_Usuario.gridx = 2;
		gbc_txt_Usuario.gridy = 1;
		getContentPane().add(txt_Usuario, gbc_txt_Usuario);
		txt_Usuario.setColumns(10);

		btn_Salvar = new JButton("Salvar");
		btn_Salvar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btn_Salvar.addActionListener(this);

		JLabel lbl_Senha = new JLabel("Senha:");
		lbl_Senha.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_Senha = new GridBagConstraints();
		gbc_lbl_Senha.anchor = GridBagConstraints.EAST;
		gbc_lbl_Senha.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Senha.gridx = 1;
		gbc_lbl_Senha.gridy = 2;
		getContentPane().add(lbl_Senha, gbc_lbl_Senha);

		txt_passWdField_Senha = new JPasswordField();
		txt_passWdField_Senha.setEchoChar('*');
		txt_passWdField_Senha.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		txt_passWdField_Senha.setColumns(10);
		GridBagConstraints gbc_txt_passWdField_Senha = new GridBagConstraints();
		gbc_txt_passWdField_Senha.insets = new Insets(0, 0, 5, 5);
		gbc_txt_passWdField_Senha.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_passWdField_Senha.gridx = 2;
		gbc_txt_passWdField_Senha.gridy = 2;
		getContentPane().add(txt_passWdField_Senha, gbc_txt_passWdField_Senha);

		chckbx_Senha = new JCheckBox("Mostrar Senha");
		chckbx_Senha.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		chckbx_Senha.addActionListener(this);
		GridBagConstraints gbc_chckbx_Senha = new GridBagConstraints();
		gbc_chckbx_Senha.anchor = GridBagConstraints.WEST;
		gbc_chckbx_Senha.insets = new Insets(0, 0, 5, 0);
		gbc_chckbx_Senha.gridx = 3;
		gbc_chckbx_Senha.gridy = 2;
		getContentPane().add(chckbx_Senha, gbc_chckbx_Senha);

		lblTipo = new JLabel("Tipo:");
		lblTipo.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_lblTipo = new GridBagConstraints();
		gbc_lblTipo.anchor = GridBagConstraints.EAST;
		gbc_lblTipo.insets = new Insets(0, 0, 5, 5);
		gbc_lblTipo.gridx = 1;
		gbc_lblTipo.gridy = 3;
		getContentPane().add(lblTipo, gbc_lblTipo);

		combo_Tipo = new JComboBox();
		combo_Tipo.setModel(new DefaultComboBoxModel(new String[] {"Admin", "Vendedor"}));
		combo_Tipo.setMaximumRowCount(3);
		combo_Tipo.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_combo_Tipo = new GridBagConstraints();
		gbc_combo_Tipo.insets = new Insets(0, 0, 5, 5);
		gbc_combo_Tipo.fill = GridBagConstraints.HORIZONTAL;
		gbc_combo_Tipo.gridx = 2;
		gbc_combo_Tipo.gridy = 3;
		getContentPane().add(combo_Tipo, gbc_combo_Tipo);

		lbl_Senha_Adm = new JLabel("Senha Administrador");
		lbl_Senha_Adm.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_Senha_Adm = new GridBagConstraints();
		gbc_lbl_Senha_Adm.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Senha_Adm.anchor = GridBagConstraints.EAST;
		gbc_lbl_Senha_Adm.gridx = 1;
		gbc_lbl_Senha_Adm.gridy = 4;
		getContentPane().add(lbl_Senha_Adm, gbc_lbl_Senha_Adm);

		txt_passField_Senha_Adm = new JPasswordField();
		txt_passField_Senha_Adm.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_txt_passField_Senha_Adm = new GridBagConstraints();
		gbc_txt_passField_Senha_Adm.insets = new Insets(0, 0, 5, 5);
		gbc_txt_passField_Senha_Adm.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_passField_Senha_Adm.gridx = 2;
		gbc_txt_passField_Senha_Adm.gridy = 4;
		getContentPane().add(txt_passField_Senha_Adm,
				gbc_txt_passField_Senha_Adm);

		btn_Salvar.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_btn_Salvar = new GridBagConstraints();
		gbc_btn_Salvar.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_Salvar.anchor = GridBagConstraints.NORTH;
		gbc_btn_Salvar.insets = new Insets(0, 0, 0, 5);
		gbc_btn_Salvar.gridx = 2;
		gbc_btn_Salvar.gridy = 6;
		getContentPane().add(btn_Salvar, gbc_btn_Salvar);
		TelaUtil.AcaoTeclaButton(this, btn_Salvar, KeyEvent.VK_ENTER);

	}

	public void actionPerformed(ActionEvent event)
	{
		if (event.getSource() == btn_Salvar)
		{	boolean flagOk = false;
			if (txt_passField_Senha_Adm.getText().equals("") || !txt_passField_Senha_Adm.getText().equals("123"))
			{
				JOptionPane.showMessageDialog(null, "SENHA ADMINISTRADOR ERRADA" );
			}
			else
			{	
				UsuarioBean usuario = new UsuarioBean();
				usuario.setNome(txt_Usuario.getText().trim());
				usuario.setSenha(txt_passWdField_Senha.getText().trim());
				System.out.println((String)combo_Tipo.getSelectedItem());
				usuario.setTipo((String)combo_Tipo.getSelectedItem());
				
				
				try {
					Connection conn = null;
					AcessoBD bd = new AcessoBD();
					conn = bd.obtemConexao();
					flagOk = UsuarioDao.salvar(conn, usuario);
					
					if(flagOk){
						JOptionPane.showMessageDialog(null, "Usu�rio " + usuario.getNome() +  " Criado com sucesso!");
						dispose();
					}
					else{
						JOptionPane.showMessageDialog(null, "Erro na cria��o de Usu�rio!");
					}
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
		}
		
		if (event.getSource() == chckbx_Senha)
		{
			if (chckbx_Senha.isSelected())
			{
				System.out.println("Selecionado");
				txt_passWdField_Senha.setEchoChar('\u0000');
				
			}
			else
			{
				txt_passWdField_Senha.setEchoChar('*');
			}	
		}
		

	}
}
