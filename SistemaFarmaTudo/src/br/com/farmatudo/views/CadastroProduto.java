package br.com.farmatudo.views;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import br.com.farmatudo.bean.CategoriaBean;
import br.com.farmatudo.bean.LaboratorioBean;
import br.com.farmatudo.bean.ProdutoBean;
import br.com.farmatudo.daos.CategoriaDao;
import br.com.farmatudo.daos.LaboratorioDao;
import br.com.farmatudo.daos.ProdutoDao;
import br.com.farmatudo.util.AcessoBD;
import br.com.farmatudo.util.JMoneyField;
import br.com.farmatudo.util.JtextFieldSomenteNumeros;
import br.com.farmatudo.util.TelaUtil;


public class CadastroProduto extends JDialog implements ActionListener, MouseListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txt_Descricao;
	private JTextField txt_Quantidade;
	private JComboBox comboBox_Laboratorio;
	private JButton btn_Editar;
	private JButton btn_Salvar;
	private JButton btn_Volta;
	private boolean flagEditar = false;
	private String idProduto = "";
	private boolean flagSalvo;
	private ProdutoBean produto;
	private JMoneyField txt_Custo;
	private JMoneyField txt_Venda;
	private JTextField txt_Margem;
	private JButton btnAddCategoria;
	private JTable table_Categoria;
	private JScrollPane scrollPane;
	private JLabel label;
	private String nomeProduto = "";
	private JButton btn_Imprimir;
	private JtextFieldSomenteNumeros txt_CodNCM;
	private JLabel lbl_CodNCM;
	private JLabel lbl_CFOP;
	private JLabel lbl_Ucom;
	private JtextFieldSomenteNumeros txt_CFOP;
	private JtextFieldSomenteNumeros txt_Ucom;
	private JtextFieldSomenteNumeros txt_RegraCalculo;
	private JLabel lblRegraDe;
	private JPanel panelTributo;
	private JLabel lbl_ICMS;
	private JComboBox combox_ICMS;
	private JLabel lbl_PIS;
	private JComboBox comboBox_PIS;
	private JLabel lbl_PISST;
	private JComboBox comboBox_PISST;
	private JLabel lbl_COFINS;
	private JComboBox comboBox_COFINS;
	private JLabel lbl_COFINSST;
	private JComboBox comboBox_COFINSST;
	private JButton btn_ISSQN;
	private JLabel lbl_Issqn;
	private JLabel lbl_ObservaesFisco;
	private JButton btn_Fisco;

	public CadastroProduto(DefaultTableModel tableModel, int row)
	{
		setTitle("Cadastro de Produtos");
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		setSize(845, 706);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(true);
		TelaUtil.setaNimbus(this);

		GridBagLayout gridPrincipal = new GridBagLayout();
		gridPrincipal.columnWidths = new int[] { 55, 61, 61, 0, 34, 0 };
		gridPrincipal.rowHeights = new int[] { 52, 0, 36, 0 };
		gridPrincipal.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gridPrincipal.rowWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridPrincipal);

		btn_Salvar = new JButton();
		btn_Salvar.setIcon(new ImageIcon(CadastroProduto.class.getResource("/imagemIcon/guardar.png")));
		GridBagConstraints gbc_btn_Salvar = new GridBagConstraints();
		gbc_btn_Salvar.insets = new Insets(0, 0, 5, 5);
		gbc_btn_Salvar.gridx = 0;
		gbc_btn_Salvar.gridy = 0;
		getContentPane().add(btn_Salvar, gbc_btn_Salvar);
		btn_Salvar.setMnemonic('s');

		btn_Editar = new JButton();
		btn_Editar.setIcon(new ImageIcon(CadastroProduto.class.getResource("/imagemIcon/editar.png")));
		btn_Editar.setMnemonic('e');
		GridBagConstraints gbc_button_1 = new GridBagConstraints();
		gbc_button_1.anchor = GridBagConstraints.WEST;
		gbc_button_1.insets = new Insets(0, 0, 5, 5);
		gbc_button_1.gridx = 1;
		gbc_button_1.gridy = 0;
		getContentPane().add(btn_Editar, gbc_button_1);
		btn_Editar.addActionListener(this);

		btn_Volta = new JButton();
		btn_Volta.setMnemonic('v');
		btn_Volta.setIcon(new ImageIcon(CadastroProduto.class.getResource("/imagemIcon/voltar.png")));
		GridBagConstraints gbc_btn_Volta = new GridBagConstraints();
		gbc_btn_Volta.anchor = GridBagConstraints.WEST;
		gbc_btn_Volta.insets = new Insets(0, 0, 5, 5);
		gbc_btn_Volta.gridx = 2;
		gbc_btn_Volta.gridy = 0;
		getContentPane().add(btn_Volta, gbc_btn_Volta);
		btn_Volta.addActionListener(this);
		
		btn_Imprimir = new JButton();
		btn_Imprimir.setIcon(new ImageIcon(CadastroProduto.class.getResource("/imagemIcon/imprimir.png")));
		btn_Imprimir.setMnemonic('v');
		GridBagConstraints gbc_btn_Imprimir = new GridBagConstraints();
		gbc_btn_Imprimir.anchor = GridBagConstraints.EAST;
		gbc_btn_Imprimir.insets = new Insets(0, 0, 5, 5);
		gbc_btn_Imprimir.gridx = 3;
		gbc_btn_Imprimir.gridy = 0;
		getContentPane().add(btn_Imprimir, gbc_btn_Imprimir);
		btn_Imprimir.addActionListener(this);

		JPanel panelDescricao = new JPanel();
		panelDescricao.setBorder(BorderFactory.createTitledBorder("<html><h4>Dados do Produtos</h4><html>"));
		GridBagConstraints gbc_panelDescricao = new GridBagConstraints();
		gbc_panelDescricao.gridheight = 2;
		gbc_panelDescricao.gridwidth = 3;
		gbc_panelDescricao.insets = new Insets(0, 0, 5, 5);
		gbc_panelDescricao.fill = GridBagConstraints.BOTH;
		gbc_panelDescricao.gridx = 1;
		gbc_panelDescricao.gridy = 1;
		getContentPane().add(panelDescricao, gbc_panelDescricao);
		GridBagLayout gbl_panelDescricao = new GridBagLayout();
		gbl_panelDescricao.columnWidths = new int[] { 95, 200, 138, 200, 56, 0 };
		gbl_panelDescricao.rowHeights = new int[] { 38, 29, 44, 44, 44, 44, 44, 44, 44, 99 };
		gbl_panelDescricao.columnWeights = new double[] { 1.0, 1.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelDescricao.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		panelDescricao.setLayout(gbl_panelDescricao);

		JLabel lblDescricao = new JLabel("Descricao: ");
		lblDescricao.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblDescricao = new GridBagConstraints();
		gbc_lblDescricao.anchor = GridBagConstraints.EAST;
		gbc_lblDescricao.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescricao.gridx = 0;
		gbc_lblDescricao.gridy = 0;
		panelDescricao.add(lblDescricao, gbc_lblDescricao);

		txt_Descricao = new JTextField();
		// txt_Descricao.setText("ddasa");
		txt_Descricao.setFont(new Font("Tahoma", Font.PLAIN, 14));

		txt_Descricao.setToolTipText("Descri��o do produto");
		GridBagConstraints gbc_txt_descricao = new GridBagConstraints();
		gbc_txt_descricao.gridwidth = 3;
		gbc_txt_descricao.insets = new Insets(0, 0, 5, 5);
		gbc_txt_descricao.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_descricao.gridx = 1;
		gbc_txt_descricao.gridy = 0;
		panelDescricao.add(txt_Descricao, gbc_txt_descricao);
		txt_Descricao.setColumns(10);

		JLabel lblQuantidade = new JLabel("Quantidade: ");
		lblQuantidade.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblQuantidade = new GridBagConstraints();
		gbc_lblQuantidade.anchor = GridBagConstraints.EAST;
		gbc_lblQuantidade.insets = new Insets(0, 0, 5, 5);
		gbc_lblQuantidade.gridx = 0;
		gbc_lblQuantidade.gridy = 1;
		panelDescricao.add(lblQuantidade, gbc_lblQuantidade);

		txt_Quantidade = new JtextFieldSomenteNumeros();
		// txt_Quantidade.setText("20");
		txt_Quantidade.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txt_Quantidade.setToolTipText("Quantidade de produto");
		txt_Quantidade.setColumns(10);
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 1;
		panelDescricao.add(txt_Quantidade, gbc_textField_1);
		ImageIcon icon = new ImageIcon("/imagemIcon/Add.png");

		JLabel lblValorDeVenda = new JLabel("Valor de Venda: ");
		lblValorDeVenda.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblValorDeVenda = new GridBagConstraints();
		gbc_lblValorDeVenda.anchor = GridBagConstraints.EAST;
		gbc_lblValorDeVenda.insets = new Insets(0, 0, 5, 5);
		gbc_lblValorDeVenda.gridx = 2;
		gbc_lblValorDeVenda.gridy = 1;
		panelDescricao.add(lblValorDeVenda, gbc_lblValorDeVenda);

		txt_Venda = new JMoneyField();
		// txt_Venda.setText("50,40");
		txt_Venda.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_txt_Venda = new GridBagConstraints();
		gbc_txt_Venda.insets = new Insets(0, 0, 5, 5);
		gbc_txt_Venda.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_Venda.gridx = 3;
		gbc_txt_Venda.gridy = 1;
		panelDescricao.add(txt_Venda, gbc_txt_Venda);

		JLabel lblValorDe = new JLabel("Valor de Custo: ");
		lblValorDe.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblValorDe = new GridBagConstraints();
		gbc_lblValorDe.anchor = GridBagConstraints.EAST;
		gbc_lblValorDe.insets = new Insets(0, 0, 5, 5);
		gbc_lblValorDe.gridx = 0;
		gbc_lblValorDe.gridy = 2;
		panelDescricao.add(lblValorDe, gbc_lblValorDe);

		txt_Custo = new JMoneyField();
		// txt_Custo.setText("55,40");
		txt_Custo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_txt_Custo = new GridBagConstraints();
		gbc_txt_Custo.insets = new Insets(0, 0, 5, 5);
		gbc_txt_Custo.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_Custo.gridx = 1;
		gbc_txt_Custo.gridy = 2;
		panelDescricao.add(txt_Custo, gbc_txt_Custo);
						
								JLabel lblMargem = new JLabel("Margem: ");
								lblMargem.setFont(new Font("Tahoma", Font.PLAIN, 14));
								GridBagConstraints gbc_lblMargem = new GridBagConstraints();
								gbc_lblMargem.anchor = GridBagConstraints.EAST;
								gbc_lblMargem.insets = new Insets(0, 0, 5, 5);
								gbc_lblMargem.gridx = 2;
								gbc_lblMargem.gridy = 2;
								panelDescricao.add(lblMargem, gbc_lblMargem);
				
						txt_Margem = new JtextFieldSomenteNumeros();
						// txt_Margem.setText("5");
						txt_Margem.setToolTipText("Quantidade de produto");
						txt_Margem.setFont(new Font("Tahoma", Font.PLAIN, 14));
						txt_Margem.setColumns(10);
						GridBagConstraints gbc_txt_Margem_1 = new GridBagConstraints();
						gbc_txt_Margem_1.insets = new Insets(0, 0, 5, 5);
						gbc_txt_Margem_1.fill = GridBagConstraints.HORIZONTAL;
						gbc_txt_Margem_1.gridx = 3;
						gbc_txt_Margem_1.gridy = 2;
						panelDescricao.add(txt_Margem, gbc_txt_Margem_1);
						
						lbl_CFOP = new JLabel("CFOP");
						lbl_CFOP.setFont(new Font("Tahoma", Font.PLAIN, 14));
						GridBagConstraints gbc_lbl_CFOP = new GridBagConstraints();
						gbc_lbl_CFOP.anchor = GridBagConstraints.EAST;
						gbc_lbl_CFOP.insets = new Insets(0, 0, 5, 5);
						gbc_lbl_CFOP.gridx = 0;
						gbc_lbl_CFOP.gridy = 3;
						panelDescricao.add(lbl_CFOP, gbc_lbl_CFOP);
						
						txt_CFOP = new JtextFieldSomenteNumeros();
						txt_CFOP.setToolTipText("C\u00F3digo Fiscal de Opera\u00E7\u00F5es e Presta\u00E7\u00F5es");
						txt_CFOP.setFont(new Font("Tahoma", Font.PLAIN, 14));
						txt_CFOP.setColumns(10);
						GridBagConstraints gbc_txt_CFOP = new GridBagConstraints();
						gbc_txt_CFOP.insets = new Insets(0, 0, 5, 5);
						gbc_txt_CFOP.fill = GridBagConstraints.HORIZONTAL;
						gbc_txt_CFOP.gridx = 1;
						gbc_txt_CFOP.gridy = 3;
						panelDescricao.add(txt_CFOP, gbc_txt_CFOP);
				
						JLabel lblLaboratrio = new JLabel("Laborat\u00F3rio: ");
						lblLaboratrio.setFont(new Font("Tahoma", Font.PLAIN, 14));
						GridBagConstraints gbc_lblLaboratrio = new GridBagConstraints();
						gbc_lblLaboratrio.anchor = GridBagConstraints.EAST;
						gbc_lblLaboratrio.insets = new Insets(0, 0, 5, 5);
						gbc_lblLaboratrio.gridx = 2;
						gbc_lblLaboratrio.gridy = 3;
						panelDescricao.add(lblLaboratrio, gbc_lblLaboratrio);
		
				comboBox_Laboratorio = new JComboBox();
				comboBox_Laboratorio.setToolTipText("Laborat\u00F3rio");
				comboBox_Laboratorio.setFont(new Font("Tahoma", Font.PLAIN, 14));
				GridBagConstraints gbc_comboBox_Laboratorio = new GridBagConstraints();
				gbc_comboBox_Laboratorio.insets = new Insets(0, 0, 5, 5);
				gbc_comboBox_Laboratorio.fill = GridBagConstraints.HORIZONTAL;
				gbc_comboBox_Laboratorio.gridx = 3;
				gbc_comboBox_Laboratorio.gridy = 3;
				panelDescricao.add(comboBox_Laboratorio, gbc_comboBox_Laboratorio);
		
		panelTributo = new JPanel();
		panelTributo.setBorder(BorderFactory.createTitledBorder("<html><h4>Tributa��es</h4><html>"));
		GridBagConstraints gbc_panelTributo = new GridBagConstraints();
		gbc_panelTributo.gridheight = 6;
		gbc_panelTributo.gridwidth = 2;
		gbc_panelTributo.insets = new Insets(0, 0, 0, 5);
		gbc_panelTributo.fill = GridBagConstraints.BOTH;
		gbc_panelTributo.gridx = 2;
		gbc_panelTributo.gridy = 4;
		panelDescricao.add(panelTributo, gbc_panelTributo);
		GridBagLayout gbl_panelTributo = new GridBagLayout();
		gbl_panelTributo.columnWidths = new int[]{79, 0, 0};
		gbl_panelTributo.rowHeights = new int[]{44, 44, 44, 44, 44, 44, 44, 0};
		gbl_panelTributo.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_panelTributo.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelTributo.setLayout(gbl_panelTributo);
		
		lbl_ICMS = new JLabel("ICMS: ");
		lbl_ICMS.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_ICMS = new GridBagConstraints();
		gbc_lbl_ICMS.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_ICMS.anchor = GridBagConstraints.EAST;
		gbc_lbl_ICMS.gridx = 0;
		gbc_lbl_ICMS.gridy = 0;
		panelTributo.add(lbl_ICMS, gbc_lbl_ICMS);
		
		combox_ICMS = new JComboBox();
		combox_ICMS.setToolTipText("Laborat\u00F3rio");
		combox_ICMS.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_combox_ICMS = new GridBagConstraints();
		gbc_combox_ICMS.insets = new Insets(0, 0, 5, 0);
		gbc_combox_ICMS.fill = GridBagConstraints.HORIZONTAL;
		gbc_combox_ICMS.gridx = 1;
		gbc_combox_ICMS.gridy = 0;
		panelTributo.add(combox_ICMS, gbc_combox_ICMS);
		
		lbl_PIS = new JLabel("PIS: ");
		lbl_PIS.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_PIS = new GridBagConstraints();
		gbc_lbl_PIS.anchor = GridBagConstraints.EAST;
		gbc_lbl_PIS.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_PIS.gridx = 0;
		gbc_lbl_PIS.gridy = 1;
		panelTributo.add(lbl_PIS, gbc_lbl_PIS);
		
		comboBox_PIS = new JComboBox();
		comboBox_PIS.setToolTipText("Laborat\u00F3rio");
		comboBox_PIS.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_comboBox_PIS = new GridBagConstraints();
		gbc_comboBox_PIS.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox_PIS.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_PIS.gridx = 1;
		gbc_comboBox_PIS.gridy = 1;
		panelTributo.add(comboBox_PIS, gbc_comboBox_PIS);
		
		lbl_PISST = new JLabel("PISST: ");
		lbl_PISST.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_PISST = new GridBagConstraints();
		gbc_lbl_PISST.anchor = GridBagConstraints.EAST;
		gbc_lbl_PISST.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_PISST.gridx = 0;
		gbc_lbl_PISST.gridy = 2;
		panelTributo.add(lbl_PISST, gbc_lbl_PISST);
		
		comboBox_PISST = new JComboBox();
		comboBox_PISST.setToolTipText("Laborat\u00F3rio");
		comboBox_PISST.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_comboBox_PISST = new GridBagConstraints();
		gbc_comboBox_PISST.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox_PISST.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_PISST.gridx = 1;
		gbc_comboBox_PISST.gridy = 2;
		panelTributo.add(comboBox_PISST, gbc_comboBox_PISST);
		
		lbl_COFINS = new JLabel("COFINS: ");
		lbl_COFINS.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_COFINS = new GridBagConstraints();
		gbc_lbl_COFINS.anchor = GridBagConstraints.EAST;
		gbc_lbl_COFINS.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_COFINS.gridx = 0;
		gbc_lbl_COFINS.gridy = 3;
		panelTributo.add(lbl_COFINS, gbc_lbl_COFINS);
		
		comboBox_COFINS = new JComboBox();
		comboBox_COFINS.setToolTipText("Laborat\u00F3rio");
		comboBox_COFINS.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_comboBox_COFINS = new GridBagConstraints();
		gbc_comboBox_COFINS.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox_COFINS.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_COFINS.gridx = 1;
		gbc_comboBox_COFINS.gridy = 3;
		panelTributo.add(comboBox_COFINS, gbc_comboBox_COFINS);
		
		lbl_COFINSST = new JLabel("COFINSST: ");
		lbl_COFINSST.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_COFINSST = new GridBagConstraints();
		gbc_lbl_COFINSST.anchor = GridBagConstraints.EAST;
		gbc_lbl_COFINSST.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_COFINSST.gridx = 0;
		gbc_lbl_COFINSST.gridy = 4;
		panelTributo.add(lbl_COFINSST, gbc_lbl_COFINSST);
		
		comboBox_COFINSST = new JComboBox();
		comboBox_COFINSST.setToolTipText("Laborat\u00F3rio");
		comboBox_COFINSST.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_comboBox_COFINSST = new GridBagConstraints();
		gbc_comboBox_COFINSST.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox_COFINSST.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_COFINSST.gridx = 1;
		gbc_comboBox_COFINSST.gridy = 4;
		panelTributo.add(comboBox_COFINSST, gbc_comboBox_COFINSST);
		
		lbl_Issqn = new JLabel("ISSQN: ");
		lbl_Issqn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_Issqn = new GridBagConstraints();
		gbc_lbl_Issqn.anchor = GridBagConstraints.EAST;
		gbc_lbl_Issqn.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Issqn.gridx = 0;
		gbc_lbl_Issqn.gridy = 5;
		panelTributo.add(lbl_Issqn, gbc_lbl_Issqn);
		
		btn_ISSQN = new JButton();
		btn_ISSQN.setIcon(new ImageIcon(CadastroProduto.class.getResource("/imagemIcon/add_min.png")));
		btn_ISSQN.setMnemonic('v');
		GridBagConstraints gbc_btn_ISSQN = new GridBagConstraints();
		gbc_btn_ISSQN.anchor = GridBagConstraints.WEST;
		gbc_btn_ISSQN.insets = new Insets(0, 0, 5, 0);
		gbc_btn_ISSQN.gridx = 1;
		gbc_btn_ISSQN.gridy = 5;
		panelTributo.add(btn_ISSQN, gbc_btn_ISSQN);
		
		lbl_ObservaesFisco = new JLabel("Observa\u00E7\u00F5es Fisco: ");
		lbl_ObservaesFisco.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_ObservaesFisco = new GridBagConstraints();
		gbc_lbl_ObservaesFisco.anchor = GridBagConstraints.EAST;
		gbc_lbl_ObservaesFisco.insets = new Insets(0, 0, 0, 5);
		gbc_lbl_ObservaesFisco.gridx = 0;
		gbc_lbl_ObservaesFisco.gridy = 6;
		panelTributo.add(lbl_ObservaesFisco, gbc_lbl_ObservaesFisco);
		
		btn_Fisco = new JButton();
		btn_Fisco.setIcon(new ImageIcon(CadastroProduto.class.getResource("/imagemIcon/add_min.png")));
		btn_Fisco.setMnemonic('v');
		GridBagConstraints gbc_btn_Fisco = new GridBagConstraints();
		gbc_btn_Fisco.anchor = GridBagConstraints.WEST;
		gbc_btn_Fisco.gridx = 1;
		gbc_btn_Fisco.gridy = 6;
		panelTributo.add(btn_Fisco, gbc_btn_Fisco);
		
		lbl_CodNCM = new JLabel("C\u00F3digo NCM");
		lbl_CodNCM.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_CodNCM = new GridBagConstraints();
		gbc_lbl_CodNCM.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_CodNCM.anchor = GridBagConstraints.EAST;
		gbc_lbl_CodNCM.gridx = 0;
		gbc_lbl_CodNCM.gridy = 4;
		panelDescricao.add(lbl_CodNCM, gbc_lbl_CodNCM);
		
		txt_CodNCM = new JtextFieldSomenteNumeros();
		txt_CodNCM.setToolTipText("Informe o C\u00F3digo fiscal do produto");
		txt_CodNCM.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txt_CodNCM.setColumns(10);
		GridBagConstraints gbc_txt_CodNCM = new GridBagConstraints();
		gbc_txt_CodNCM.insets = new Insets(0, 0, 5, 5);
		gbc_txt_CodNCM.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_CodNCM.gridx = 1;
		gbc_txt_CodNCM.gridy = 4;
		panelDescricao.add(txt_CodNCM, gbc_txt_CodNCM);
		
		lblRegraDe = new JLabel("Regra de C\u00E1lculo");
		lblRegraDe.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblRegraDe = new GridBagConstraints();
		gbc_lblRegraDe.insets = new Insets(0, 0, 5, 5);
		gbc_lblRegraDe.anchor = GridBagConstraints.EAST;
		gbc_lblRegraDe.gridx = 0;
		gbc_lblRegraDe.gridy = 5;
		panelDescricao.add(lblRegraDe, gbc_lblRegraDe);
		
		txt_RegraCalculo = new JtextFieldSomenteNumeros();
		txt_RegraCalculo.setToolTipText("(A ou T)");
		txt_RegraCalculo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txt_RegraCalculo.setColumns(10);
		GridBagConstraints gbc_txt_RegraCalculo = new GridBagConstraints();
		gbc_txt_RegraCalculo.insets = new Insets(0, 0, 5, 5);
		gbc_txt_RegraCalculo.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_RegraCalculo.gridx = 1;
		gbc_txt_RegraCalculo.gridy = 5;
		panelDescricao.add(txt_RegraCalculo, gbc_txt_RegraCalculo);
		
		lbl_Ucom = new JLabel("Unidade Comercial");
		lbl_Ucom.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_Ucom = new GridBagConstraints();
		gbc_lbl_Ucom.anchor = GridBagConstraints.EAST;
		gbc_lbl_Ucom.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Ucom.gridx = 0;
		gbc_lbl_Ucom.gridy = 6;
		panelDescricao.add(lbl_Ucom, gbc_lbl_Ucom);
								
								txt_Ucom = new JtextFieldSomenteNumeros();
								txt_Ucom.setToolTipText("Digite a unidade comercial. Ex: Kg");
								txt_Ucom.setFont(new Font("Tahoma", Font.PLAIN, 14));
								txt_Ucom.setColumns(10);
								GridBagConstraints gbc_txt_Ucom = new GridBagConstraints();
								gbc_txt_Ucom.insets = new Insets(0, 0, 5, 5);
								gbc_txt_Ucom.fill = GridBagConstraints.HORIZONTAL;
								gbc_txt_Ucom.gridx = 1;
								gbc_txt_Ucom.gridy = 6;
								panelDescricao.add(txt_Ucom, gbc_txt_Ucom);
										
												label = new JLabel("Categoria: ");
												label.setFont(new Font("Tahoma", Font.PLAIN, 14));
												GridBagConstraints gbc_label = new GridBagConstraints();
												gbc_label.anchor = GridBagConstraints.EAST;
												gbc_label.insets = new Insets(0, 0, 5, 5);
												gbc_label.gridx = 0;
												gbc_label.gridy = 7;
												panelDescricao.add(label, gbc_label);
								
										scrollPane = new JScrollPane();
										GridBagConstraints gbc_scrollPane = new GridBagConstraints();
										gbc_scrollPane.gridheight = 3;
										gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
										gbc_scrollPane.fill = GridBagConstraints.BOTH;
										gbc_scrollPane.gridx = 1;
										gbc_scrollPane.gridy = 7;
										panelDescricao.add(scrollPane, gbc_scrollPane);
										
												table_Categoria = new JTable();
												table_Categoria.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
												table_Categoria.setAutoCreateRowSorter(true);
												table_Categoria.setFont(new Font("Segoe UI", Font.PLAIN, 14));
												table_Categoria.setModel(new DefaultTableModel(new Object[][] { { null }, }, new String[] { "Categoria" })
												{
													boolean[] columnEditables = new boolean[] { false, false, false, false, false, false, false };

													public boolean isCellEditable(int row, int column)
													{
														return columnEditables[column];
													}
												});
												scrollPane.setViewportView(table_Categoria);
												table_Categoria.addMouseListener(this);
														
																btnAddCategoria = new JButton();
																btnAddCategoria.setToolTipText("Adcionar Categoria");
																btnAddCategoria.setIcon(new ImageIcon(CadastroProduto.class.getResource("/imagemIcon/add_min.png")));
																btnAddCategoria.setMnemonic('c');
																GridBagConstraints gbc_btnAddCategoria = new GridBagConstraints();
																gbc_btnAddCategoria.insets = new Insets(0, 0, 0, 5);
																gbc_btnAddCategoria.anchor = GridBagConstraints.EAST;
																gbc_btnAddCategoria.gridx = 0;
																gbc_btnAddCategoria.gridy = 9;
																panelDescricao.add(btnAddCategoria, gbc_btnAddCategoria);
										btnAddCategoria.addActionListener(this);

		btn_Editar.setEnabled(false);
		btn_Salvar.addActionListener(this);
		carregarLaboratorio();
		carregaCategoriaTable();
		carregaCategoriaTable();

		if (tableModel != null)
		{
			// "C�digo de Produto, Descri��o,Valor de Custo, Margem, Valor de
			// Venda, Quantidades, Categoria"
			flagEditar = true;
			// comboBox_Laboratorio.setModel(
			// new DefaultComboBoxModel(new String[] { separaInteiro((String)
			// tableModel.getValueAt(row, 0)) }));
			comboBox_Laboratorio.setEditable(false);

			txt_Descricao.setText((String) tableModel.getValueAt(row, 1));
			txt_Descricao.setEditable(false);

			txt_Custo.setText(String.valueOf(tableModel.getValueAt(row, 2)));
			txt_Custo.setEditable(false);

			txt_Margem.setText(String.valueOf(tableModel.getValueAt(row, 3)));
			txt_Margem.setEditable(false);

			txt_Venda.setText(String.valueOf(tableModel.getValueAt(row, 4)));
			txt_Venda.setEditable(false);

			txt_Quantidade.setText(String.valueOf(tableModel.getValueAt(row, 5)));
			txt_Quantidade.setEditable(false);

			DefaultTableModel tableModelCategoria = (DefaultTableModel) this.table_Categoria.getModel();

			int linha = 0;

			while (linha < table_Categoria.getRowCount())
			{
				if (String.valueOf(tableModelCategoria.getValueAt(linha, 0))
						.equals(String.valueOf(tableModel.getValueAt(row, 6))))
				{
					table_Categoria.setRowSelectionInterval(linha, linha);
				}
				linha++;
			}
			table_Categoria.setEnabled(false);
			btn_Editar.setEnabled(true);

		}

	}// fim do construtor

	/**
	 * Metodo que recebe por parametro uma String concatenada com numeros,
	 * remove os numeros e devolve a String restante*
	 * 
	 * @param p_String
	 *            String concatenada com numeros
	 **/
	private String separaInteiro(String p_String)
	{
		char[] cs = p_String.toCharArray();
		p_String = "";
		for (int i = 0; i < cs.length; i++)
		{
			// verifico se o char n�o � um numero
			if (!Character.isDigit(cs[i]))
			{
				p_String += cs[i];
			} else
			{
				idProduto += cs[i];
			}
		}
		return p_String;
	}

	/** Metodo que carrega todas os laboratorios e seta em um combobox **/
	private void carregarLaboratorio()
	{
		Connection connection = null;
		ArrayList<LaboratorioBean> todaLista = new ArrayList<LaboratorioBean>();

		try
		{
			AcessoBD bd = new AcessoBD();
			connection = bd.obtemConexao();
			todaLista = LaboratorioDao.carregarNomeLaboratorio(connection);
			for (LaboratorioBean laboratorioBean : todaLista)
			{

				comboBox_Laboratorio.addItem(laboratorioBean.getNome());
			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	/** Metodo que carrega todas as categorias e seta em um JTable **/
	private void carregaCategoriaTable()
	{
		Connection connection = null;
		ArrayList<CategoriaBean> todaLista = new ArrayList<CategoriaBean>();

		try
		{
			AcessoBD bd = new AcessoBD();
			connection = bd.obtemConexao();
			todaLista = CategoriaDao.carregarTodasCategoria(connection);

			DefaultTableModel dtm = (DefaultTableModel) table_Categoria.getModel();

			while (dtm.getRowCount() > 0)
			{
				dtm.removeRow(0);

			}
			for (CategoriaBean categoriaBean : todaLista)
			{
				adcionaLinhas(dtm, categoriaBean);

			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void adcionaLinhas(DefaultTableModel dtm, CategoriaBean categoriaBean)
	{
		dtm.addRow(new Object[] { categoriaBean.getNome() });

	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		if (event.getSource() == btn_Editar)
		{

			editarProduto();
		}
		if (event.getSource() == btn_Salvar)
		{
			salvarProduto();
		}
		if (event.getSource() == btn_Volta)
		{
			dispose();
		}
		if (event.getSource() == btnAddCategoria)
		{

			adicionaCategoria();
		}
		if (event.getSource() == btn_Imprimir)
		{

			/**Impressao impressao = new Impressao();
			impressao.detectaImpressoras();
			impressao.imprime("Teste");**/
		}

	}

	/** Metodo que adiciona categoria **/
	private void adicionaCategoria()
	{
		DefaultTableModel tableModel = (DefaultTableModel) this.table_Categoria.getModel();
		Connection conn = null;
		AcessoBD acessoBD = new AcessoBD();
		String nomeCategoria = "";
		nomeCategoria = JOptionPane.showInputDialog("Digite o nome da nova categoria ");
		int linha = 0;
		boolean flagEncontrado = false;
		while (linha < table_Categoria.getRowCount())
		{
			if (String.valueOf(tableModel.getValueAt(linha, 0)).equals(nomeCategoria))
			{
				flagEncontrado = true;
			}
			linha++;
		}

		if (!flagEncontrado)
		{

			try
			{

				conn = acessoBD.obtemConexao();
				boolean result = CategoriaDao.criarCategoria(conn, nomeCategoria);
				if (result)
				{
					carregaCategoriaTable();

				} else
				{
					JOptionPane.showMessageDialog(null, "Categoria n�o criada", "Importante",
							JOptionPane.CANCEL_OPTION);
				}
			} catch (Exception e)
			{
				JOptionPane.showMessageDialog(null, "Categoria n�o criada", "Importante", JOptionPane.CANCEL_OPTION);
				e.printStackTrace();
			}
		} else
		{
			JOptionPane.showMessageDialog(null, "Nome Duplicado", "Importante", JOptionPane.CANCEL_OPTION);
		}
	}

	/** Metodo que salva produtos **/
	private void salvarProduto()
	{
		String custo = "";
		String venda = "";
		if (txt_Descricao.getText().equals("") || txt_Venda.getText().equals("") || txt_Custo.getText().equals("")
				|| txt_Quantidade.getText().equals("") || txt_Margem.getText().equals("")
				|| table_Categoria.getSelectedRow() <= 0 || comboBox_Laboratorio.getSelectedIndex() <= 0)
		{
			JOptionPane.showMessageDialog(null, "Favor preencher todos os campos.", "Erro", JOptionPane.ERROR_MESSAGE);
			return;
		}

		int row = table_Categoria.getSelectedRow();
		DefaultTableModel tableModel = (DefaultTableModel) this.table_Categoria.getModel();

		custo = txt_Custo.getText().replace(".", "");
		venda = txt_Venda.getText().replace(".", "");

		produto = new ProdutoBean();
		produto.setCategoria(String.valueOf(tableModel.getValueAt(row, 0)));
		produto.setDescricao(txt_Descricao.getText());
		produto.setLaboratorio(comboBox_Laboratorio.getSelectedIndex());
		produto.setMargem(Integer.parseInt(txt_Margem.getText()));
		produto.setQuantidade(Integer.parseInt(txt_Quantidade.getText()));
		produto.setValor_custo(Double.parseDouble(custo.replace(",", ".")));
		produto.setValor_venda(Double.parseDouble(venda.replace(",", ".")));

		if (flagEditar)
		{
			System.out.println("Editar");
			Connection conn = null;
			try
			{
				AcessoBD bd = new AcessoBD();
				conn = bd.obtemConexao();
				ProdutoBean bean = ProdutoDao.buscaPorNome(conn, nomeProduto);
				System.out.println("Beean encontrado " + bean.getDescricao() + " " + bean.getId());
				produto.setId(bean.getId());
				produto.setCategoria(String.valueOf(tableModel.getValueAt(row, 0)));
				produto.setDescricao(txt_Descricao.getText());
				produto.setLaboratorio(comboBox_Laboratorio.getSelectedIndex());
				produto.setMargem(Integer.parseInt(txt_Margem.getText()));
				produto.setQuantidade(Integer.parseInt(txt_Quantidade.getText()));
				produto.setValor_custo(Double.parseDouble(custo.replace(",", ".")));
				produto.setValor_venda(Double.parseDouble(venda.replace(",", ".")));

				flagSalvo = ProdutoDao.updateProduto(conn, produto);
				if (flagSalvo)
				{
					flagSalvo = true;
					JOptionPane.showMessageDialog(null, "Produto " + txt_Descricao.getText() + " atualizado.",
							"Atualiza��o com sucesso", JOptionPane.PLAIN_MESSAGE);
					dispose();
				} else
				{

					JOptionPane.showMessageDialog(null, "Erro para atualizar " + txt_Descricao.getText() + ".", "Erro",
							JOptionPane.ERROR_MESSAGE);
				}
			} catch (Exception e)
			{
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Erro para atualizar " + txt_Descricao.getText() + ".", "Erro",
						JOptionPane.ERROR_MESSAGE);
			}
		} else
		{
			System.out.println("Novo produto/Cria��o");
			Connection conn = null;
			try
			{
				AcessoBD bd = new AcessoBD();
				conn = bd.obtemConexao();
				flagSalvo = ProdutoDao.salvar(conn, produto);
				if (flagSalvo)
				{
					JOptionPane.showMessageDialog(null, "Produto " + txt_Descricao.getText() + " criado.",
							"Atualiza��o com sucesso", JOptionPane.PLAIN_MESSAGE);
					dispose();
				} else
				{

					JOptionPane.showMessageDialog(null, "Erro para criar " + txt_Descricao.getText() + ".", "Erro",
							JOptionPane.ERROR_MESSAGE);
				}
			} catch (Exception e)
			{
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Erro para criar " + txt_Descricao.getText() + ".", "Erro",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		dispose();

	}

	/** Metodo que habilita todos os campos para edi��o **/
	private void editarProduto()
	{
		nomeProduto = txt_Descricao.getText();
		comboBox_Laboratorio.setEditable(true);

		table_Categoria.setEnabled(true);

		txt_Descricao.setEditable(true);

		txt_Custo.setEditable(true);

		txt_Margem.setEditable(true);

		txt_Venda.setEditable(true);

		txt_Quantidade.setEditable(true);

		btn_Editar.setEnabled(false);
		carregarLaboratorio();
	}

	public boolean cadastroProduto()
	{
		flagSalvo = false; // Marcamos que o ok n�o foi selecionado
		setModal(true); // A dialog tem que ser modal. S� pode retornar do
						// setVisible ap�s ficar invis�vel.
		setVisible(true); // Mostramos a dialog e esperamos o usu�rio escolher
							// alguma coisa.
		return flagSalvo; // Retornamos true, se ele pressionou ok.
	}

	public ProdutoBean retornaProduto()
	{
		setModal(true); // S� pode retornar do setVisible ap�s ficar invis�vel.
		setVisible(true); // Mostramos a dialog e esperamos o usu�rio escolher
		return produto;
	}

	@Override
	public void mouseClicked(MouseEvent event)
	{
		if (event.getClickCount() == 2)
		{
			int row = table_Categoria.getSelectedRow();
			DefaultTableModel tableModel = (DefaultTableModel) this.table_Categoria.getModel();
			Connection conn = null;
			AcessoBD acessoBD = new AcessoBD();
			String nomeCategoria = "";
			nomeCategoria = JOptionPane.showInputDialog("Atualize a categoria :  " + tableModel.getValueAt(row, 0),
					tableModel.getValueAt(row, 0));
			int linha = 0;
			boolean flagEncontrado = false;
			while (linha < table_Categoria.getRowCount())
			{
				if (String.valueOf(tableModel.getValueAt(linha, 0)).equals(nomeCategoria))
				{
					flagEncontrado = true;
				}
				linha++;
			}

			if (!flagEncontrado)
			{

				try
				{

					conn = acessoBD.obtemConexao();
					boolean result = CategoriaDao.atualizarCategoria(conn, nomeCategoria,
							String.valueOf(tableModel.getValueAt(row, 0)));
					if (result)
					{
						carregaCategoriaTable();

					} else
					{
						JOptionPane.showMessageDialog(null, "Categoria n�o atualizada", "Importante",
								JOptionPane.CANCEL_OPTION);
					}
				} catch (Exception e)
				{
					JOptionPane.showMessageDialog(null, "Categoria n�o atualizada", "Importante",
							JOptionPane.CANCEL_OPTION);
					e.printStackTrace();
				}
			} else
			{
				JOptionPane.showMessageDialog(null, "Nome Duplicado", "Importante", JOptionPane.CANCEL_OPTION);
			}
		}

	}

	@Override
	public void mouseEntered(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0)
	{
		// TODO Auto-generated method stub

	}

}
