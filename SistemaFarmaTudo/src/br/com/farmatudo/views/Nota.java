package br.com.farmatudo.views;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import br.com.farmatudo.bean.NotaBean;
import br.com.farmatudo.bean.ProdutoBean;
import br.com.farmatudo.daos.NotaDao;
import br.com.farmatudo.daos.ProdutoDao;
import br.com.farmatudo.daos.ProdutoNotaDao;
import br.com.farmatudo.util.AcessoBD;
import br.com.farmatudo.util.JMoneyField;
import br.com.farmatudo.util.JtextFieldSomenteNumeros;
import br.com.farmatudo.util.TelaUtil;
import datechooser.beans.DateChooserCombo;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import javax.swing.JComboBox;

public class Nota extends JDialog implements ActionListener, MouseListener, KeyListener, FocusListener
{
	private JTextField txtNumeroDaNota;
	private JTable table_Produto;
	private JButton btnAddProduto;
	private JButton btnSalvar;
	private JMoneyField txtValorNota;
	private ArrayList<ProdutoBean> listProdutos = new ArrayList<ProdutoBean>();
	private JTextField txtValorTotal;
	private JButton btnVolta;
	private JXDatePicker dateChooser_Data;
	private JLabel lblProduto;
	private JLabel lblQuantidade;
	private JtextFieldSomenteNumeros txtQuantidade;
	private ProdutoBean produtoSelecionado;
	private JComboBox comboBoxProduto;
	private ArrayList<ProdutoBean> listaDeProduto;
	private ProdutoBean Bean;

	public Nota()
	{
		setTitle("Nota");
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 44, 0, 0, 0, 44, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 161, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		btnSalvar = new JButton();
		btnSalvar.setEnabled(false);
		btnSalvar.setIcon(new ImageIcon(Nota.class.getResource("/imagemIcon/guardar.png")));
		btnSalvar.setMnemonic('s');
		GridBagConstraints gbc_btnSalvar = new GridBagConstraints();
		gbc_btnSalvar.insets = new Insets(0, 0, 5, 5);
		gbc_btnSalvar.gridx = 0;
		gbc_btnSalvar.gridy = 0;
		getContentPane().add(btnSalvar, gbc_btnSalvar);
		btnSalvar.addActionListener(this);

		btnAddProduto = new JButton();
		btnAddProduto.setIcon(new ImageIcon(Nota.class.getResource("/imagemIcon/addProduto.png")));
		btnAddProduto.setMnemonic('p');
		GridBagConstraints gbc_btnAddProduto = new GridBagConstraints();
		gbc_btnAddProduto.insets = new Insets(0, 0, 5, 5);
		gbc_btnAddProduto.gridx = 1;
		gbc_btnAddProduto.gridy = 0;
		getContentPane().add(btnAddProduto, gbc_btnAddProduto);
		btnAddProduto.addActionListener(this);

		btnVolta = new JButton();
		btnVolta.setIcon(new ImageIcon(Nota.class.getResource("/imagemIcon/voltar.png")));
		btnVolta.setMnemonic('v');
		GridBagConstraints gbc_btnVolta = new GridBagConstraints();
		gbc_btnVolta.anchor = GridBagConstraints.WEST;
		gbc_btnVolta.insets = new Insets(0, 0, 5, 5);
		gbc_btnVolta.gridx = 2;
		gbc_btnVolta.gridy = 0;
		getContentPane().add(btnVolta, gbc_btnVolta);
		btnVolta.addActionListener(this);

		JPanel panelNota = new JPanel();
		panelNota.setBorder(BorderFactory.createTitledBorder("<html><h4>Dados da Nota</h4><html>"));
		GridBagConstraints gbc_panelNota = new GridBagConstraints();
		gbc_panelNota.gridwidth = 3;
		gbc_panelNota.insets = new Insets(0, 0, 5, 5);
		gbc_panelNota.fill = GridBagConstraints.BOTH;
		gbc_panelNota.gridx = 1;
		gbc_panelNota.gridy = 1;
		getContentPane().add(panelNota, gbc_panelNota);
		GridBagLayout gbl_panelNota = new GridBagLayout();
		gbl_panelNota.columnWidths = new int[] { 125, 124, 102, 97, 0 };
		gbl_panelNota.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panelNota.columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panelNota.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panelNota.setLayout(gbl_panelNota);

		JLabel lblNumeroDaNota = new JLabel("N�mero da Nota:");
		lblNumeroDaNota.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblNumeroDaNota = new GridBagConstraints();
		gbc_lblNumeroDaNota.insets = new Insets(0, 0, 5, 5);
		gbc_lblNumeroDaNota.anchor = GridBagConstraints.EAST;
		gbc_lblNumeroDaNota.gridx = 0;
		gbc_lblNumeroDaNota.gridy = 2;
		panelNota.add(lblNumeroDaNota, gbc_lblNumeroDaNota);

		txtNumeroDaNota = new JtextFieldSomenteNumeros();
		txtNumeroDaNota.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_txtNumeroDaNota = new GridBagConstraints();
		gbc_txtNumeroDaNota.gridwidth = 3;
		gbc_txtNumeroDaNota.insets = new Insets(0, 0, 5, 0);
		gbc_txtNumeroDaNota.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtNumeroDaNota.gridx = 1;
		gbc_txtNumeroDaNota.gridy = 2;
		panelNota.add(txtNumeroDaNota, gbc_txtNumeroDaNota);
		txtNumeroDaNota.setColumns(10);

		JLabel lblData = new JLabel("Data:");
		lblData.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblData = new GridBagConstraints();
		gbc_lblData.anchor = GridBagConstraints.EAST;
		gbc_lblData.insets = new Insets(0, 0, 5, 5);
		gbc_lblData.gridx = 0;
		gbc_lblData.gridy = 3;
		panelNota.add(lblData, gbc_lblData);

		dateChooser_Data = new JXDatePicker(System.currentTimeMillis());
		dateChooser_Data.getEditor().setFont(new Font("Tahoma", Font.PLAIN, 14));
		dateChooser_Data.setFormats(new String[] { "dd/MM/yyyy" });
		GridBagConstraints gbc_datePicker_1 = new GridBagConstraints();
		gbc_datePicker_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_datePicker_1.insets = new Insets(0, 0, 5, 5);
		gbc_datePicker_1.gridx = 1;
		gbc_datePicker_1.gridy = 3;
		panelNota.add(dateChooser_Data, gbc_datePicker_1);

		JLabel lblValor = new JLabel("Valor da Nota:");
		lblValor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblValor = new GridBagConstraints();
		gbc_lblValor.anchor = GridBagConstraints.EAST;
		gbc_lblValor.insets = new Insets(0, 0, 5, 5);
		gbc_lblValor.gridx = 0;
		gbc_lblValor.gridy = 4;
		panelNota.add(lblValor, gbc_lblValor);

		txtValorNota = new JMoneyField();
		txtValorNota.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_txtValorNota = new GridBagConstraints();
		gbc_txtValorNota.insets = new Insets(0, 0, 5, 5);
		gbc_txtValorNota.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtValorNota.gridx = 1;
		gbc_txtValorNota.gridy = 4;
		panelNota.add(txtValorNota, gbc_txtValorNota);

		JLabel lblValorTotal = new JLabel("Valor  Total:");
		lblValorTotal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblValorTotal = new GridBagConstraints();
		gbc_lblValorTotal.insets = new Insets(0, 0, 5, 5);
		gbc_lblValorTotal.anchor = GridBagConstraints.EAST;
		gbc_lblValorTotal.gridx = 0;
		gbc_lblValorTotal.gridy = 5;
		panelNota.add(lblValorTotal, gbc_lblValorTotal);

		txtValorTotal = new JTextField();
		txtValorTotal.setText("0,00");
		txtValorTotal.setEditable(false);
		txtValorTotal.setEnabled(false);
		txtValorTotal.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_txtValorTotal = new GridBagConstraints();
		gbc_txtValorTotal.insets = new Insets(0, 0, 5, 5);
		gbc_txtValorTotal.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtValorTotal.gridx = 1;
		gbc_txtValorTotal.gridy = 5;
		panelNota.add(txtValorTotal, gbc_txtValorTotal);
		txtValorTotal.setColumns(10);

		lblProduto = new JLabel("Produto:");
		lblProduto.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblProduto = new GridBagConstraints();
		gbc_lblProduto.anchor = GridBagConstraints.EAST;
		gbc_lblProduto.insets = new Insets(0, 0, 5, 5);
		gbc_lblProduto.gridx = 0;
		gbc_lblProduto.gridy = 6;
		panelNota.add(lblProduto, gbc_lblProduto);

		comboBoxProduto = new JComboBox();
		comboBoxProduto.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 6;
		panelNota.add(comboBoxProduto, gbc_comboBox);
		comboBoxProduto.addFocusListener(this);
		comboBoxProduto.addActionListener(this);
		comboBoxProduto.addKeyListener(this);

		lblQuantidade = new JLabel("Quantidade:");
		lblQuantidade.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_lblQuantidade = new GridBagConstraints();
		gbc_lblQuantidade.anchor = GridBagConstraints.EAST;
		gbc_lblQuantidade.insets = new Insets(0, 0, 5, 5);
		gbc_lblQuantidade.gridx = 2;
		gbc_lblQuantidade.gridy = 6;
		panelNota.add(lblQuantidade, gbc_lblQuantidade);

		txtQuantidade = new JtextFieldSomenteNumeros();
		txtQuantidade.setText("0");
		txtQuantidade.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtQuantidade.setColumns(10);
		GridBagConstraints gbc_txtQuantidade = new GridBagConstraints();
		gbc_txtQuantidade.insets = new Insets(0, 0, 5, 0);
		gbc_txtQuantidade.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtQuantidade.gridx = 3;
		gbc_txtQuantidade.gridy = 6;
		panelNota.add(txtQuantidade, gbc_txtQuantidade);
		txtQuantidade.addFocusListener(this);

		JPanel panelProduto = new JPanel();
		GridBagConstraints gbc_panelProduto = new GridBagConstraints();
		gbc_panelProduto.gridwidth = 3;
		gbc_panelProduto.insets = new Insets(0, 0, 0, 5);
		gbc_panelProduto.fill = GridBagConstraints.BOTH;
		gbc_panelProduto.gridx = 1;
		gbc_panelProduto.gridy = 2;
		getContentPane().add(panelProduto, gbc_panelProduto);
		GridBagLayout gbl_panelProduto = new GridBagLayout();
		gbl_panelProduto.columnWidths = new int[] { 0, 0 };
		gbl_panelProduto.rowHeights = new int[] { 0, 0 };
		gbl_panelProduto.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panelProduto.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panelProduto.setLayout(gbl_panelProduto);

		JScrollPane scrollPaneProduto = new JScrollPane();
		GridBagConstraints gbc_scrollPaneProduto = new GridBagConstraints();
		gbc_scrollPaneProduto.fill = GridBagConstraints.BOTH;
		gbc_scrollPaneProduto.gridx = 0;
		gbc_scrollPaneProduto.gridy = 0;
		panelProduto.add(scrollPaneProduto, gbc_scrollPaneProduto);

		table_Produto = new JTable();
		table_Produto.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_Produto.setAutoCreateRowSorter(true);
		table_Produto.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		table_Produto.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "C�digo de Produto", "Descri��o",
				"Valor de Custo", "Margem", "Valor de Venda", "Quantidades", "Categoria" })
		{
			boolean[] columnEditables = new boolean[] { false, false, false, false, false, false, false };

			public boolean isCellEditable(int row, int column)
			{
				return columnEditables[column];
			}
		});
		table_Produto.getColumnModel().getColumn(0).setPreferredWidth(140);
		table_Produto.getColumnModel().getColumn(1).setPreferredWidth(140);
		table_Produto.getColumnModel().getColumn(2).setPreferredWidth(50);
		table_Produto.getColumnModel().getColumn(3).setPreferredWidth(50);
		table_Produto.getColumnModel().getColumn(4).setPreferredWidth(50);
		table_Produto.getColumnModel().getColumn(5).setPreferredWidth(50);
		table_Produto.getColumnModel().getColumn(6).setPreferredWidth(100);
		table_Produto.addMouseListener(this);
		table_Produto.addKeyListener(this);
		scrollPaneProduto.setViewportView(table_Produto);

		// carregando lista dinamica
		carregaProdutoDinamico();

	}// fim do construtor

	/**
	 * Metodo que carrega todos os produtos dinamicamente, e joga no JTextField.
	 **/
	public void carregaProdutoDinamico()
	{
		Connection connection = null;
		AcessoBD acessoBD = new AcessoBD();
		listaDeProduto = new ArrayList<ProdutoBean>();
		try
		{
			connection = acessoBD.obtemConexao();
			listaDeProduto = ProdutoDao.carregarTodosProduto(connection);
			for (ProdutoBean produtoBean : listaDeProduto)
			{

				comboBoxProduto.addItem(produtoBean.getDescricao());
			}

			AutoCompleteDecorator.decorate(this.comboBoxProduto);
		} catch (Exception e)
		{
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,
					"Problema ao carregar Produtos. Por favor, feche a tela e tente novamente, ou entre em contato com Administrador do sistema",
					"Erro", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void actionPerformed(ActionEvent event)
	{

		if (event.getSource() == btnAddProduto)
		{
			CadastroProduto cadastroProduto = new CadastroProduto(null, 0);
			if (cadastroProduto.cadastroProduto())
			{
				JOptionPane.showMessageDialog(null, "Produto criado com sucesso ", "Cria��o com sucesso",
						JOptionPane.PLAIN_MESSAGE);
				carregaProdutoDinamico();
			}
		} // fim da verifi��o do addProduto

		if (event.getSource() == btnSalvar)
		{
			String valorNota = txtValorNota.getText().replace(".", "");
			String valorTotal = txtValorTotal.getText().replace(".", "");

			if (Double.parseDouble(valorNota.replace(",", ".")) < Double.parseDouble(valorTotal.replace(",", "."))
					|| Double.parseDouble(valorNota.replace(",", ".")) > Double
							.parseDouble(valorTotal.replace(",", ".")))
			{
				int respo = JOptionPane.showConfirmDialog(null,
						"Valor da soma dos produtos e o valor total da nota est�o diferentes. Deseja realmente salvar?",
						"Importante", JOptionPane.YES_NO_OPTION);
				if (respo == JOptionPane.YES_OPTION)
				{
					inserirProdutos();
				}
			} else
			{
				inserirProdutos();
			}

		}
		if (event.getSource() == btnVolta)
		{
			dispose();
		}

	}

	/** Metodo que inserir um arraylist de produtos no banco **/
	private void inserirProdutos()
	{
		DefaultTableModel tableModel = (DefaultTableModel) this.table_Produto.getModel();
		ArrayList<ProdutoBean> produtoBeans = new ArrayList<ProdutoBean>();
		int idNota = 0;
		int i = 0;

		Connection connection = null;
		AcessoBD acessoBD = new AcessoBD();

		while (i < tableModel.getRowCount())
		{

			try
			{
				connection = acessoBD.obtemConexao();
				ProdutoBean bean = ProdutoDao.buscaPorNome(connection, String.valueOf(tableModel.getValueAt(i, 1)));
				bean.setQuantidade(Integer.parseInt(String.valueOf(tableModel.getValueAt(i, 5))));

				if (ProdutoDao.updateEstoque(connection, bean.getQuantidade(), bean.getId()))
				{
					produtoBeans.add(bean);

				}
				i++;
			} catch (SQLException e)
			{

				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Erro ao inserir nota ", "Erro", JOptionPane.ERROR_MESSAGE);
				dispose();
			}

		}

		String valorNota = txtValorNota.getText().replace(".", "");
		Date dataNota = dateChooser_Data.getDate();

		try
		{
			connection = acessoBD.obtemConexao();

			NotaBean notaBean = new NotaBean();
			notaBean.setNumero_nota(txtNumeroDaNota.getText());
			notaBean.setTotal(Float.parseFloat(valorNota.replace(",", ".")));
			notaBean.setData(dataNota);
			if (NotaDao.inserirNota(connection, notaBean))
			{
				idNota = NotaDao.getProximoId(connection);

				for (ProdutoBean produtoBean : produtoBeans)
				{
					ProdutoNotaDao.inserirNota(connection, idNota, produtoBean.getId(), dateChooser_Data);
				}
				JOptionPane.showMessageDialog(null,
						"Nota " + txtNumeroDaNota.getText() + " criada com sucesso. Foram inseridos "
								+ tableModel.getRowCount() + " produtos",
						"Cria��o com sucesso", JOptionPane.PLAIN_MESSAGE);
				dispose();
			}

		} catch (SQLException e)
		{

			JOptionPane.showMessageDialog(null, "Erro ao inserir nota ", "Erro", JOptionPane.ERROR_MESSAGE);
			dispose();

		}
	}

	/**
	 * Metodo que atualiza o campo de total
	 * 
	 * @param dtm
	 **/
	private void atualizaTotal(DefaultTableModel dtm, ArrayList<ProdutoBean> listaProduto)
	{
		txtValorTotal.setText("");
		double valor = 0;
		for (ProdutoBean produtoBean : listaProduto)
		{

			valor += produtoBean.getValor_venda() * produtoBean.getQuantidade();

		}
		txtValorTotal.setText(String.format("%.2f", valor));

	}

	@Override
	public void focusLost(FocusEvent event)
	{

		if (event.getSource() == txtQuantidade)
		{
			incluirProdutoTabela();
			comboBoxProduto.requestFocus();
			txtQuantidade.setText("0");
			
		}

	}

	public void incluirProdutoTabela()
	{
		if (txtQuantidade.getText().equals("") || Integer.parseInt(txtQuantidade.getText()) <= 0)
		{
			JOptionPane.showMessageDialog(null, "Quantidade Invalida", "Importante", JOptionPane.INFORMATION_MESSAGE);

			comboBoxProduto.requestFocus();
			return;

		}
		if (Integer.parseInt(txtQuantidade.getText()) > 0 || !txtQuantidade.getText().equals(""))
		{

			DefaultTableModel dtm = (DefaultTableModel) table_Produto.getModel();

			Connection conn = null;
			AcessoBD acessoBD = new AcessoBD();
			try
			{
				conn = acessoBD.obtemConexao();
				Bean = ProdutoDao.buscaPorNome(conn, comboBoxProduto.getSelectedItem().toString());
				Bean.setQuantidade(Integer.parseInt(txtQuantidade.getText()));
	
			} catch (Exception e)
			{
				e.printStackTrace();
				JOptionPane.showMessageDialog(null,
						"Problema ao carregar Produtos. Por favor, feche a tela e tente novamente, ou entre em contato com Administrador do sistema",
						"Erro", JOptionPane.ERROR_MESSAGE);
			}

			for (ProdutoBean produtoBean : listProdutos)
			{
				
				if (produtoBean.getDescricao().equals(comboBoxProduto.getSelectedItem().toString()))
				{
					int somaQuantidade = Integer.parseInt(txtQuantidade.getText()) + produtoBean.getQuantidade();
					Bean.setQuantidade(somaQuantidade);
					listProdutos.remove(produtoBean);
					break;
				}
			}
			while (dtm.getRowCount() > 0)
			{
				dtm.removeRow(0);

			}
			
			listProdutos.add(Bean);
			atualizarGrid(dtm);

		}
	}

	/** Metodo que atualiza Jtable de produtos **/
	private void atualizarGrid(DefaultTableModel dtm)
	{
		NumberFormat nf = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
		String valorNota = "";
		String valorTotal = "";

		atualizaTotal(dtm, listProdutos);
		if (txtValorTotal.getText().equals(txtValorNota.getText()))
		{
			btnSalvar.setEnabled(true);
			btnAddProduto.setEnabled(false);
		}

		valorNota = txtValorNota.getText().replace(".", "");
		valorTotal = txtValorTotal.getText().replace(".", "");
		

		for (ProdutoBean produtoBean : listProdutos)
		{
			adcionaLinhas(dtm, produtoBean);

		}

		if (Double.parseDouble(valorNota.replace(",", ".")) < Double.parseDouble(valorTotal.replace(",", ".")))
		{
			btnSalvar.setEnabled(true);
			int respo = JOptionPane.showConfirmDialog(null,
					"Valor da soma dos produtos � maior que o valor total da nota. \nDeseja continuar adicionando produto?",
					"Importante", JOptionPane.YES_NO_OPTION);
			if (respo != JOptionPane.YES_OPTION)
			{
				btnAddProduto.setEnabled(false);
				comboBoxProduto.requestFocus();
				return;

			} else
			{
				int i = 0;
				while (i != dtm.getRowCount())
				{
					i++;

				}
				if (i == dtm.getRowCount())
				{
					dtm.removeRow(0);

				}

				atualizarGrid(dtm);
				return;
			}

		}
	}

	@Override
	public void mouseClicked(MouseEvent event)
	{
		if (event.getClickCount() == 2)
		{
			int row = table_Produto.getSelectedRow();
			DefaultTableModel tableModel = (DefaultTableModel) this.table_Produto.getModel();

			CadastroProduto cadastroProduto = new CadastroProduto(tableModel, row);
			cadastroProduto.setResizable(true);
			cadastroProduto.setLocationRelativeTo(null);
			cadastroProduto.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

			ProdutoBean bean = cadastroProduto.retornaProduto();
			if (bean != null)
			{

				int i = 0;
				while (i != row)
				{
					i++;

				}
				if (i == row)
				{
					tableModel.removeRow(0);
					tableModel.addRow(new Object[] { (bean.getId() + bean.getLaboratorio()), bean.getDescricao(),
							bean.getValor_custo(), bean.getMargem(), bean.getValor_venda(), bean.getQuantidade(),
							bean.getCategoria() });
				}

				for (int j = 0; j < comboBoxProduto.getItemCount(); j++)
				{
					System.out.println(comboBoxProduto.getSelectedItem());
					if (comboBoxProduto.getSelectedItem() == bean.getDescricao())
					{
						System.out.println("Entrou");
					}
				}

				carregaProdutoDinamico();
			}
		}

	}

	public void mouseEntered(MouseEvent arg0)
	{

	}

	@Override
	public void mouseExited(MouseEvent arg0)
	{

	}

	@Override
	public void mousePressed(MouseEvent arg0)
	{

	}

	@Override
	public void mouseReleased(MouseEvent arg0)
	{

	}

	@Override
	public void keyPressed(KeyEvent event)
	{

		if (event.getKeyChar() == KeyEvent.VK_DELETE)
		{
			if (event.getSource() == table_Produto)
			{
				int respo = JOptionPane.showConfirmDialog(null, "Deseja realmente remover o produto selecionado?",
						"Importante", JOptionPane.YES_NO_OPTION);
				if (respo == JOptionPane.YES_OPTION)
				{
					DefaultTableModel dtm = (DefaultTableModel) table_Produto.getModel();

					for (ProdutoBean produtoBean : listProdutos)
					{
						
						if (produtoBean.getDescricao().equals(dtm.getValueAt(table_Produto.getSelectedRow(), 1)))
						{

							listProdutos.remove(produtoBean);
							while (dtm.getRowCount() > 0)
							{
								dtm.removeRow(0);

							}
							atualizaTotal(dtm, listProdutos);

							for (ProdutoBean produtoBean2 : listProdutos)
							{
								adcionaLinhas(dtm, produtoBean2);

							}

							break;
						}

					}

				}

			}
		}
	}

	private void adcionaLinhas(DefaultTableModel dtm, ProdutoBean bean)
	{
		dtm.addRow(new Object[] { (bean.getId() + bean.getLaboratorio()), bean.getDescricao(), bean.getValor_custo(),
				bean.getMargem(), bean.getValor_venda(), bean.getQuantidade(), bean.getCategoria() });
	}

	@Override
	public void keyReleased(KeyEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void focusGained(FocusEvent event)
	{
		
		System.out.println("Gained");

	}

}
