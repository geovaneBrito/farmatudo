package br.com.farmatudo.views;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.com.farmatudo.util.GravaTXT;
import br.com.farmatudo.util.Impressora;
import br.com.farmatudo.util.TelaUtil;
import javax.swing.JCheckBox;
import javax.swing.JPasswordField;
import java.awt.Toolkit;

public class NovoAcessoDB extends JDialog implements ActionListener
{
	private JTextField txt_Usuario;
	private JPasswordField passWdField_Senha;
	private JButton btn_Salvar;
	private static ResourceBundle formatLanguage;
	private JLabel lbl_TipoDeUsuario;
	private JComboBox comboBox_Tipo;
	private JCheckBox chckbx_Senha;

	public NovoAcessoDB()
	{
		setIconImage(Toolkit.getDefaultToolkit().getImage(NovoAcessoDB.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));

		TelaUtil.setaNimbus(this);
		setModal(true);
		setSize(424, 217);
		setResizable(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]
		{ 0, 99, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[]
		{ 44, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[]
		{ 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[]
		{ 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		JLabel lbl_Usuario = new JLabel();
		lbl_Usuario.setText("Usu\u00E1rio do Banco");
		lbl_Usuario.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_Usuario = new GridBagConstraints();
		gbc_lbl_Usuario.anchor = GridBagConstraints.SOUTHEAST;
		gbc_lbl_Usuario.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Usuario.gridx = 1;
		gbc_lbl_Usuario.gridy = 0;
		getContentPane().add(lbl_Usuario, gbc_lbl_Usuario);

		txt_Usuario = new JTextField();
		txt_Usuario.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_txt_Usuario = new GridBagConstraints();
		gbc_txt_Usuario.anchor = GridBagConstraints.SOUTH;
		gbc_txt_Usuario.insets = new Insets(0, 0, 5, 5);
		gbc_txt_Usuario.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_Usuario.gridx = 2;
		gbc_txt_Usuario.gridy = 0;
		getContentPane().add(txt_Usuario, gbc_txt_Usuario);
		txt_Usuario.setColumns(10);

		JLabel lbl_Senha = new JLabel();
		lbl_Senha.setText("Senha de Acesso");
		lbl_Senha.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_lbl_Senha = new GridBagConstraints();
		gbc_lbl_Senha.anchor = GridBagConstraints.EAST;
		gbc_lbl_Senha.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Senha.gridx = 1;
		gbc_lbl_Senha.gridy = 1;
		getContentPane().add(lbl_Senha, gbc_lbl_Senha);

		passWdField_Senha = new JPasswordField();
		passWdField_Senha.setEchoChar('*');
		passWdField_Senha.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		passWdField_Senha.setColumns(10);
		GridBagConstraints gbc_txt_Senha = new GridBagConstraints();
		gbc_txt_Senha.insets = new Insets(0, 0, 5, 5);
		gbc_txt_Senha.fill = GridBagConstraints.HORIZONTAL;
		gbc_txt_Senha.gridx = 2;
		gbc_txt_Senha.gridy = 1;
		getContentPane().add(passWdField_Senha, gbc_txt_Senha);


		btn_Salvar = new JButton();
		btn_Salvar.setText("Salvar");
		btn_Salvar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btn_Salvar.addActionListener(this);
		
		chckbx_Senha = new JCheckBox();
		chckbx_Senha.setText("Verifica Senha");
		chckbx_Senha.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		chckbx_Senha.addActionListener(this);
		GridBagConstraints gbc_chckbx_Senha = new GridBagConstraints();
		gbc_chckbx_Senha.anchor = GridBagConstraints.WEST;
		gbc_chckbx_Senha.insets = new Insets(0, 0, 5, 0);
		gbc_chckbx_Senha.gridx = 3;
		gbc_chckbx_Senha.gridy = 1;
		getContentPane().add(chckbx_Senha, gbc_chckbx_Senha);
		
		btn_Salvar.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		GridBagConstraints gbc_btn_Salvar = new GridBagConstraints();
		gbc_btn_Salvar.insets = new Insets(0, 0, 5, 5);
		gbc_btn_Salvar.gridx = 2;
		gbc_btn_Salvar.gridy = 3;
		getContentPane().add(btn_Salvar, gbc_btn_Salvar);
		TelaUtil.AcaoTeclaButton(this, btn_Salvar, KeyEvent.VK_ENTER);

	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		if (event.getSource() == btn_Salvar)
		{
				
			
				if (!txt_Usuario.getText().equals(""))
				{
					GravaTXT gravaTXT = new GravaTXT();
				
					if(gravaTXT.gravarNovoTxt(passWdField_Senha.getText(), txt_Usuario.getText()))
					{
						JOptionPane.showMessageDialog(null, "Acesso do Banco Alterado");
						dispose();
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Erro para cadastrar novo usu�rio de Banco");
						dispose();
					}

				}
				else
				{
					JOptionPane.showMessageDialog(null, "Favor preencher os campos");
				}	
			
		}
		
		if (event.getSource() == chckbx_Senha)
		{
			if (chckbx_Senha.isSelected())
			{
				System.out.println("Selecionado");
				passWdField_Senha.setEchoChar('\u0000');
				
			}
			else
			{
				passWdField_Senha.setEchoChar('*');
			}	
		}
		

	}

	
	


}
