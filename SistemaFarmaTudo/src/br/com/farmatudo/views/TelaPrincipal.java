package br.com.farmatudo.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.JXDatePicker;

import br.com.farmatudo.bean.UsuarioBean;
import br.com.farmatudo.daos.ProdutoDao;
import br.com.farmatudo.daos.VendaDao;
import br.com.farmatudo.util.AcessoBD;
import br.com.farmatudo.util.TelaUtil;

public class TelaPrincipal extends JFrame implements ActionListener
{
	private JButton btn_Estoque;
	private JButton btn_Laboratorio;
	private JButton btn_Relatrio;
	private JButton btn_Usuario;
	private JButton btn_Vendas;
	UsuarioBean usuario;

	/**
	 * Contrutor da Classe TelaPrincipal
	 * 
	 * @param usuarioBean
	 */
	public TelaPrincipal(UsuarioBean usuarioBean)
	{
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		// configurando a��es da Janela
		usuario = usuarioBean;
		getContentPane().setBackground(Color.WHITE);

		setLocationRelativeTo(null);
		setTitle("Seja Bem Vindo");
		setSize(982, 500);
		// setUndecorated(true);
		setResizable(false);

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		// instanciando GridBagLayout e adcionando na tela
		GridBagLayout containerGrid = new GridBagLayout();
		containerGrid.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 };
		containerGrid.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0 };
		containerGrid.columnWidths = new int[] { 120, 120, 120, 155, 120 }; // tamanho
																			// de
																			// cada
																			// coluna
		containerGrid.rowHeights = new int[] { 53, 41, 110, 110, 0 };// tamanho
																		// de
																		// cada
																		// linha
		getContentPane().setLayout(containerGrid);

		// instanciando JLabel de Sauda��o e adcionando na tela
		JLabel lbl_Saudacao = new JLabel("Seja Bem-Vindo " + usuarioBean.getNome().toUpperCase());
		lbl_Saudacao.setVerticalAlignment(SwingConstants.BOTTOM);
		lbl_Saudacao.setFont(new Font("Segoe UI", Font.BOLD, 14));
		GridBagConstraints gbc_lbl_Saudacao = new GridBagConstraints();
		gbc_lbl_Saudacao.anchor = GridBagConstraints.SOUTH;
		gbc_lbl_Saudacao.gridwidth = 3;
		gbc_lbl_Saudacao.insets = new Insets(0, 10, 5, 5); // Insets(int top,
															// int left, int
															// bottom, int
															// right)
		gbc_lbl_Saudacao.gridx = 1;
		gbc_lbl_Saudacao.gridy = 0;
		getContentPane().add(lbl_Saudacao, gbc_lbl_Saudacao);

		// instanciando JLabel de A��o e adcionando na tela
		JLabel lbl_acao = new JLabel("O que deseja fazer?");
		lbl_acao.setFont(new Font("Segoe UI", Font.BOLD, 12));
		GridBagConstraints gbc_lbl_acao = new GridBagConstraints();
		gbc_lbl_acao.gridwidth = 2;
		gbc_lbl_acao.anchor = GridBagConstraints.NORTHEAST;
		gbc_lbl_acao.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_acao.gridx = 2;
		gbc_lbl_acao.gridy = 1;
		getContentPane().add(lbl_acao, gbc_lbl_acao);

		// instanciando JPanel de Curso e adcionando no Grid
		JPanel pn_Estoque = new JPanel();
		GridBagConstraints gbc_pn_Estoque = new GridBagConstraints();
		gbc_pn_Estoque.insets = new Insets(0, 0, 5, 5);
		gbc_pn_Estoque.fill = GridBagConstraints.BOTH;
		gbc_pn_Estoque.gridx = 0;
		gbc_pn_Estoque.gridy = 2;
		getContentPane().add(pn_Estoque, gbc_pn_Estoque);
		pn_Estoque.setLayout(new BorderLayout(0, 0));

		// instanciando JButton de Curso e adcionando ao JPanel de Curso
		btn_Estoque = new JButton();
		btn_Estoque.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/imagemIcon/estoque.png")));
		btn_Estoque.setToolTipText("Estoque");
		btn_Estoque.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btn_Estoque.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btn_Estoque.addActionListener(this);
		pn_Estoque.add(btn_Estoque, BorderLayout.CENTER);

		JPanel pn_Usuario = new JPanel();
		GridBagConstraints gbc_pn_Usuario = new GridBagConstraints();
		gbc_pn_Usuario.insets = new Insets(0, 0, 5, 5);
		gbc_pn_Usuario.fill = GridBagConstraints.BOTH;
		gbc_pn_Usuario.gridx = 1;
		gbc_pn_Usuario.gridy = 2;
		getContentPane().add(pn_Usuario, gbc_pn_Usuario);
		pn_Usuario.setLayout(new BorderLayout(0, 0));

		btn_Usuario = new JButton();
		btn_Usuario.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/imagemIcon/usuarios.png")));
		btn_Usuario.setToolTipText("Usu�rio");
		btn_Usuario.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btn_Usuario.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btn_Usuario.addActionListener(this);
		pn_Usuario.add(btn_Usuario, BorderLayout.CENTER);

		JPanel pn_Venda = new JPanel();
		GridBagConstraints gbc_pn_Venda = new GridBagConstraints();
		gbc_pn_Venda.insets = new Insets(0, 0, 5, 5);
		gbc_pn_Venda.fill = GridBagConstraints.BOTH;
		gbc_pn_Venda.gridx = 2;
		gbc_pn_Venda.gridy = 2;
		getContentPane().add(pn_Venda, gbc_pn_Venda);
		pn_Venda.setLayout(new BorderLayout(0, 0));

		btn_Vendas = new JButton();
		btn_Vendas.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/imagemIcon/venda.png")));
		btn_Vendas.setToolTipText("Venda");
		btn_Vendas.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btn_Vendas.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btn_Vendas.addActionListener(this);
		pn_Venda.add(btn_Vendas, BorderLayout.CENTER);

		// instanciando JPanel de Matricula e adcionando no Grid
		JPanel pn_Laboratorio = new JPanel();
		GridBagConstraints gbc_pn_Laboratorio = new GridBagConstraints();
		gbc_pn_Laboratorio.insets = new Insets(0, 0, 5, 5);
		gbc_pn_Laboratorio.fill = GridBagConstraints.BOTH;
		gbc_pn_Laboratorio.gridx = 3;
		gbc_pn_Laboratorio.gridy = 2;
		getContentPane().add(pn_Laboratorio, gbc_pn_Laboratorio);
		pn_Laboratorio.setLayout(new BorderLayout(0, 0));

		// instanciando JButton de Matricula e adcionando ao JPanel de Matricula
		btn_Laboratorio = new JButton();
		btn_Laboratorio.setToolTipText("Laborat�rio\\Fornecedor\\Categoria");
		btn_Laboratorio.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/imagemIcon/laboratorio.png")));
		btn_Laboratorio.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btn_Laboratorio.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btn_Laboratorio.addActionListener(this);
		pn_Laboratorio.add(btn_Laboratorio, BorderLayout.CENTER);

		// instanciando JPanel de Relat�rio e adcionando no Grid
		JPanel pn_Relatorio = new JPanel();
		GridBagConstraints gbc_pn_Relatorio = new GridBagConstraints();
		gbc_pn_Relatorio.insets = new Insets(0, 0, 5, 0);
		gbc_pn_Relatorio.fill = GridBagConstraints.BOTH;
		gbc_pn_Relatorio.gridx = 4;
		gbc_pn_Relatorio.gridy = 2;
		getContentPane().add(pn_Relatorio, gbc_pn_Relatorio);
		pn_Relatorio.setLayout(new BorderLayout(0, 0));

		// instanciando JButton de Relat�rio e adcionando ao JPanel de Relat�rio
		btn_Relatrio = new JButton("");
		btn_Relatrio.setToolTipText("Relat�rio");
		btn_Relatrio.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/imagemIcon/relatorio.png")));
		btn_Relatrio.setCursor(new Cursor(Cursor.HAND_CURSOR));
		pn_Relatorio.add(btn_Relatrio, BorderLayout.CENTER);
		btn_Relatrio.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		btn_Relatrio.addActionListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == btn_Relatrio)
		{
			Object[] options = { "Relat�rio de Vendas", "Relat�rio de Estoque" };
			int optionselecionado = JOptionPane.showOptionDialog(null, "Favor selecione uma das op��o", "Relat�rio",
					JOptionPane.INFORMATION_MESSAGE, JOptionPane.OK_OPTION, null, options, options[0]);
			if (optionselecionado >= 0)
			{
				Connection conn = null;
				AcessoBD acessoBD = new AcessoBD();
				if (optionselecionado == 0)
				{
					
					JXDatePicker datePickerDataRelatorio = new JXDatePicker(System.currentTimeMillis());
					Object[] components = { "A partir de qual data? ", datePickerDataRelatorio };
					Object[] buttons = { "OK", "CANCELAR" };
					int dataSelecionada = JOptionPane.showOptionDialog(null, components, "Relatorio",
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, buttons, buttons[0]);
					System.out.println(dataSelecionada);
					if (JOptionPane.YES_NO_OPTION == dataSelecionada)
					{
						
					
						try
						{
							conn = acessoBD.obtemConexao();
							TelaUtil.gerarRelatorio(VendaDao.retornaRelatorioVendas(conn, datePickerDataRelatorio.getDate()),
									"relatorios/report_venda.jasper");
						} catch (Exception e2)
						{
							e2.printStackTrace();
							JOptionPane.showMessageDialog(null, "Erro ao abrir relatorio", "Erro",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				} else if (optionselecionado == 1)
				{
					try
					{
						conn = acessoBD.obtemConexao();
						TelaUtil.gerarRelatorio(ProdutoDao.retornaRelatorioEstoque(conn),
								"relatorios/report_produtos_estoque.jasper");
					} catch (Exception e3)
					{
						e3.printStackTrace();
						JOptionPane.showMessageDialog(null, "Erro ao abrir relatorio", "Erro",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}

		}
		if (e.getSource() == btn_Vendas)
		{

			Venda venda = new Venda(usuario);
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Dimension screenSize = toolkit.getScreenSize();
			venda.setBounds(0, 0, screenSize.width, screenSize.height);
			venda.setLocationRelativeTo(null);
			venda.setVisible(true);
		}
		if (e.getSource() == btn_Usuario)
		{

			NovoUsuario novoUsuario = new NovoUsuario();

			novoUsuario.setVisible(true);
		}
		if (e.getSource() == btn_Estoque)
		{
			Produto produto = new Produto();
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Dimension screenSize = toolkit.getScreenSize();
			produto.setBounds(0, 0, screenSize.width, screenSize.height);
			produto.setVisible(true);
		}
		if (e.getSource() == btn_Laboratorio)
		{
			Fornecedor_Laboratorio for_lab = new Fornecedor_Laboratorio();

			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Dimension screenSize = toolkit.getScreenSize();

			for_lab.setBounds(0, 0, screenSize.width, screenSize.height);
			for_lab.setVisible(true);
		}
	}
}
