package br.com.farmatudo.views;

import javax.swing.JDialog;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import org.jdesktop.swingx.JXDatePicker;
import java.awt.GridBagConstraints;
import java.awt.Color;
import javax.swing.border.EtchedBorder;

public class DataRelatorio extends JDialog
{
	public DataRelatorio()
	{
		GridBagLayout gridBagLayout = new GridBagLayout();
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(TelaPrincipal.class.getResource("/imagemIcon/icone-drogaria-farmacia.png")));
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JXDatePicker datePickerRelatorio = new JXDatePicker(System.currentTimeMillis());
		datePickerRelatorio.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		datePickerRelatorio.setBackground(Color.WHITE);
		GridBagConstraints gbc_datePickerRelatorio = new GridBagConstraints();
		gbc_datePickerRelatorio.gridx = 1;
		gbc_datePickerRelatorio.gridy = 1;
		getContentPane().add(datePickerRelatorio, gbc_datePickerRelatorio);
		
	}
}
