package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.farmatudo.bean.CategoriaBean;


public class CategoriaDao
{
	private static String BUSCAR_TODOS = "SELECT * FROM db_farmatudo_.categoria";
	private static String ATUALIZAR_CATEGORIA = "UPDATE categoria SET nome = ? WHERE nome = ?";
	private static String CRIAR_CATEGORIA = "INSERT INTO categoria (nome) VALUES(?)";
	
	public static ArrayList<CategoriaBean> carregarTodasCategoria(Connection conn)
	{
		PreparedStatement stm = null;
		ResultSet rs = null;
		ArrayList<CategoriaBean> listCategoria = new ArrayList<CategoriaBean>();
		try
		{
			stm = conn.prepareStatement(BUSCAR_TODOS);
			rs = stm.executeQuery();
			while (rs.next())
			{
				CategoriaBean categoriaBean = new CategoriaBean();
				categoriaBean.setNome(rs.getString(1));
				listCategoria.add(categoriaBean);
			}
		return listCategoria;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listCategoria;
		
	}
	
	/**Metodo que atualizar categoria**/
	public static boolean atualizarCategoria(Connection conn, String p_NomeAtualizado, String p_NomeAtual)
	{
		PreparedStatement stm = null;
		boolean flagOk = false;

		try
		{
			stm = conn.prepareStatement(ATUALIZAR_CATEGORIA);
			stm.setString(1, p_NomeAtualizado);
			stm.setString(2, p_NomeAtual);
			stm.executeUpdate();
			flagOk = true;
		return flagOk;
		} catch (Exception e)
		{
			e.printStackTrace();
			flagOk = false;
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return flagOk;
		
	}

	public static boolean criarCategoria(Connection conn, String p_NomeCategoria)
	{
		PreparedStatement stm = null;
		boolean flagOk = false;

		try
		{
			stm = conn.prepareStatement(CRIAR_CATEGORIA);
			stm.setString(1, p_NomeCategoria);
		
			stm.executeUpdate();
			flagOk = true;
		return flagOk;
		} catch (Exception e)
		{
			e.printStackTrace();
			flagOk = false;
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return flagOk;
	}
}
