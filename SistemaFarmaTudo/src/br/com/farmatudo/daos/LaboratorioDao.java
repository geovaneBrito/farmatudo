package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.farmatudo.bean.CategoriaBean;
import br.com.farmatudo.bean.FornecedorBean;
import br.com.farmatudo.bean.LaboratorioBean;

public class LaboratorioDao
{

	private static String QUERY_BUSCAR_TODOS = "SELECT * FROM db_farmatudo_.laboratorio";
	private static String QUERY_BUSCAR_LABORATORIO_EXPE = "SELECT * FROM db_farmatudo_.laboratorio where telefone = ? and nome = ?";
	private static String BUSCAR_TODOS_NOME = "SELECT nome FROM db_farmatudo_.laboratorio";
	private static String QUERY_BUSCAR_LABORATORIO_NOME = "SELECT * FROM db_farmatudo_.laboratorio where nome like ?";
	private static String QUERY_BUSCAR_LABORATORIO_ID = "SELECT * FROM db_farmatudo_.laboratorio where id like ?";
	private static String QUERY_BUSCAR_LABORATORIO_TELEFONE = "SELECT * FROM db_farmatudo_.laboratorio where telefone like ?";
	private static String QUERY_INSERIR_LABORATORIO = "INSERT INTO `db_farmatudo_`.`laboratorio`"
			+"(`id`,`nome`,`telefone`) VALUES (?,?,?)";
	private static String QUERY_UPDATE_LABORATORIO = "UPDATE `db_farmatudo_`.`laboratorio` SET"
			+"`telefone` = ? WHERE `nome` = ?";
	
	
	public static ArrayList<LaboratorioBean> carregarLaboratorioPorTelefone(Connection conn, String p_Telefone)
	{
		PreparedStatement stm = null;
		ResultSet rs = null;
		 ArrayList<LaboratorioBean> listLabora = new ArrayList<LaboratorioBean>();
		try
		{
			stm = conn.prepareStatement(QUERY_BUSCAR_LABORATORIO_TELEFONE);
			stm.setString(1, p_Telefone);
			rs = stm.executeQuery();
			while (rs.next())
			{
				LaboratorioBean laboratorioBean = new LaboratorioBean();
				laboratorioBean.setNome(rs.getString(2));
				laboratorioBean.setTelefone(rs.getString(3));
			
				listLabora.add(laboratorioBean);
			}
			return listLabora;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listLabora;

	}
	
	public static ArrayList<LaboratorioBean> carregarLaboratorioPorNome(Connection conn, String p_Nome)
	{
		PreparedStatement stm = null;
		ResultSet rs = null;
		 ArrayList<LaboratorioBean> listLabora = new ArrayList<LaboratorioBean>();
		try
		{
			stm = conn.prepareStatement(QUERY_BUSCAR_LABORATORIO_NOME);
			stm.setString(1, p_Nome);
			rs = stm.executeQuery();
			while (rs.next())
			{
				LaboratorioBean laboratorioBean = new LaboratorioBean();
				laboratorioBean.setNome(rs.getString(2));
				laboratorioBean.setTelefone(rs.getString(3));
			
				listLabora.add(laboratorioBean);
			}
			return listLabora;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listLabora;

	}
	
	public static boolean updateLaboratorio(Connection conn, LaboratorioBean laboratorio)
	{
		boolean flagOk = false;
		PreparedStatement statement = null;
			
		try
		{
			statement = conn.prepareStatement(QUERY_UPDATE_LABORATORIO);
			statement.setString(1, laboratorio.getTelefone());
			statement.setString(2, laboratorio.getNome());

			statement.executeUpdate();
			flagOk = true;
		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return flagOk;

		
	}
	
			
	
	public static boolean salvar(Connection conn, LaboratorioBean laboratorio) {
		boolean flagOk = false;
		PreparedStatement statement = null;
		ResultSet result = null;

		try {
			statement = conn.prepareStatement(QUERY_INSERIR_LABORATORIO);
			
			statement.setInt(1, laboratorio.getId());
			statement.setString(2, laboratorio.getNome());
			statement.setString(3, laboratorio.getTelefone());


			statement.executeUpdate();
			flagOk = true;
		} catch (Exception e) {
			flagOk = false;
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException el) {
				System.out.println(el.getStackTrace());
			}
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException el2) {
					System.err.println(el2.getStackTrace());
				}
			}
		}

		return flagOk;

	}
	
	/***Metodo que realiza consulta de todos os laboratorio, trazendo todos os dados dos laboratorios*/
	public static ArrayList<LaboratorioBean> carregarLaboratorios(Connection conn)
	{
		PreparedStatement stm = null;
		ResultSet rs = null;
		ArrayList<LaboratorioBean> listLaboratorios = new ArrayList<LaboratorioBean>();
		try
		{
			stm = conn.prepareStatement(QUERY_BUSCAR_TODOS);
			rs = stm.executeQuery();
			while (rs.next())
			{
				LaboratorioBean laboratorioBean = new LaboratorioBean();
				laboratorioBean.setId(rs.getInt(1));
				laboratorioBean.setNome(rs.getString(2));
				laboratorioBean.setTelefone(rs.getString(3));
				
				listLaboratorios.add(laboratorioBean);
			}
		return listLaboratorios;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listLaboratorios;
		
	}
	
	/***Metodo que realiza consulta de todos os laboratorio , e traz apenas o nome deles**/
	public static ArrayList<LaboratorioBean> carregarNomeLaboratorio(Connection conn)
	{
		PreparedStatement stm = null;
		ResultSet rs = null;
		ArrayList<LaboratorioBean> listNomeLaboratorio = new ArrayList<LaboratorioBean>();
		try
		{
			stm = conn.prepareStatement(BUSCAR_TODOS_NOME);
			rs = stm.executeQuery();
			while (rs.next())
			{
				LaboratorioBean laboratorioBean = new LaboratorioBean();
				laboratorioBean.setNome(rs.getString(1));
				listNomeLaboratorio.add(laboratorioBean);
			}
		return listNomeLaboratorio;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listNomeLaboratorio;
		
	}

	public static LaboratorioBean buscaLaboratorioExpecifico(Connection conn, String p_Telefone, String p_Nome) {
		
		PreparedStatement stm = null;
		ResultSet rs = null;
		LaboratorioBean laboratorioBean = new LaboratorioBean();
		try
		{
			stm = conn.prepareStatement(QUERY_BUSCAR_LABORATORIO_EXPE);
			stm.setString(1, p_Telefone);
			stm.setString(2, p_Nome);
			rs = stm.executeQuery();
			if (rs.next())
			{

				laboratorioBean.setNome(rs.getString(2));
				laboratorioBean.setTelefone(rs.getString(3));
			}
			return laboratorioBean;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}

			return laboratorioBean;

		}
	}
	
		public static LaboratorioBean buscaLaboratorioPorId(Connection conn,int p_Id) {
		
		PreparedStatement stm = null;
		ResultSet rs = null;
		LaboratorioBean laboratorioBean = new LaboratorioBean();
		try
		{
			stm = conn.prepareStatement(QUERY_BUSCAR_LABORATORIO_ID);
			stm.setInt(1, p_Id);
			rs = stm.executeQuery();
			if (rs.next())
			{
				laboratorioBean.setId(rs.getInt(1));
				laboratorioBean.setNome(rs.getString(2));
				laboratorioBean.setTelefone(rs.getString(3));
			}
			return laboratorioBean;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}

			return laboratorioBean;

		}
	}
}
