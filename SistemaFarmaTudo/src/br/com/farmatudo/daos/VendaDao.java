package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.farmatudo.bean.VendaBean;

public class VendaDao
{

	private static String CRIAR_VENDA = "INSERT INTO venda"
			+ "(usuario,sub_total,desconto,valor_desconto,data)"
			+ "VALUES (?,?,?,?,?)";
	private static String RELATORIO_VENDAS_DIARIO = "SELECT * FROM db_farmatudo_.venda where data >=? order by data";

	public static ResultSet retornaRelatorioVendas(Connection conn, java.util.Date dateSelecionado)
	{
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try
		{
			preparedStatement = conn.prepareStatement(RELATORIO_VENDAS_DIARIO);
			preparedStatement.setDate(1,  new java.sql.Date(dateSelecionado.getTime()));
			resultSet = preparedStatement.executeQuery();
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return resultSet;
	}
	/** Metodo que inseri uma venda **/
	public static boolean inseriVenda(Connection conn, VendaBean vendaBean)
	{
		PreparedStatement stm = null;
		boolean flagOk = false;

		try
		{
			stm = conn.prepareStatement(CRIAR_VENDA);
			stm.setString(1, vendaBean.getUsuario());
			stm.setDouble(2, vendaBean.getSub_total());
			stm.setInt(3, vendaBean.getDesconto());
			stm.setDouble(4, vendaBean.getValor_desconto());
			stm.setDate(5, new java.sql.Date(vendaBean.getData().getTime()));
			
			
			stm.executeUpdate();
			flagOk = true;
			return flagOk;
		} catch (Exception e)
		{
			e.printStackTrace();
			flagOk = false;
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return flagOk;

	}
}
