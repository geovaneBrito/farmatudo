package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.farmatudo.bean.VendaBean;

public class VendaConvenioDao
{
	private static String QUERY_ULTIMO_ID_VENDA = "select max(id) as ULTIMO_ID from venda";
	private static String INSERIR_VENDA_CONVENIO = "INSERT INTO venda_convenio(empresa,venda_id,nome_funcionario)VALUES(?,?,?)";

	/** Metodo que inseri uma venda **/
	public static boolean inseriVendaConvenio(Connection conn, String p_Empresa, String p_Funcionario)
	{
		PreparedStatement stm = null;
		boolean flagOk = false;
		int idVenda = getUltimaVenda(conn);

		try
		{
			stm = conn.prepareStatement(INSERIR_VENDA_CONVENIO);
			stm.setString(1, p_Empresa);
			stm.setInt(2, idVenda);
			stm.setString(3, p_Funcionario);
			stm.executeUpdate();

			flagOk = true;
			return flagOk;
		} catch (Exception e)
		{
			e.printStackTrace();
			flagOk = false;
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return flagOk;

	}

	/**
	 * Metodo que realizar consulta no bd para verificar qual o ultimo ID de
	 * venda
	 * 
	 **/
	private static int getUltimaVenda(Connection conn)
	{

		PreparedStatement statement = null;
		ResultSet result = null;
		int novoIdProduto = 0;
		try
		{

			statement = conn.prepareStatement(QUERY_ULTIMO_ID_VENDA);
			result = statement.executeQuery();
			if (result.next())
			{

				novoIdProduto = result.getInt("ULTIMO_ID");
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return novoIdProduto;
	}

}
