package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.farmatudo.bean.FornecedorBean;
import br.com.farmatudo.bean.ProdutoBean;

public class FornecedorDao
{

	private static String QUERY_BUSCAR_FORNECEDOR_EXPE = "SELECT * FROM db_farmatudo_.fornecedor where telefone = ? and nome = ?";
	
	private static String QUERY_BUSCAR_TODOS = "SELECT * FROM db_farmatudo_.fornecedor";
	private static String QUERY_BUSCAR_FORNECEDOR_TELEFONE = "SELECT * FROM db_farmatudo_.fornecedor where telefone like ?";
	private static String QUERY_BUSCAR_FORNECEDOR_NOME = "SELECT * FROM db_farmatudo_.fornecedor where nome like ?";
	
	private static String QUERY_INSERIR_FORNECEDOR = "INSERT INTO `db_farmatudo_`.`fornecedor`(`nome`,`telefone`)"
			+ "VALUES" + "(?,?);";

	private static String QUERY_UPDATE_FORNECEDOR = "UPDATE `db_farmatudo_`.`fornecedor` SET" + "`telefone` = ?"
			+ "WHERE `nome` = ?;";

	/***
	 * Metodo que realiza consulta de todos os laboratorio, trazendo todos os
	 * dados dos laboratorios
	 */
	public static ArrayList<FornecedorBean> carregarFornecedoresPorTelefone(Connection conn, String p_Telefone)
	{
		PreparedStatement stm = null;
		ResultSet rs = null;
		ArrayList<FornecedorBean> listFornecedor = new ArrayList<FornecedorBean>();
		try
		{
			stm = conn.prepareStatement(QUERY_BUSCAR_FORNECEDOR_TELEFONE);
			stm.setString(1, p_Telefone);
			rs = stm.executeQuery();
			while (rs.next())
			{
				FornecedorBean fornecedor = new FornecedorBean();
				fornecedor.setNome(rs.getString(1));
				fornecedor.setTelefone(rs.getString(2));
				System.out.println(fornecedor.getNome() + "  " + fornecedor.getTelefone());
				listFornecedor.add(fornecedor);
			}
			return listFornecedor;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listFornecedor;

	}

	/***
	 * Metodo que realiza consulta de todos os laboratorio, trazendo todos os
	 * dados dos laboratorios
	 */
	public static ArrayList<FornecedorBean> carregarFornecedores(Connection conn)
	{
		PreparedStatement stm = null;
		ResultSet rs = null;
		ArrayList<FornecedorBean> listFornecedor = new ArrayList<FornecedorBean>();
		try
		{
			stm = conn.prepareStatement(QUERY_BUSCAR_TODOS);
			rs = stm.executeQuery();
			while (rs.next())
			{
				FornecedorBean fornecedor = new FornecedorBean();
				fornecedor.setNome(rs.getString(1));
				fornecedor.setTelefone(rs.getString(2));
				System.out.println(fornecedor.getNome() + "  " + fornecedor.getTelefone());
				listFornecedor.add(fornecedor);
			}
			return listFornecedor;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listFornecedor;

	}

	public static boolean salvar(Connection conn, FornecedorBean fornecedor)
	{
		boolean flagOk = false;
		PreparedStatement statement = null;
		ResultSet result = null;

		try
		{
			statement = conn.prepareStatement(QUERY_INSERIR_FORNECEDOR);

			statement.setString(1, fornecedor.getNome());
			statement.setString(2, fornecedor.getTelefone());

			statement.executeUpdate();
			flagOk = true;
		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}

		return flagOk;

	}

	public static boolean updateFornecedor(Connection conn, FornecedorBean fornecedor)
	{
		boolean flagOk = false;
		PreparedStatement statement = null;
		System.out.println("Update");
		try
		{
			statement = conn.prepareStatement(QUERY_UPDATE_FORNECEDOR);
			statement.setString(1, fornecedor.getTelefone());
			statement.setString(2, fornecedor.getNome());

			statement.executeUpdate();
			flagOk = true;
		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return flagOk;

	}

	public static FornecedorBean buscaFornecedorExpecifico(Connection conn, String p_Telefone, String p_Nome)
	{
		PreparedStatement stm = null;
		ResultSet rs = null;
		FornecedorBean fornecedorBean = new FornecedorBean();
		try
		{
			stm = conn.prepareStatement(QUERY_BUSCAR_FORNECEDOR_EXPE);
			stm.setString(1, p_Telefone);
			stm.setString(2, p_Nome);
			rs = stm.executeQuery();
			if (rs.next())
			{

				fornecedorBean.setNome(rs.getString(1));
				fornecedorBean.setTelefone(rs.getString(2));
			}
			return fornecedorBean;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}

			return fornecedorBean;

		}
	}

	public static ArrayList<FornecedorBean>  carregarFornecedoresPorNome(Connection conn, String p_nome)
	{
		
		PreparedStatement stm = null;
		ResultSet rs = null;
		ArrayList<FornecedorBean> listFornecedor = new ArrayList<FornecedorBean>();
		try
		{
			stm = conn.prepareStatement(QUERY_BUSCAR_FORNECEDOR_NOME);
			stm.setString(1, p_nome);
			rs = stm.executeQuery();
			while (rs.next())
			{
				FornecedorBean fornecedor = new FornecedorBean();
				fornecedor.setNome(rs.getString(1));
				fornecedor.setTelefone(rs.getString(2));
				listFornecedor.add(fornecedor);
			}
			return listFornecedor;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listFornecedor;
	}
}