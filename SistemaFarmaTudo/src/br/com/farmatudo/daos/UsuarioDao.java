package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.farmatudo.bean.LaboratorioBean;
import br.com.farmatudo.bean.UsuarioBean;

public class UsuarioDao {
	private static String QUERY_INSERT_USUARIO = "INSERT INTO `db_farmatudo_`.`usuario`"
												+"(`nome`,`senha`,`tipo`)"
												+"VALUES(?,?,?);";
	
	public static boolean salvar(Connection conn, UsuarioBean usuario) {
		boolean flagOk = false;
		PreparedStatement statement = null;
		ResultSet result = null;

		try {
			statement = conn.prepareStatement(QUERY_INSERT_USUARIO);
			
			statement.setString(1, usuario.getNome());
			statement.setString(2, usuario.getSenha());
			statement.setString(3, usuario.getTipo());


			statement.executeUpdate();
			flagOk = true;
		} catch (Exception e) {
			flagOk = false;
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException el) {
				System.out.println(el.getStackTrace());
			}
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException el2) {
					System.err.println(el2.getStackTrace());
				}
			}
		}

		return flagOk;

	}

}
