package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.jdesktop.swingx.JXDatePicker;

import br.com.farmatudo.bean.CaixaBean;
import br.com.farmatudo.bean.NotaBean;

public class CaixaDao
{
	private static String QUERY_BUSCA_POR_ID = "SELECT * FROM caixa WHERE usuario_nome = ? and data = ? ";
	private static String QUERY_INSERIR = "INSERT INTO `db_farmatudo_`.`caixa`"
			+ "(`usuario_nome`,`data`,`status`,`valor_abertura`) VALUES (?,?,?,?)";
	private static String QUERY_ATUALIZAR_ABERTO = "UPDATE `db_farmatudo_`.`caixa` SET "
			+ "`usuario_nome` = ?,`data` = ?,`status` = ?,`valor_abertura` = ? "
			+ " WHERE `id` = ?";

	/** Metodo que retorna o caixa ***/
	public static CaixaBean buscaPorID(Connection conn, String p_NomeUsuario, JXDatePicker date)
	{

		PreparedStatement statement = null;
		ResultSet resultSet = null;
		CaixaBean bean = new CaixaBean();

		try
		{
			statement = conn.prepareStatement(QUERY_BUSCA_POR_ID);
			statement.setString(1, p_NomeUsuario);
			statement.setDate(2, new java.sql.Date(date.getDate().getTime()));
			System.out.println(statement);
			resultSet = statement.executeQuery();
			if (resultSet.next())
			{
				bean.setId(resultSet.getInt(1));
				bean.setUsuario_nome(resultSet.getString(2));
				bean.setData(resultSet.getDate(3));
				bean.setStatus(resultSet.getBoolean(4));
				bean.setValor_abertura(resultSet.getFloat(5));

			}
			return bean;
		} catch (Exception e)
		{

			e.printStackTrace();

			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}

		return bean;

	}

	public static boolean inserirCaixaAberto(CaixaBean caixaBean, Connection conn)
	{
		PreparedStatement stm = null;
		boolean flagOk = false;
		try
		{
			stm = conn.prepareStatement(QUERY_INSERIR);
			
			stm.setString(1, caixaBean.getUsuario_nome());
			stm.setDate(2, new java.sql.Date(caixaBean.getData().getTime()));
			stm.setBoolean(3, caixaBean.getStatus());
			stm.setFloat(4, caixaBean.getValor_abertura());
		
			
			stm.executeUpdate();
			
			flagOk = true;

		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();

			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}

		return flagOk;

	}

	public static boolean atualizarCaixaAberto(CaixaBean caixaBean, Connection conn)
	{
		PreparedStatement stm = null;
		boolean flagOk = false;
		try
		{
			stm = conn.prepareStatement(QUERY_ATUALIZAR_ABERTO);
			stm.setString(1, caixaBean.getUsuario_nome());
			stm.setDate(2, new java.sql.Date(caixaBean.getData().getTime()));
			stm.setBoolean(3, caixaBean.getStatus());
			stm.setFloat(4, caixaBean.getValor_abertura());
			stm.setInt(5, caixaBean.getId());
			stm.executeUpdate();
			flagOk = true;

		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();

			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}

		return flagOk;

	}

}
