package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.jdesktop.swingx.JXDatePicker;

import br.com.farmatudo.bean.NotaBean;

public class ProdutoNotaDao
{
	private static String QUERY_INSERIR_PRODUTO_NOTA = "INSERT INTO produto_nota (produto_id,nota_id,data_criacao) VALUES (?,?,?)";
	
	public static boolean inserirNota(Connection conn, int p_NotaId, int p_ProdutoId, JXDatePicker p_DataInicial)
	{
		PreparedStatement stm =null;
		boolean flagOk = false;
		try
		{
			stm = conn.prepareStatement(QUERY_INSERIR_PRODUTO_NOTA);
			stm.setInt(1, p_ProdutoId);
			stm.setFloat(2, p_NotaId);
			stm.setDate(3,new java.sql.Date(p_DataInicial.getDate().getTime()));
			stm.executeUpdate();
			flagOk = true;
			
		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();
			
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
	
		return flagOk;
	}
}