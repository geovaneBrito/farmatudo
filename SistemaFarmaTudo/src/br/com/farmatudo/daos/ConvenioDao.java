package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.farmatudo.bean.CategoriaBean;
import br.com.farmatudo.bean.ConvenioBean;

public class ConvenioDao
{
	private static String BUSCAR_TODOS = "SELECT * FROM convenio";
	
	public static ArrayList<ConvenioBean> carregarTodosConvenio(Connection conn)
	{
		PreparedStatement stm = null;
		ResultSet rs = null;
		ArrayList<ConvenioBean> listConvenio = new ArrayList<ConvenioBean>();
		try
		{
			stm = conn.prepareStatement(BUSCAR_TODOS);
			rs = stm.executeQuery();
			while (rs.next())
			{
				ConvenioBean convenioBean = new ConvenioBean();
				convenioBean.setEmpresa(rs.getString(1));
				listConvenio.add(convenioBean);
			}
		return listConvenio;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listConvenio;
		
	}
}
