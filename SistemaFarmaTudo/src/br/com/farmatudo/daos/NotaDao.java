package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.farmatudo.bean.NotaBean;

public class NotaDao
{
	private static String QUERY_CRIAR_NOTA = "INSERT INTO `db_farmatudo_`.`nota`"
			+ "(numero_nota,total,data) VALUES (?,?,?)";
	private static String QUERY_SEQUENCE_NOTA = "select max(id) as ULTIMO_ID from nota";
	
	/**
	 * Metodo que realizar consulta no bd para verificar qual o proximo ID
	 * disponivel
	 **/
	public static int getProximoId(Connection conn)
	{

		PreparedStatement statement = null;
		ResultSet result = null;
		int novoIdProduto = 0;
		try
		{

			statement = conn.prepareStatement(QUERY_SEQUENCE_NOTA);
			result = statement.executeQuery();
			if (result.next())
			{

				novoIdProduto = result.getInt("ULTIMO_ID");
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return novoIdProduto;
	}
	
	
	public static boolean inserirNota(Connection conn, NotaBean notaBean)
	{
		PreparedStatement stm =null;
		boolean flagOk = false;
		try
		{
			stm = conn.prepareStatement(QUERY_CRIAR_NOTA);
			stm.setString(1, notaBean.getNumero_nota());
			stm.setFloat(2, notaBean.getTotal());
			stm.setDate(3, new java.sql.Date(notaBean.getData().getTime()));
			stm.executeUpdate();
			flagOk = true;
			
		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();
			
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
	
		return flagOk;
	}
}
