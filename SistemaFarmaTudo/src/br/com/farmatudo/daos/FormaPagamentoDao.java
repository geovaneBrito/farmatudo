package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.farmatudo.bean.VendaBean;

public class FormaPagamentoDao
{
	private static String INSERIR_FORMA_PAGAMENTO = "INSERT INTO forma_pagamento_venda (forma_pagamento_id,venda_id)"
			+ "VALUES (?,?)";

	private static String QUERY_ULTIMO_ID_VENDA = "select max(id) as ULTIMO_ID from venda";
	/** Metodo que inseri uma venda **/
	public static boolean inseriFormaPagamento(Connection conn,int[] p_FormaPagamento)
	{
		PreparedStatement stm = null;
		boolean flagOk = false;
		int idVenda = getUltimaVenda(conn);
		try
		{
			stm = conn.prepareStatement(INSERIR_FORMA_PAGAMENTO);
			for (int i = 0; i < p_FormaPagamento.length; i++)
			{
				System.out.println(p_FormaPagamento[i] + " : " + idVenda);
				stm.setInt(1, p_FormaPagamento[i]);
				stm.setInt(2, idVenda);
				
				stm.executeUpdate();
				
			}
			
			flagOk = true;
			return flagOk;
		} catch (Exception e)
		{
			e.printStackTrace();
			flagOk = false;
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return flagOk;

	}
	
	
	/**
	 * Metodo que realizar consulta no bd para verificar qual o ultimo ID de venda
	 * 
	 **/
	private static int getUltimaVenda(Connection conn)
	{

		PreparedStatement statement = null;
		ResultSet result = null;
		int novoIdProduto = 0;
		try
		{

			statement = conn.prepareStatement(QUERY_ULTIMO_ID_VENDA);
			result = statement.executeQuery();
			if (result.next())
			{

				novoIdProduto = result.getInt("ULTIMO_ID");
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return novoIdProduto;
	}


}
