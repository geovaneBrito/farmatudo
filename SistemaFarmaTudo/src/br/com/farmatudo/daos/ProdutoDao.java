package br.com.farmatudo.daos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;

import br.com.farmatudo.bean.NotaBean;
import br.com.farmatudo.bean.ProdutoBean;

public class ProdutoDao
{
	private static String QUERY_CRIAR_PRODUTO = "INSERT INTO `db_farmatudo_`.`produto`" + "(`id`, "
			+ "`laboratorio_id`, " + "`categoria`, " + "`descricao`, " + "`valor_custo`," + "`margem`, "
			+ "`valor_venda`) VALUES (?,?,?,?,?,?,?)";

	private static String QUERY_CRIAR_LISTA_PRODUTO = "INSERT INTO `db_farmatudo_`.`produto`" + "(`id`, "
			+ "`laboratorio_id`, " + "`categoria`, " + "`descricao`, " + "`valor_custo`," + "`margem`, "
			+ "`valor_venda`) VALUES (?,?,?,?,?,?,?)";

	private static String QUERY_CRIAR_ESTOQUE = "INSERT INTO `db_farmatudo_`.`estoque`"
			+ "(`qtd`,`produto_id`)	VALUES	(?,?)";
	
	private static String QUERY_CONSULTA_PRODUTO_ESTOQUE = "SELECT * FROM estoque es inner join produto pr on pr.id = es.produto_id where pr.id = ?";

	private static String QUERY_SEQUENCE_PRODUTO = "select max(id)+1 as NOVO_ID_PRODUTO from produto";

	private static String QUERY_TODOS_PRODUTOS = "SELECT " + "produto.id," + "produto.laboratorio_id, produto.categoria, "
			+ "produto.descricao, produto.valor_custo, " + "produto.margem, produto.valor_venda, estoque.qtd "
			+ " FROM produto inner join estoque on estoque.produto_id = produto.id "
			+ "group by produto.descricao asc ";

	private static String QUERY_UPDATE_PRODUTO = "UPDATE produto SET laboratorio_id = ?,"
			+ "categoria = ?,descricao = ?,valor_custo = ? ,margem = ?,valor_venda = ? " + "WHERE id = ?";

	private static String QUERY_UPDATE_ESTOQUE = "UPDATE estoque SET " + "qtd = ? WHERE produto_id = ?";

	private static String QUERY_BUSCA_POR_ID = "SELECT * FROM produto WHERE id = ?";
	private static String QUERY_BUSCA_POR_NOME = "SELECT * FROM produto WHERE descricao = ?";
	
	// C�digo de Produto", "Descri��o", "Valor de Custo",
	// "Margem", "Valor de Venda", "Quantidades", "Categoria"
	private static String QUERY_BUSCAR_COM_LIKE = "SELECT * FROM produto where ";
	private static String RELATORIO_ESTOQUE_PRODUTO = "SELECT concat(pr.id,'',pr.laboratorio_id) as 'codigo',pr.categoria,pr.descricao,es.qtd FROM estoque es inner join produto pr on pr.id = es.produto_id group by pr.descricao order by pr.categoria,pr.descricao ";

	public static ResultSet retornaRelatorioEstoque(Connection conn)
	{
		Date data = new Date(System.currentTimeMillis());
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		try
		{
			preparedStatement = conn.prepareStatement(RELATORIO_ESTOQUE_PRODUTO);
			resultSet = preparedStatement.executeQuery();
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return resultSet;
	}
	
	/**
	 * Metodo que carrega todos os produtos cadastrados, e devolve um arralist
	 * de produtos
	 ***/
	public static ArrayList<ProdutoBean> carregarTodosProduto(Connection conn)
	{

		PreparedStatement stm = null;
		ResultSet rs = null;
		ArrayList<ProdutoBean> listProduto = new ArrayList<ProdutoBean>();
		try
		{
			stm = conn.prepareStatement(QUERY_TODOS_PRODUTOS);
			rs = stm.executeQuery();
			while (rs.next())
			{
				ProdutoBean produto = new ProdutoBean(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4),
						rs.getDouble(5), rs.getInt(6), rs.getDouble(7), rs.getInt(8));
				listProduto.add(produto);

			}
			return listProduto;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listProduto;

	}

	

	/**
	 * Metodo que realizar consulta no bd para verificar qual o proximo ID
	 * disponivel
	 **/
	private static int getProximoId(Connection conn)
	{

		PreparedStatement statement = null;
		ResultSet result = null;
		int novoIdProduto = 0;
		try
		{

			statement = conn.prepareStatement(QUERY_SEQUENCE_PRODUTO);
			result = statement.executeQuery();
			if (result.next())
			{

				novoIdProduto = result.getInt("NOVO_ID_PRODUTO");
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return novoIdProduto;
	}
	
	public static int consultaProdutoEstoque( Connection conn , int p_IdProduto)
	{
	
		PreparedStatement statement = null;
		int quantidadeEstoque = 0;
		ResultSet rs = null;
		try
		{
			statement = conn.prepareStatement(QUERY_CONSULTA_PRODUTO_ESTOQUE);
			statement.setInt(1, p_IdProduto);
			rs = statement.executeQuery();
			if (rs.next())
			{
				quantidadeEstoque = rs.getInt(1);
			}
		} catch (Exception e)
		{
			e.printStackTrace();

			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		}finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		
		return quantidadeEstoque;
	}
	
	/** Metodo que inseri um arraylist de produtos ***/
	public static boolean salvar(Connection conn, ArrayList<ProdutoBean> produtoBeans, NotaBean notaBean)
	{
		boolean flagOk = false;
		PreparedStatement statement = null;

		try
		{
			statement = conn.prepareStatement(QUERY_CRIAR_LISTA_PRODUTO);
			for (ProdutoBean bean : produtoBeans)
			{
				int novoIdProduto = getProximoId(conn);
				statement.setInt(1, novoIdProduto);
				statement.setInt(2, bean.getLaboratorio());
				statement.setString(3, bean.getCategoria());
				statement.setString(4, bean.getDescricao());
				statement.setDouble(5, bean.getValor_custo());
				statement.setInt(6, bean.getMargem());
				statement.setDouble(7, bean.getValor_venda());
				statement.executeUpdate();

				flagOk = true;
			}
		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();

			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		if (flagOk)
		{
			flagOk = inserirEstoque(conn, produtoBeans, flagOk);
			if (flagOk)
			{
				flagOk = NotaDao.inserirNota(conn, notaBean);
			}

		}
		return flagOk;

	}

	/** Metodo que inseri um arraylist de produtos ***/
	public static ProdutoBean buscaPorID(Connection conn, int p_IdProduto)
	{
		boolean flagOk = false;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		ProdutoBean bean = new ProdutoBean();

		try
		{
			statement = conn.prepareStatement(QUERY_BUSCA_POR_ID);
			statement.setInt(1, p_IdProduto);
			resultSet = statement.executeQuery();
			if (resultSet.next())
			{
				bean.setId(resultSet.getInt(1));
				bean.setCategoria(resultSet.getString(3));
				bean.setDescricao(resultSet.getString(4));
				bean.setValor_custo(resultSet.getDouble(5));
				bean.setMargem(resultSet.getInt(6));
				bean.setValor_venda(resultSet.getDouble(7));

			}
			return bean;
		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();

			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}

		return bean;

	}

	/**
	 * Metodo que inseri em um arraylist de produtos*
	 * 
	 * @param String
	 *            p_NomeProduto
	 **/
	public static ProdutoBean buscaPorNome(Connection conn, String p_NomeProduto)
	{
		
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		ProdutoBean bean = new ProdutoBean();

		try
		{
			statement = conn.prepareStatement(QUERY_BUSCA_POR_NOME);
			statement.setString(1, p_NomeProduto);
			resultSet = statement.executeQuery();
			if (resultSet.next())
			{
				bean.setId(resultSet.getInt(1));
				bean.setLaboratorio(resultSet.getInt(2));
				bean.setCategoria(resultSet.getString(3));
				bean.setDescricao(resultSet.getString(4));
				bean.setValor_custo(resultSet.getDouble(5));
				bean.setMargem(resultSet.getInt(6));
				bean.setValor_venda(resultSet.getDouble(7));

			}
			return bean;
		} catch (Exception e)
		{
			
			e.printStackTrace();

			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}

		return bean;

	}

	private static boolean inserirEstoque(Connection conn, ArrayList<ProdutoBean> produtoBeans, boolean flagOk)
	{
		PreparedStatement statement;
		ResultSet result;
		statement = null;
		result = null;
		try
		{
			statement = conn.prepareStatement(QUERY_CRIAR_ESTOQUE);
			for (ProdutoBean bean : produtoBeans)
			{
				statement.setInt(1, bean.getQuantidade());
				statement.setInt(2, bean.getId());
				statement.executeUpdate();
				flagOk = true;

			}

		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();
			try
			{
				conn.rollback();
				System.out.println("Erro no segundo insert");

			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return flagOk;
	}

	public static boolean salvar(Connection conn, ProdutoBean bean)
	{
		int novoIdProduto = getProximoId(conn);
		boolean flagOk = false;
		PreparedStatement statement = null;
		ResultSet result = null;

		try
		{
			statement = conn.prepareStatement(QUERY_CRIAR_PRODUTO);
			bean.setId(novoIdProduto);

			statement.setInt(1, bean.getId());
			statement.setInt(2,bean.getLaboratorio());
			statement.setString(3, bean.getCategoria());
			statement.setString(4, bean.getDescricao());
			statement.setDouble(5, bean.getValor_custo());
			statement.setInt(6, bean.getMargem());
			statement.setDouble(7, bean.getValor_venda());

			statement.executeUpdate();
			flagOk = true;
		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}

		statement = null;
		result = null;
		try
		{
			statement = conn.prepareStatement(QUERY_CRIAR_ESTOQUE);
			statement.setInt(1, bean.getQuantidade());
			statement.setInt(2, bean.getId());

			statement.executeUpdate();
			flagOk = true;

		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();
			try
			{
				conn.rollback();
				System.out.println("Erro no segundo insert");

			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return flagOk;

	}
	
	/**Metodo que atualizar estoque de produtos**/
	public static boolean updateEstoque(Connection conn, int p_Quantidade, int p_IdProduto)
	{
		PreparedStatement statement = null;
		boolean flagOk = false;
		try
		{
			statement = conn.prepareStatement(QUERY_UPDATE_ESTOQUE);
			statement.setInt(1, p_Quantidade);
			statement.setInt(2, p_IdProduto);
			statement.executeUpdate();
			flagOk  = true;

		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();
			try
			{
				conn.rollback();

			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return flagOk;
	}
	public static boolean updateProduto(Connection conn, ProdutoBean bean)
	{
		boolean flagOk = false;
		PreparedStatement statement = null;
		int idProduto = bean.getId();

		try
		{
			statement = conn.prepareStatement(QUERY_UPDATE_PRODUTO);
			statement.setInt(1, bean.getLaboratorio());
			statement.setString(2, bean.getCategoria());
			statement.setString(3, bean.getDescricao());
			statement.setDouble(4, bean.getValor_custo());
			statement.setInt(5, bean.getMargem());
			statement.setDouble(6, bean.getValor_venda());
			statement.setInt(7, idProduto);

			statement.executeUpdate();
			flagOk = true;
		} catch (Exception e)
		{
			flagOk = false;
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		} finally
		{
			if (statement != null)
			{
				try
				{
					statement.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		
		flagOk = updateEstoque(conn, bean.getQuantidade(), idProduto);
		
		return flagOk;

	}



	public static ArrayList<ProdutoBean> buscarPorStringExpecifico(Connection conn, String p_sqlWhere, String p_pesquisa)
	{
		PreparedStatement stm = null;
		ResultSet resultSet = null;
		
		ArrayList<ProdutoBean> listaProduto = new ArrayList<ProdutoBean>();
		try
		{
		
			stm = conn.prepareStatement(QUERY_BUSCAR_COM_LIKE + p_sqlWhere);
			stm.setString(1, "%"+p_pesquisa+"%");

			resultSet = stm.executeQuery();
			while (resultSet.next())
			{
				ProdutoBean bean = new ProdutoBean();
				bean.setId(resultSet.getInt(1));
				bean.setLaboratorio(resultSet.getInt(2));
				bean.setCategoria(resultSet.getString(3));
				bean.setDescricao(resultSet.getString(4));
				bean.setValor_custo(resultSet.getDouble(5));
				bean.setMargem(resultSet.getInt(6));
				bean.setValor_venda(resultSet.getDouble(7));
				listaProduto.add(bean);
							
			}
			return listaProduto;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		}finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listaProduto;
	}



	public static  ArrayList<ProdutoBean> buscarPorDoubleExpecifico(Connection conn, String p_sqlWhere, String p_pesquisa)
	{
		PreparedStatement stm = null;
		ResultSet resultSet = null;
		double pesquisaConvert =  Double.parseDouble(p_pesquisa);
	
		ArrayList<ProdutoBean> listaProduto = new ArrayList<ProdutoBean>();
		try
		{
		
			stm = conn.prepareStatement(QUERY_BUSCAR_COM_LIKE + p_sqlWhere);
			stm.setString(1, "%"+pesquisaConvert + "%");
	
			resultSet = stm.executeQuery();
			while (resultSet.next())
			{
				ProdutoBean bean = new ProdutoBean();
				bean.setId(resultSet.getInt(1));
				bean.setLaboratorio(resultSet.getInt(2));
				bean.setCategoria(resultSet.getString(3));
				bean.setDescricao(resultSet.getString(4));
				bean.setValor_custo(resultSet.getDouble(5));
				bean.setMargem(resultSet.getInt(6));
				bean.setValor_venda(resultSet.getDouble(7));
				listaProduto.add(bean);
							
			}
			return listaProduto;
		} catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				conn.rollback();
			} catch (SQLException el)
			{
				System.out.println(el.getStackTrace());
			}
		}finally
		{
			if (stm != null)
			{
				try
				{
					stm.close();
				} catch (SQLException el2)
				{
					System.err.println(el2.getStackTrace());
				}
			}
		}
		return listaProduto;
	}


}
