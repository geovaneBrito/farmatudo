package br.com.farmatudo.startApplication;

import br.com.farmatudo.views.Login;

public class StartApplication
{
	public static void main(String[] args)
	{
		Login login = new Login();
		login.setLocationRelativeTo(null);
		login.setVisible(true);
	}
}
